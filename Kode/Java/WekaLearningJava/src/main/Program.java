package main;

import utilities.Data;

import java.net.UnknownHostException;


/**
 * Created by Aleksander on 26.02.14.
 */
public class Program {

    public static void main(String[] args) throws UnknownHostException {

        if(Data.getUser()== Data.Users.Erlend){
            Erlend.main(args);
            return;
        }

        if(Data.getUser()== Data.Users.Aleksander || Data.getUser() == Data.Users.AleksanderMac){
            Aleksander.main(args);
            return;
        }

        if(Data.getUser() == Data.Users.Simen){
            Simen.main(args);
            return;
        }
    }
}
