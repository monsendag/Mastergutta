package main;

import classifiers.IBk;
import classifiers.Svm;
import classifiers.batch.ABatchClassifier;
import classifiers.batch.AnnBatchClassifier;
import classifiers.batch.IBkBatchClassifier;
import classifiers.batch.SvmBatchClassifier;
import classifiers.settings.IBkSettings;
import classifiers.settings.SvmSettings;
import utilities.Data;
import utilities.FileTools;
import utilities.Log;
import utilities.StopWatch;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Erlend on 27.02.14.
 */
public class Erlend {

    public static final String[] representativeSelection = new String[]{"004", "034", "064", "094", "124", "154", "186", "207", "236", "242", "254", "259", "267", "287", "310", "330", "349", "370", "373", "385", "388", "404"};
    public static final String[] oldRepresentativeSelection = new String[]{"004", "064", "124", "244", "304", "364", "378", "406", "427", "456", "469", "487", "507", "530", "550", /*"569",*/ "593", "595", "608", "624"};
    public static final String[] additionalRepresentativeSelection = new String[]{"388", "404"};

    static int availableCores = 3;

    public static void main(String[] args){
        try {

            if(args.length > 0){
                try{
                    availableCores = Integer.parseInt(args[0]);
                    System.out.println("Set number of cores to " + availableCores);
                }catch(Exception ex){
                    System.out.println("Failed to set number of core. Please use a single integer as parameter.");
                }
            }
            System.out.println("Number of cores: " + availableCores);

            //ABatchClassifier.clean(ABatchClassifier.BatchFolder.finalDataSet, "svr");
            //ABatchClassifier.clean(ABatchClassifier.BatchFolder.finalDataSet, "svr_improved");
            //ABatchClassifier.clean(ABatchClassifier.BatchFolder.finalDataSet, "ibk");
            //ABatchClassifier.clean(ABatchClassifier.BatchFolder.finalDataSet, "ibk_improved");
            //ABatchClassifier.clean(ABatchClassifier.BatchFolder.finalDataSet, "ann");
            //ABatchClassifier.clean(ABatchClassifier.BatchFolder.finalDataSet, "ann_improved");

            ABatchClassifier.GlobalCodeSuffix = "_improved2";

            //runAllBatchesOnRepresentativeSelection();
            runAllBatchesOnFinalDataset();
            //createJobs();
            //runParameterTestingSvm();

            //createKnnJobs();
            //runParameterTestingKnn();

            /*String[] neverRemoveAttributes = new String[]{
                    "Delay",
                    "StopTime",
                    "ActualArrival",
                    "ScheduledArrival",
                    "DayOfWeek",
                    "TimeOfDayQuarter"
            };

            FindBestAttributes.removeExistingJobs();
            //for(ClassifierType t : ClassifierType.values())
            //  FindBestAttributes.createJobFiles(Erlend.representativeSelection, neverRemoveAttributes, t, Classifiers.createSettings(t));
            FindBestAttributes.createJobFiles(new String[]{"004"}, neverRemoveAttributes, ClassifierType.Ibk, Classifiers.createSettings(ClassifierType.Ibk));
            FindBestAttributes.runJobs(2);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createKnnJobs() throws IOException {
        String[] ids = representativeSelection;

        ArrayList<Integer> kValues = new ArrayList<Integer>();

        for(int i = -84; i <= 100; i++){
            kValues.add(i);
        }

        int jobs = 0;
        for (String fileName : ids) {
            for (IBkSettings.NeighborSearchAlgorithm algorithm : IBkSettings.NeighborSearchAlgorithm.values()) {
                for (IBkSettings.DistanceWeighting distance : IBkSettings.DistanceWeighting.values()) {
                    for (int kValue : kValues) {
                        File file = new File(Data.getWekaFile("jobs", "Knn_grid_5", fileName + "_" +  algorithm.name() + "_" + distance.name() + "_" + Integer.toString(kValue) + ".job"));
                        if(file.exists()) continue;
                        BufferedWriter output = new BufferedWriter(new FileWriter(file, true));
                        output.write(fileName);
                        output.newLine();
                        output.write(Integer.toString(algorithm.ordinal()));
                        output.newLine();
                        output.write(Integer.toString(distance.ordinal()));
                        output.newLine();
                        output.write(Integer.toString(kValue));
                        output.close();
                        jobs++;
                    }
                }
            }
        }

        System.out.println("Created " + jobs + " jobs.");
    }

    private static void runParameterTestingKnn() throws InterruptedException, IOException {
        ExecutorService service = Executors.newFixedThreadPool(availableCores);
        final Log log = Log.createNamed("knn_parametertesting_grid_5_" + Data.getUser().name() + ".txt");

        final File folder = new File(Data.getWekaFile("jobs", "Knn_grid_5"));

        for (int i = 0; i < availableCores; i++) {
            final int id = i;
            service.submit(new Runnable() {
                public void run() {
                    while (true) {
                        String settingsString = "";
                        try {
                            settingsString = "";

                            File current;
                            synchronized (folder) {
                                File[] jobs = FileTools.endsWith(folder.listFiles(), ".job");
                                if (jobs == null || jobs.length == 0) return;

                                File job = jobs[0];
                                current = new File(job.getAbsolutePath().replace(".job", "." + id + "_curr"));
                                job.renameTo(current);
                            }
                            String[] cmds = readFile(current.getAbsolutePath()).replace("\r\n", "\n").split("\n");
                            IBkSettings.NeighborSearchAlgorithm algorithm = IBkSettings.NeighborSearchAlgorithm.values()[Integer.parseInt(cmds[1])];
                            IBkSettings.DistanceWeighting distance = IBkSettings.DistanceWeighting.values()[Integer.parseInt(cmds[2])];
                            int kValue = Integer.parseInt(cmds[3]);

                            String dsID = cmds[0];
                            String file = Data.getWekaFile(ABatchClassifier.BatchFolder.finalDataSet.name(), dsID);

                            IBkSettings settings = IBkSettings.getDefaultSettings();

                            settings.Algorithm = algorithm;
                            settings.DistanceMeasure = distance;

                            String trainFile = file + "train.csv";
                            String testFile = file + "test.csv";

                            Instances train = new ConverterUtils.DataSource(trainFile).getDataSet();
                            Instances test = new ConverterUtils.DataSource(testFile).getDataSet();
                            train.setClass(train.attribute("Delay"));
                            test.setClass(test.attribute("Delay"));

                            int orgK = kValue;

                            if(kValue==0)
                                kValue = (int)(((float)train.numAttributes()));
                            if(kValue==-1)
                                kValue = (int)(((float)train.numAttributes()) / 2f);
                            if(kValue==-2)
                                kValue = (int)(((float)train.numAttributes()) / 3f);
                            if(kValue==-3)
                                kValue = (int)(((float)train.numAttributes()) / 4f);
                            if(kValue==-4)
                                kValue = (int)(((float)train.numAttributes()) / 5f);

                            if(kValue==-5)
                                kValue = (int)(((float)train.numInstances()));
                            if(kValue==-6)
                                kValue = (int)(((float)train.numInstances()) / 2f);
                            if(kValue==-7)
                                kValue = (int)(((float)train.numInstances()) / 3f);
                            if(kValue==-8)
                                kValue = (int)(((float)train.numInstances()) / 4f);
                            if(kValue==-9)
                                kValue = (int)(((float)train.numInstances()) / 5f);
                            if(kValue==-10)
                                kValue = (int)(((float)train.numInstances()) / 6f);
                            if(kValue==-11)
                                kValue = (int)(((float)train.numInstances()) / 7f);
                            if(kValue==-12)
                                kValue = (int)(((float)train.numInstances()) / 8f);
                            if(kValue==-13)
                                kValue = (int)(((float)train.numInstances()) / 9f);
                            if(kValue==-14)
                                kValue = (int)(((float)train.numInstances()) / 10f);
                            if(kValue==-15)
                                kValue = (int)(((float)train.numInstances()) / 15f);
                            if(kValue==-16)
                                kValue = (int)(((float)train.numInstances()) / 20f);
                            if(kValue==-17)
                                kValue = (int)(((float)train.numInstances()) / 25f);
                            if(kValue==-18)
                                kValue = (int)(((float)train.numInstances()) / 30f);
                            if(kValue==-19)
                                kValue = (int)(((float)train.numInstances()) / 50f);
                            if(kValue==-20)
                                kValue = (int)(((float)train.numInstances()) / 60f);
                            if(kValue==-21)
                                kValue = (int)(((float)train.numInstances()) / 70f);
                            if(kValue==-22)
                                kValue = (int)(((float)train.numInstances()) / 80f);
                            if(kValue==-23)
                                kValue = (int)(((float)train.numInstances()) / 90f);
                            if(kValue==-24)
                                kValue = (int)(((float)train.numInstances()) / 100f);
                            if(kValue==-25)
                                kValue = (int)(((float)train.numInstances()) / 110f);
                            if(kValue==-26)
                                kValue = (int)(((float)train.numInstances()) / 120f);
                            if(kValue==-27)
                                kValue = (int)(((float)train.numInstances()) / 130f);
                            if(kValue==-28)
                                kValue = (int)(((float)train.numInstances()) / 140f);
                            if(kValue==-29)
                                kValue = (int)(((float)train.numInstances()) / 150f);
                            if(kValue==-30)
                                kValue = (int)(((float)train.numInstances()) / 160f);
                            if(kValue==-31)
                                kValue = (int)(((float)train.numInstances()) / 170f);
                            if(kValue==-32)
                                kValue = (int)(((float)train.numInstances()) / 180f);
                            if(kValue==-33)
                                kValue = (int)(((float)train.numInstances()) / 190f);
                            if(kValue==-34)
                                kValue = (int)(((float)train.numInstances()) / 200f);
                            if(kValue==-35)
                                kValue = (int)(((float)train.numInstances()) / 210f);
                            if(kValue==-36)
                                kValue = (int)(((float)train.numInstances()) / 220f);
                            if(kValue==-37)
                                kValue = (int)(((float)train.numInstances()) / 230f);
                            if(kValue==-38)
                                kValue = (int)(((float)train.numInstances()) / 240f);
                            if(kValue==-39)
                                kValue = (int)(((float)train.numInstances()) / 250f);
                            if(kValue==-40)
                                kValue = (int)(((float)train.numInstances()) / 260f);
                            if(kValue==-41)
                                kValue = (int)(((float)train.numInstances()) / 270f);
                            if(kValue==-42)
                                kValue = (int)(((float)train.numInstances()) / 280f);
                            if(kValue==-43)
                                kValue = (int)(((float)train.numInstances()) / 290f);
                            if(kValue==-44)
                                kValue = (int)(((float)train.numInstances()) / 300f);
                            if(kValue==-45)
                                kValue = (int)(((float)train.numInstances()) / 310f);
                            if(kValue==-46)
                                kValue = (int)(((float)train.numInstances()) / 320f);
                            if(kValue==-47)
                                kValue = (int)(((float)train.numInstances()) / 330f);
                            if(kValue==-48)
                                kValue = (int)(((float)train.numInstances()) / 340f);
                            if(kValue==-49)
                                kValue = (int)(((float)train.numInstances()) / 350f);
                            if(kValue==-50)
                                kValue = (int)(((float)train.numInstances()) / 360f);
                            if(kValue==-51)
                                kValue = (int)(((float)train.numInstances()) / 370f);
                            if(kValue==-52)
                                kValue = (int)(((float)train.numInstances()) / 380f);
                            if(kValue==-53)
                                kValue = (int)(((float)train.numInstances()) / 390f);
                            if(kValue==-54)
                                kValue = (int)(((float)train.numInstances()) / 400f);
                            if(kValue==-55)
                                kValue = (int)(((float)train.numInstances()) / 410f);
                            if(kValue==-56)
                                kValue = (int)(((float)train.numInstances()) / 420f);
                            if(kValue==-57)
                                kValue = (int)(((float)train.numInstances()) / 430f);
                            if(kValue==-58)
                                kValue = (int)(((float)train.numInstances()) / 440f);
                            if(kValue==-59)
                                kValue = (int)(((float)train.numInstances()) / 450f);
                            if(kValue==-60)
                                kValue = (int)(((float)train.numInstances()) / 460f);
                            if(kValue==-61)
                                kValue = (int)(((float)train.numInstances()) / 470f);
                            if(kValue==-62)
                                kValue = (int)(((float)train.numInstances()) / 480f);
                            if(kValue==-63)
                                kValue = (int)(((float)train.numInstances()) / 490f);
                            if(kValue==-64)
                                kValue = (int)(((float)train.numInstances()) / 500f);


                            if(kValue==-65)
                                kValue = (int)(((float)train.numInstances()) / 11f);
                            if(kValue==-66)
                                kValue = (int)(((float)train.numInstances()) / 12f);
                            if(kValue==-67)
                                kValue = (int)(((float)train.numInstances()) / 13f);
                            if(kValue==-68)
                                kValue = (int)(((float)train.numInstances()) / 14f);
                            if(kValue==-69)
                                kValue = (int)(((float)train.numInstances()) / 16f);
                            if(kValue==-70)
                                kValue = (int)(((float)train.numInstances()) / 17f);
                            if(kValue==-71)
                                kValue = (int)(((float)train.numInstances()) / 18f);
                            if(kValue==-72)
                                kValue = (int)(((float)train.numInstances()) / 19f);
                            if(kValue==-73)
                                kValue = (int)(((float)train.numInstances()) / 21f);
                            if(kValue==-74)
                                kValue = (int)(((float)train.numInstances()) / 22f);
                            if(kValue==-75)
                                kValue = (int)(((float)train.numInstances()) / 23f);
                            if(kValue==-76)
                                kValue = (int)(((float)train.numInstances()) / 24f);
                            if(kValue==-77)
                                kValue = (int)(((float)train.numInstances()) / 26f);
                            if(kValue==-78)
                                kValue = (int)(((float)train.numInstances()) / 27f);
                            if(kValue==-79)
                                kValue = (int)(((float)train.numInstances()) / 28f);
                            if(kValue==-80)
                                kValue = (int)(((float)train.numInstances()) / 29f);
                            if(kValue==-81)
                                kValue = (int)(((float)train.numInstances()) / 11f);
                            if(kValue==-82)
                                kValue = (int)(((float)train.numInstances()) / 12f);
                            if(kValue==-83)
                                kValue = (int)(((float)train.numInstances()) / 13f);
                            if(kValue==-84)
                                kValue = (int)(((float)train.numInstances()) / 14f);

                            if(kValue < 1) kValue = 1;

                            settings.K = kValue;

                            settingsString = settings.toOptionsString();

                            IBk cls = new IBk(settings);

                            StopWatch stopWatch = new StopWatch();
                            cls.train(train);
                            String res = "Algorithm=" + algorithm.name() + ", Distance=" + distance.name() + ", K=" + kValue + ", OK=" + orgK + ", file='" + file + "'.\n";
                            res += stopWatch.get("Training finished in X ms.\n");
                            cls.evaluate(test);
                            res += cls.evalToString();
                            res += stopWatch.get("\nTesting finished in X ms.\n\n");

                            log.log(res);

                            current.delete();
                        } catch (Exception e) {
                            System.out.println(settingsString);
                            e.printStackTrace();
                            continue;
                        }
                    }
                }
            });
        }

        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }

    private static void createJobs() throws IOException {
        String[] ids = representativeSelection;

        ArrayList<Double> cValues = new ArrayList<Double>();
        ArrayList<Double> gammaValues = new ArrayList<Double>();
        /* Run 1: 2^-15 - 2^15
        for(int i = -15; i < 16; i++){
            cValues.add(Math.pow(2, i));
        }*/

        /* Run 2: 11 - 21
        for(int i = 11; i < 22; i++){
            cValues.add(new Double(i));
        }*/

        /* Run 2: 12 - 16
        for(double i = 12; i <= 16; i+= 0.2){
            cValues.add(i);
        } */

        /* Run 3: 13 - 13.6 CANCELLED
        for(double i = 12; i <= 16; i+= 0.02){
            cValues.add(i);
        }*/

        /* Run 4: C=13.3, gamma=[2^-15 - 2^15]
        cValues.add(13.3);
        for(int i = -15; i < 16; i++){
            gammaValues.add(Math.pow(2, i));
        }*/

        // Run 5: C=[2^-15 - 2^15], gamma=[2^-15 - 2^15]
        for(int i = -15; i < 16; i++){
            cValues.add(Math.pow(2, i));
        }
        for(int i = -15; i < 16; i++){
            gammaValues.add(Math.pow(2, i));
        }/**/

        /* Puk run 1: C=0.1-10.1
        for(int i = 1; i <= 101; i++){
            cValues.add(((double)i)/10f);
        }*/

        /* Run 6 (Svm4): C=[2^-1 - 2^4], gamma=[2^-6 - 2^-3]
        for(double i = -1; i < 4.1; i+=0.2){
            cValues.add(Math.pow(2, i));
        }
        for(double i = -6; i < -2.9; i+=0.2){
            gammaValues.add(Math.pow(2, i));
        } //*/

        /* Run 7 (Svm4): C=[2^2 - 2^3], gamma=[2^-5.8 - 2^-5]
        for(double i = 2; i < 3.02; i+=0.05){
            cValues.add(Math.pow(2, i));
        }
        for(double i = -5.6; i < -4.98; i+=0.05){
            gammaValues.add(Math.pow(2, i));
        } //*/

        /* Run 8 (Svm4): C=[2^2 - 2^3], gamma=[2^-5.8 - 2^-5]
        for(double i = 5.657; i < 6.062; i+=0.05){
            cValues.add(i);
        }
        for(double i = 0.0237; i < 0.0254+0.0005; i+=0.0005){
            gammaValues.add(i);
        } //*/

        /* Run 9 (Svm4): C=[2^2 - 2^3], gamma=[2^-5.8 - 2^-5]
        for(double i = 5.657; i < 5.758; i+=0.01){
            cValues.add(i);
        }
        for(double i = 0.0247; i < 0.0258; i+=0.0001){
            gammaValues.add(i);
        } //*/

        int jobs = 0;
        //All kernels: SvmSettings.Kernels.values()
        for (String fileName : ids) {
            for (SvmSettings.Kernels kernelValue : new SvmSettings.Kernels[]{SvmSettings.Kernels.RBFKernel}) {
                for (double cValue : cValues) {
                    for(double gammaValue : gammaValues){
                        DecimalFormat format = new DecimalFormat("0.0000000000000000000000000");
                        String filename = kernelValue.name() + "_" + format.format(cValue).replace(".",",") + "_" + format.format(gammaValue).replace(".",",") + "_" + fileName + ".job";
                        File file = new File(Data.getWekaFile("jobs", "Svm1", filename));
                        BufferedWriter output = new BufferedWriter(new FileWriter(file, true));
                        output.write(fileName);
                        output.newLine();
                        output.write(Integer.toString(kernelValue.ordinal()));
                        output.newLine();
                        output.write(format.format(cValue));
                        output.newLine();
                        output.write(format.format(gammaValue));
                        output.close();
                        jobs++;
                    }
                }
            }
        }
        System.out.println("Created " + jobs + "jobs.");
    }

    public static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        reader.close();

        return stringBuilder.toString();
    }

    private static void runParameterTestingSvm() throws InterruptedException, IOException {
        //final Log log = Log.createPath("/home/shomed/d/dageinm/erlend/svm_parametertesting.txt");
        final Log log = Log.createNamed("svm_grid1_" + Data.getUser().name() + ".txt");
        //final Log log = Log.createNamed("svm_parametertesting_" + Data.getUser().name() + ".txt");

        //Svm1/Svm2 is the rest of the initial C-Gamma-search for RBF.

        final File folder = new File(Data.getWekaFile("jobs", "Svm1"));
        System.out.println(folder.getAbsolutePath());

        Thread[] threads = new Thread[availableCores];
        for (int i = 0; i < availableCores; i++) {
            final int id = i;
            Thread t = new Thread(new Runnable() {
                public void run() {
                    while(true){
                        try {

                            File current;
                            synchronized (folder){
                                File[] jobs = FileTools.endsWith(folder.listFiles(), ".job");
                                if (jobs == null || jobs.length == 0) {
                                    System.out.println("Thread " + id +" exiting, no more files.");
                                    break;
                                }

                                File job = jobs[0];
                                current = new File(job.getAbsolutePath().replace(".job", "." + id + "_curr"));
                                job.renameTo(current);
                            }

                            String[] cmds = readFile(current.getAbsolutePath()).replace("\r\n", "\n").split("\n");
                            SvmSettings.Kernels kernel = SvmSettings.Kernels.values()[Integer.parseInt(cmds[1])];
                            double cValue;
                            try {
                                cValue = Double.parseDouble(cmds[2]);
                            } catch (NumberFormatException e) {
                                cValue = Double.parseDouble(cmds[2].replace(",","."));
                            }
                            double gammaValue;
                            try {
                                gammaValue = Double.parseDouble(cmds[3]);
                            } catch (NumberFormatException e) {
                                gammaValue = Double.parseDouble(cmds[3].replace(",", "."));
                            }
                            String dsID = cmds[0];
                            String file = Data.getWekaFile(ABatchClassifier.BatchFolder.finalDataSet.name(), dsID);

                            SvmSettings settings = SvmSettings.defaultValues();
                            settings.Kernel = kernel;
                            settings.C = cValue;
                            settings.Kernel_Gamma = gammaValue;

                            String trainFile = file + "train.csv";
                            String testFile = file + "test.csv";

                            Instances train = new ConverterUtils.DataSource(trainFile).getDataSet();
                            Instances test = new ConverterUtils.DataSource(testFile).getDataSet();
                            train.setClass(train.attribute("Delay"));
                            test.setClass(test.attribute("Delay"));

                            Svm cls = new Svm(settings);

                            StopWatch stopWatch = new StopWatch();
                            cls.train(train);
                            String res = kernel.name() + ", C=" + cValue + ", Gamma=" + gammaValue + ", file '" + file + "'.\n";
                            //String res = kernel.name() + ", C=" + cValue + ", file '" + file + "'.\n";
                            res += stopWatch.get("Training finished in X ms.\n");
                            cls.evaluate(test);
                            res += cls.evalToString();
                            res += stopWatch.get("\nTesting finished in X ms.\n\n");

                            log.log(res);

                            current.delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                            continue;
                        }
                    }
                    System.out.println("Thread " + id + " done.");
                }
            });
            threads[i] = t;
            t.start();
        }

        for (int i = 0; i < threads.length;i++)
            threads[i].join();
    }

    private static void runAllBatchesOnRepresentativeSelection() throws Exception {
        final String[] ids = representativeSelection;

        ExecutorService service = Executors.newFixedThreadPool(availableCores);

        service.submit(new Runnable() {
            public void run() {
                try {
                    new AnnBatchClassifier(ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids)).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        service.submit(new Runnable() {
            public void run() {
                try {
                    new SvmBatchClassifier(ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids)).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        service.submit(new Runnable() {
            public void run() {
                try {
                    new IBkBatchClassifier(ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids)).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }

    private static void runAllBatchesOnFinalDataset() throws Exception {

        ExecutorService service = Executors.newFixedThreadPool(8);

        service.submit(new Runnable() {
            public void run() {
                try {
                    new AnnBatchClassifier(ABatchClassifier.BatchFolder.finalDataSet).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        service.submit(new Runnable() {
            public void run() {
                try {
                    //new SvmBatchClassifier(ABatchClassifier.BatchFolder.finalDataSet).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        service.submit(new Runnable() {
            public void run() {
                try {
                    //new IBkBatchClassifier(ABatchClassifier.BatchFolder.finalDataSet).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }
}
