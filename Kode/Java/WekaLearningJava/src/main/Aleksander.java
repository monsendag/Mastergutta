package main;

import classifiers.AClassifier;
import classifiers.Ann;
import classifiers.IBk;
import classifiers.Svm;
import dataset.*;
import utilities.Data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Aleksander on 27.02.14.
 */
public class Aleksander {

    private static ExecutorService executorService;

    public static void main(String[] args) {
        //LogReader reader = new LogReader("C:\\Master\\RepoDataSync\\weka\\log\\log 2014-03-26 08-05-13.txt");
        //ParamExperimentation.AnnHiddenLayersTesting();
         // ParamExperimentation.AnnLearningRateAndMomentumTesting();
         //createPowerSetResults();
          //processPowerSetResults();
       // manualTestingOfAttributes();
       // Erlend.main(args);
        ParamExperimentation.AnnEpochsTesting();





       // SetUtilities.generatePowerSet(attributes);




    }

 /*   private static void manualTestingOfAttributes(){
        String train = "C:\\Master\\RepoDataSync\\weka\\parameterExperimentation\\Results\\04-07 Ann 1-dagers sammenligning\\488train.csv";
        String test = "C:\\Master\\RepoDataSync\\weka\\parameterExperimentation\\Results\\04-07 Ann 1-dagers sammenligning\\488test.csv";

        try {
            DataSet dataSet = new DataSet(train,test);
            dataSet.setClassAttribute("Delay");
            dataSet.removeAttributesForever(new ArrayList<String>(){{add("ActualArrival");}});

            dataSet.applyMyAttributeFilter(new ArrayList<String>(){{add("Humidity");}});


            AClassifier ann = new Ann();
            ann.train(dataSet.getTrainingData());
            Evaluation evaluation = ann.evaluate(dataSet.getTestData());
            System.out.println("Evaluation got an MAE of " + evaluation.meanAbsoluteError());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
*/
    private static void createPowerSetResults(){
        String train;
        String test;

        final String[] fileIds;
        final String classifierName;
        String[] removeAttributes;
        final String[] keepAttributes;
        final int cores;

        try {
            FindBestParameterSettings settings = new FindBestParameterSettings("." + File.separator + "settings.txt");
            classifierName = settings.getAttribute(SettingsAttribute.Classifier).toLowerCase();
            keepAttributes = settings.getAttributeArray(SettingsAttribute.KeepAttributes);
            removeAttributes = settings.getAttributeArray(SettingsAttribute.RemoveAttributes);
            cores = Integer.parseInt(settings.getAttribute(SettingsAttribute.Cores));
            fileIds = settings.getAttributeArray(SettingsAttribute.FileIds);

            System.out.println("=================================");
            System.out.println("Runner for finding best ");
            System.out.println("parameters for an " + settings.getAttribute(SettingsAttribute.Classifier) + ".");
            System.out.println("=================================");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


        executorService = Executors.newFixedThreadPool(cores);

        for(String fileId: fileIds){
            train = Data.getWekaFile("finalDataset_with_all_attributes", fileId + "train.csv") ;
            test = Data.getWekaFile("finalDataset_with_all_attributes", fileId + "test.csv");

            DataSet dataSet;
            try {
                dataSet = new DataSet(train,test);
                dataSet.setId(fileId);
                dataSet.removeAttributesForever(Arrays.asList(removeAttributes));
                run(dataSet,classifierName,keepAttributes, removeAttributes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private static void run(final DataSet dataSet, final String classifierName, final String[] keepAttributes, String[] removeAttributes){
        dataSet.applyMyAttributeFilter(Arrays.asList(removeAttributes));

        AClassifier classifier = null;
        if(classifierName.equals("ann")){
            //AnnSettings settings = new AnnSettings(0.1,0.6,500,"3,3,3");
            classifier = new Ann();
        }
        else if(classifierName.equals("ibk"))
            classifier = new IBk();
        else if(classifierName.equals("svm")) {
            classifier = new Svm();
        }

        final AClassifier finalClassifier = classifier;

        executorService.submit(new Runnable() {
            public void run() {
                try {
                    FindBestParameters bestParameters = new FindBestParameters(finalClassifier, dataSet, "Delay", classifierName);

                    bestParameters.setIgnoreAttributes(Arrays.asList(keepAttributes));
                   // bestParameters.generatePowerSet(1);
                    bestParameters.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


}


