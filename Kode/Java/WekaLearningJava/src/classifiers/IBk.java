package classifiers;

import classifiers.settings.IBkSettings;
import weka.classifiers.Classifier;


/**
 * Created by Aleksander on 13.03.14.
 */
public class IBk extends AClassifier {

    public static boolean debug = false;

    public IBk(){
        this(IBkSettings.getDefaultSettings());
    }

    public IBk(IBkSettings settings){
        super(settings);
        if(debug) System.out.println(settings.toOptionsString());
    }

    public String getName() {
        return "ibk";
    }

    @Override
    public Classifier createClassifier() {
        return new weka.classifiers.lazy.IBk();
    }
}
