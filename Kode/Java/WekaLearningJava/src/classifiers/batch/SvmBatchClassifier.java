package classifiers.batch;

import classifiers.Svm;
import classifiers.settings.SvmSettings;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.ArrayList;

/**
 * Created by Erlend on 12.03.14.
 */
public class SvmBatchClassifier extends ABatchClassifier {

    private SvmSettings settings;

    public SvmBatchClassifier(BatchFolder folder) {
        super(folder);
        initialize();
    }

    private void initialize() {
        //settings = SvmSettings.getBestValues();
        settings = SvmSettings.defaultValues();
    }

    public SvmBatchClassifier(ArrayList<String> files) {
        super(files);
        initialize();
    }

    @Override
    protected String getSettingsString() {
        return settings.toOptionsString();
    }

    @Override
    protected String getClassifierName() {
        return "Support Vector Regression";
    }

    @Override
    protected String getCode() {
        return "svr" + GlobalCodeSuffix;
    }

    @Override
    protected Evaluation runCase(Instances train, Instances test) throws Exception {
        Svm svm = new Svm(settings);
        svm.train(train);
        return svm.evaluate(test);
    }

}
