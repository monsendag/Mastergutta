package classifiers;

import classifiers.settings.AClassifierSettings;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.EvaluationUtils;
import weka.classifiers.evaluation.Prediction;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Created by Aleksander on 24.03.2014.
 */
public abstract class AClassifier {

    private AClassifierSettings _settings;
    private Instances _train;
    private Instances _test;

    private Classifier _classifier;
    private Evaluation _evaluation;

    public void setSettings(AClassifierSettings settings){
        _settings = settings;
    }

    public AClassifier(AClassifierSettings settings){
        setSettings(settings);
    }

    public void train(Instances train) throws Exception {
        this._train = train;
        _classifier = createClassifier();
        _classifier.setOptions(_settings.toOptions());
        _classifier.buildClassifier(train);
    }

    public Evaluation evaluate(Instances test) throws Exception {
        _test = test;
        _evaluation = new Evaluation(_train);
        _evaluation.evaluateModel(_classifier, test);
        return _evaluation;
    }

    public String evalToString() {
        if(_evaluation == null)
            throw new NullPointerException("You have to run 'Evaluate()' on the classifier before you fetch the evaluation string.");

        return "      > " + _evaluation.toSummaryString().trim().replace("\n", "\n      > ");
    }

    public abstract String getName();

    public Instances getTrainingInstances(){
        return _train;
    }

    public Instances getTestInstances(){
        return _test;
    }

    public Prediction[] getPredictions() throws Exception {
        EvaluationUtils utils = new EvaluationUtils();

        Prediction[] predictions = new Prediction[_test.numInstances()];

        for(int i = 0; i < getTestInstances().numInstances(); i++){
            Instance instance = getTestInstances().instance(i);
            Prediction pred = utils.getPrediction(_classifier, instance);
            predictions[i] = pred;
        }

        return predictions;
    }

    public abstract Classifier createClassifier();


}
