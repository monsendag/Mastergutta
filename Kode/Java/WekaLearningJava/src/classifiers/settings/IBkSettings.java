package classifiers.settings;

/**
 * Created by Aleksander on 13.03.14.
 */
public class IBkSettings extends AClassifierSettings {

    public DistanceWeighting DistanceMeasure = DistanceWeighting.None;
    public ErrorMinimization ErrorMinimizationMethod = ErrorMinimization.MinimizeMeanAbsoluteError;
    public int K = 0;
    public int Window = 0;
    public NeighborSearchAlgorithm Algorithm = NeighborSearchAlgorithm.LinearNN;

    public enum DistanceWeighting {
        InverseDistance,
        Distance,
        None
    }

    public enum ErrorMinimization {
        MinimizeMeanSquaredError,
        MinimizeMeanAbsoluteError
    }

    public enum NeighborSearchAlgorithm {
        BallTree,
        CoverTree,
        KDTree,
        LinearNN,
        LinearNNWithManhattanDistance,
        LinearNNWithChebyshevDistance
    }

    public IBkSettings(){

    }

    public static IBkSettings getDefaultSettings(){
        IBkSettings settings = new IBkSettings();
        settings.K = 5;
        return settings;
    }

    public static IBkSettings getBestSettings(int instances){
        IBkSettings settings = new IBkSettings();
        settings.K = (int) (((double)instances)/22);
        if(settings.K < 5) settings.K = 5;
        settings.Algorithm = NeighborSearchAlgorithm.LinearNNWithManhattanDistance;
        settings.DistanceMeasure = DistanceWeighting.InverseDistance;
        return settings;
    }

    @Override
    public String toOptionsString() {
        String optionsString = "";
        optionsString += " -K " + K;
        optionsString += " -W " + Window;

        if(DistanceMeasure == DistanceWeighting.InverseDistance)
            optionsString += " -I";
        else if(DistanceMeasure == DistanceWeighting.Distance)
            optionsString += " -F";

        if(ErrorMinimizationMethod == ErrorMinimization.MinimizeMeanSquaredError)
            optionsString += " -E";

        optionsString += " -A \"" + getAlgorithm() + "\"";
        return optionsString;
    }

    private String getAlgorithm() {
        switch (Algorithm){
            case BallTree:
                return "weka.core.neighboursearch.BallTree -A \\\"weka.core.EuclideanDistance -R first-last\\\" -C \\\"weka.core.neighboursearch.balltrees.TopDownConstructor -N 40 -S weka.core.neighboursearch.balltrees.PointsClosestToFurthestChildren\\\"";
            case CoverTree:
                return "weka.core.neighboursearch.CoverTree -A \\\"weka.core.EuclideanDistance -R first-last\\\" -B 1.3";
            case KDTree:
                return "weka.core.neighboursearch.KDTree -A \\\"weka.core.EuclideanDistance -R first-last\\\" -S weka.core.neighboursearch.kdtrees.SlidingMidPointOfWidestSide -W 0.01 -L 40 -N";
            case LinearNNWithManhattanDistance:
                return "weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.ManhattanDistance -R first-last\\\"";
            case LinearNN:
            default:
                return "weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"";
            case LinearNNWithChebyshevDistance:
                return "weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.ChebyshevDistance -R first-last\\\"";
        }
    }
}
