package classifiers.settings;

/**
 * Created by Aleksander on 19.03.2014.
 */
public abstract class AClassifierSettings {

    public abstract String toOptionsString();
    public String[] toOptions() throws Exception {
        return weka.core.Utils.splitOptions(toOptionsString());
    }

}
