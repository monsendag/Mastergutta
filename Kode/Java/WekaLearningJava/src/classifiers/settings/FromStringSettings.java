package classifiers.settings;

/**
 * Created by Erlend on 02.04.2014.
 */
public class FromStringSettings extends AClassifierSettings {

    private final String _settings;

    public FromStringSettings(String settings){
        _settings = settings;
    }

    @Override
    public String toOptionsString() {
        return _settings;
    }
}
