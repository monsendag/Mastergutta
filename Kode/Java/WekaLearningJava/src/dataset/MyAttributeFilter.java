package dataset;

import weka.core.Attribute;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksander on 27.02.14.
 */
public class MyAttributeFilter {

    private List<String>_attributesToRemove = new ArrayList<String>();

    public MyAttributeFilter(String attributeToRemove){
        _attributesToRemove.add(attributeToRemove);
    }

    public MyAttributeFilter(List<String> attributesToRemove){
        _attributesToRemove.addAll(attributesToRemove);
    }


    public Instances useFilter(Instances instances){
        Instances newInstances = new Instances(instances);

        for(int i = 0; i < newInstances.numAttributes();){
            String attrName = newInstances.attribute(i).name();
            if(_attributesToRemove.contains(attrName)){
                newInstances.deleteAttributeAt(i);
            }else i++;
        }
        return newInstances;
    }



}
