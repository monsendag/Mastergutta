package dataset;

/**
 * Created by Aleksander on 19.03.2014.
 */
public enum SettingsAttribute {
    KeepAttributes(0),
    RemoveAttributes(1),
    FileIds(2),
    Classifier(3),
    Cores(4);

    private final int index;

    SettingsAttribute(int index){
        this.index = index;
    }

    public int index(){
        return index;
    }
}
