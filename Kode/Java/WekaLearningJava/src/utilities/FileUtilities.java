package utilities;

import java.io.*;
import java.nio.charset.Charset;
import java.security.Permission;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Aleksander on 07.03.14.
 */
public class FileUtilities {

    public static void splitFile(String file, int numOfOutputFiles, String baseFolder, String baseFileName){
        int linesPerFile = countLines(file)/numOfOutputFiles;

        try {
            InputStream fis = null;
            BufferedReader br = null;
            String line;

            fis = new FileInputStream(file);
            br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));

            int currFileNumber = 1;
            int currLine = 0;

            PrintWriter writer = new PrintWriter(getFullFilePathForSplitFile(baseFolder,baseFileName,currFileNumber));

            while ((line = br.readLine()) != null && currLine <= currFileNumber*linesPerFile ) {
                int tmpCurrFileNumber = (currLine / linesPerFile) + 1;
                if(tmpCurrFileNumber > currFileNumber){
                    writer.close();
                    writer = new PrintWriter(getFullFilePathForSplitFile(baseFolder,baseFileName,tmpCurrFileNumber));
                    currFileNumber = tmpCurrFileNumber;
                }
                writer.println(line);
                currLine++;
            }
            writer.close();
            br.close();
            fis.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getFullFilePathForSplitFile(String baseFolder, String baseFileName, int currFileNumber){
        return baseFolder + "\\" + baseFileName + " - part " + String.format("%03d", currFileNumber) + ".txt";
    }

    public static int countLines(String filename){
        try{
            InputStream is = new BufferedInputStream(new FileInputStream(filename));
            try {
                byte[] c = new byte[1024];
                int count = 0;
                int readChars = 0;
                boolean empty = true;
                while ((readChars = is.read(c)) != -1) {
                    empty = false;
                    for (int i = 0; i < readChars; ++i) {
                        if (c[i] == '\n') {
                            ++count;
                        }
                    }
                }
                return (count == 0 && !empty) ? 1 : count+1;
            } finally {
                is.close();
            }
        }catch(Exception e){
            return -1;
        }
    }


    /**
     * Reads all lines of a file and returns them. Do not use on really large files.
     * @param file
     * @return all lines of the file.
     * @throws IOException
     */
    public static String[] readLines(String file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String[] lines = new String[countLines(file)];

        String line;
        int i = 0;
        while ((line = br.readLine()) != null) {
            lines[i] = line;
            i++;
        }
        br.close();

        return lines;
    }
}
