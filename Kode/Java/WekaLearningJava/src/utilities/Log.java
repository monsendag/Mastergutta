package utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Erlend on 23.03.14.
 */
public class Log {

    private BufferedWriter output;
    private static Log _log;

    public Log(File file){
        try {
            output = new BufferedWriter(new FileWriter(file, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTimeStamp(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static synchronized void slog(String message){
        if(_log==null) _log = createRandom();
        _log.log(message);
    }

    public synchronized void log(String message){
        try {
            System.out.println(message);
            output.write(message);
            output.newLine();
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Log createRandom() {
        File path = new File(Data.getWekaFile("log"));
        try {
            File logFile = File.createTempFile("log", ".txt", path);
            return new Log(logFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Log createTimeStamped() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        Calendar cal = Calendar.getInstance();
        File path = new File(Data.getWekaFile("log", "log " + dateFormat.format(cal.getTime()) + ".txt"));
        return new Log(path);
    }

    public static Log createNamed(String filename) {
        File path = new File(Data.getWekaFile("log", filename));
        return new Log(path);
    }

    public static Log createPath(String filepath) {
        File path = new File(filepath);
        return new Log(path);
    }
}
