package utilities;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Erlend on 02.04.2014.
 */
public class FileTools {
    public static File[] endsWith(File[] files, String s) {
        if(files == null) return null;
        ArrayList<File> matches = new ArrayList<File>();
        for(int i = 0; i < files.length; i++)
            if(files[i].getAbsolutePath().endsWith(s))
                matches.add(files[i]);
        File[] am = new File[matches.size()];
        for(int i = 0; i < am.length; i++) am[i] = matches.get(i);
        return am;
    }

    public static File[] startsWith(File[] files, String s) {
        if(files == null) return null;
        ArrayList<File> matches = new ArrayList<File>();
        for(int i = 0; i < files.length; i++)
            if(files[i].getName().startsWith(s))
                matches.add(files[i]);
        File[] am = new File[matches.size()];
        for(int i = 0; i < am.length; i++) am[i] = matches.get(i);
        return am;
    }
}
