package utilities;

/**
 * Created by Erlend on 27.02.14.
 */
public class StopWatch {

    long _last = Long.MIN_VALUE;

    /***
     * Initialize a StopWatch object and print the given text.
     * @param text
     */
    public StopWatch(String text){
        this();
        if(text!=null && !text.equals(""))
            Log.slog(text);
    }

    /***
     * Initialize a StopWatch object.
     */
    public StopWatch(){
        reset();
    }

    /***
     * Reset the StopWatch.
     */
    public void reset(){
        _last = System.currentTimeMillis();
    }

    /***
     * Print the given text with the passed time (in milliseconds). Uses
     * String.format with the duration as the first parameter. If no format
     * strings are given, the duration is added as a prefix with the default
     * format '%,d ms'.
     * The passed time is reset when calling this method.
     * @param text
     */
    public void log(String text) {
        Log.slog(get(text));
    }

    public String get(String text) {
        long dur = elapsed();
        reset();
        if(text.contains("X")) text = text.replace("X","%,d");
        if(text.contains("%"))
            return String.format(text, dur);
        else
            return text + " " + String.format("%,d ms.", dur);
    }

    public long elapsed() {
        return System.currentTimeMillis() - _last;
    }
}
