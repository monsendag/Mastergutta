package utilities;

/**
 * Created by aleksandersjafjell on 3/1/14.
 */
public class StopWatch2 {

    long _last = Long.MIN_VALUE;

    public StopWatch2(String text){
        if(text!="")
            System.out.println(text);
        _last = System.currentTimeMillis();
    }

    public StopWatch2(){
        this("");
    }

    /**
     * Print the time of the stopwatch.
     * @param text Insert XXXXX where you want the time to be inserted. If empty, only time is printed in seconds.
     */
    public void log(String text) {
        long dur = System.currentTimeMillis() - _last;
        String time = String.valueOf((int) dur/1000F);

        if(text.isEmpty()){
            System.out.println(time);
        }else{
            text = text.replaceAll("XXXXX", time);
            System.out.println(text);
        }
    }

    /**
     * Returns the duration of the timer so far. Call does not reset timer.
      * @return time in milliseconds.
     */
    public long duration(){
        return System.currentTimeMillis() - _last;
    }
}
