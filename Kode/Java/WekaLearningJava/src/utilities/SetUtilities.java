package utilities;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by aleksandersjafjell on 3/2/14.
 */
public class SetUtilities {

    public static <T> Set<Set<T>> generatePowerSet(Set<T> originalSet) {
        Set<Set<T>> sets = new HashSet<Set<T>>();
        if (originalSet.isEmpty()) {
            sets.add(new HashSet<T>());
            return sets;
        }
        List<T> list = new ArrayList<T>(originalSet);
        T head = list.get(0);
        Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
        for (Set<T> set : generatePowerSet(rest)) {
            Set<T> newSet = new HashSet<T>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }

    public static <T> void writePowerSet(String file, Set<Set<T>> powerSet, String id){
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for(Set<T> set : powerSet){
          String setString = set.toString().replaceAll("\\s", "");
          if(setString.equals(""))
              continue;
          String row = setString.substring(1, setString.length()-1);
          printWriter.println(row);
        }

        printWriter.close();
    }

    public static <T> void writePowerSet(String file, List<List<T>> powerSet, String id){
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for(List<T> set : powerSet){
            String setString = set.toString().replaceAll("\\s", "");
            if(setString.equals(""))
                continue;
            String row = setString.substring(1, setString.length()-1);
            printWriter.println(row);
        }

        printWriter.close();
    }

    public static List<List<String>> readPowerSet(String file){
        List<List<String>> powerSet = new ArrayList<List<String>>();

        InputStream fis = null;
        BufferedReader br = null;
        String line;

        try {
            fis = new FileInputStream(file);
            br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));

            while ((line = br.readLine()) != null) {
                if(line.equals("")){
                    powerSet.add(new ArrayList<String>());
                    continue;
                }

                List<String> set = new ArrayList<String>();
                List<String> setList = Arrays.asList(line.split(","));
                set.addAll(setList); 
                powerSet.add(set);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
        e.printStackTrace();
        }
        return powerSet;
    }
}
