package utilities;

import classifiers.batch.ABatchClassifier;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
//import java.nio.file.Path;
//import java.nio.file.Paths;

/**
 * Created by Erlend on 27.02.14.
 */
public class Data {

    public static String getDataset(String setID, boolean trainingSet) {
        String pathBase = Data.getWekaFile(ABatchClassifier.BatchFolder.finalDataSet.name(), setID);
        return pathBase + (trainingSet ? "train" : "test") + ".csv";
    }

    public enum Users{
        Erlend,
        Aleksander,
        Simen,
        AleksanderMac,
        DagServer,
        ErlendSkule,
        Server,
        Amazon,
        Unknown
    }

    public static Users getUser() {
        try {
            String name = InetAddress.getLocalHost().getHostName().toLowerCase();
            if(name.equals("masterpcen"))
                return Users.Aleksander;
            if(name.equals("erlendx"))
                return Users.Erlend;
            if(name.equals("skogen"))
                return Users.Simen;
            if(name.equals("aleksanders-macbook-pro.local"))
                return Users.AleksanderMac;
            if(name.equals("lyon"))
                return Users.DagServer;
            if(name.equals("stud3071"))
                return Users.ErlendSkule;
            if(name.equals("stud3051"))
                return Users.Server;
            if(name.equals("ip-172-31-18-151.ec2.internal"))
                return Users.Amazon;
        } catch (UnknownHostException e) {
        }

        return Users.Amazon;
    }

    public static String getDataDirectory() {
        switch(getUser()){
            case Erlend:
                return "G:\\MasterData\\";
            case Aleksander:
                return "D:\\Master\\RepoDataSync\\";
            case AleksanderMac:
                return "/Users/aleksandersjafjell/Documents/Mastergutta/Data/";
            case Simen:
                return "C:\\Users\\simen\\Master\\";
            case DagServer:
                return "/home/shomed/d/dageinm/erlend/";
            case ErlendSkule:
                return "C:\\Users\\erlendah\\Desktop\\Master\\";
            case Server:
                return "C:\\Data\\";
            case Amazon:
                return "/home/ec2-user/data/";
        }
        return null;
    }

    /***
     * Returns a file found within the data directory. Use multiple strings for sub directories.
     * Example: getDataFile("ltt", "file.arff") returns something like "C:\data\ltt\file.arff".
     * This method is platform independent.
     * @param pathParts The relative path from the data directory to the wanted file.
     * @return
     */
    public static String getDataFile(String... pathParts){
        //return Paths.get(getDataDirectory(), pathParts).toAbsolutePath().toString();
        String s = getDataDirectory();
        for (int i = 0; i < pathParts.length; i++) {
            s += pathParts[i] + (i < pathParts.length - 1 ? File.separator : "");
        }
        return s;
    }

    /***
     * Returns a file found within the weka directory. Use multiple strings for sub directories.
     * Example: getDataFile("ltt", "file.arff") returns something like "C:\data\weka\ltt\file.arff".
     * NOTE: This method assumes that there is a subfolder named "weka" within your data directory.
     * This method is platform independent.
     * @param pathParts The relative path from the weka directory to the wanted file.
     * @return
     */
    public static String getWekaFile(String... pathParts) {
        String[] p = new String[pathParts.length+1];
        p[0]="weka";
        for(int i = 1; i < p.length; i++) p[i] = pathParts[i-1];
        return getDataFile(p);
    }
}
