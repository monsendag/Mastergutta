﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.AtB.RealTime;
using Utilities.AtB.Utilities;

namespace RealTimeLogger
{
    class Program
    {
        //Before IDs were fucked: private static readonly string[] StopsToCheck = "100990;100955;100984;101131;102497;101777;101776;101340;101303;101335;101299;101487;101521;101481;102012;102313;101517;101651;101678;101639;101502;101477;100327;100300;102846;102813;102026;102164;102184;102333;102519;102490;102835;100149;100656;100653;100337;100307;100487;100458;100815;100782;100617;100644;100636;100611;100634;100822;100787;100976;100950;100970;100944;102501;102475;100977;100768;101503;101478;100707;101496;101471;100821;100786;101627;101831;101805;101780;102315;102017;101799;102334;102008;101984;102621;100812;100965;100956;100997;100960;101157;101124;101341;101304;101979;100952;101328;101486;101518;101652;101672;101825;101859;101995;100819;100784;102187;102153;102328;100820;100785;101911;101910;102641;102661;102816;102660;102815;102847;102814;100177;100147;100136;100492;100299;100489;100460;100480;100479;100451;100454;100805;100606;100794;100773;101105;101135;101115;101134;101113;101318;101295;101292;101313;101513;101647;101293;101509;101289;101312;101290;100623;100766;101500;101475;101508;101288;101494;101469;101507;101497;101472;101103;101832;101806;101830;101804;102010;101986;101097;102147;100638;100613;101922;101909;102314;101342;101305;100987;101520;101511;101645;101677;101638;102032;102357;102837;102804;101580;102099;101104;101446;100331;100302;100171;100140;100172;100141;100168;100166;101829;101803;100971;100945;101141;101120;101315;101666;101467;102007;101983;102177;101977;102172".Split(';').Distinct().ToArray();
        private static readonly string[] StopsToCheck = "100136;100140;100141;100147;100149;100166;100168;100171;100172;100177;100299;100300;100302;100307;100327;100331;100337;100449;100452;100456;100458;100477;100478;100485;100487;100490;100604;100609;100611;100615;100621;100632;100634;100636;100642;100651;100654;100705;100764;100766;100771;100780;100782;100783;100784;100785;100792;100803;100810;100813;100817;100818;100819;100820;100942;100943;100948;100950;100953;100954;100958;100963;100968;100969;100974;100975;100982;100985;100988;100995;101095;101101;101102;101103;101111;101113;101118;101122;101129;101132;101133;101139;101155;101286;101287;101288;101290;101291;101293;101297;101301;101302;101303;101310;101311;101313;101316;101326;101333;101338;101339;101340;101444;101465;101467;101469;101470;101473;101475;101476;101479;101484;101485;101492;101494;101495;101498;101500;101501;101505;101506;101507;101509;101511;101515;101516;101518;101519;101578;101625;101636;101637;101643;101645;101649;101650;101664;101670;101675;101676;101774;101775;101778;101797;101801;101802;101803;101804;101823;101827;101828;101829;101830;101857;101907;101908;101909;101921;101975;101977;101981;101982;101984;101993;102005;102006;102008;102010;102015;102024;102030;102097;102145;102151;102162;102170;102175;102182;102185;102311;102312;102313;102326;102331;102332;102355;102473;102488;102495;102499;102517;102619;102639;102658;102659;102802;102811;102812;102813;102814;102833;102835;102844;102845".Split(';').Distinct().ToArray();
        private const int ThreadCount = 30;

        static void Main(string[] args)
        {
            Trace.Listeners.Clear();
            Trace.Listeners.Add(new TextWriterTraceListener(Data.GetDataFile(Data.DataFolders.RealTime, "log_" + Environment.MachineName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log"))
            {
                Name = "TextLogger",
                TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
            });
            Trace.Listeners.Add(new ConsoleTraceListener(false)
            {
                TraceOutputOptions = TraceOptions.DateTime
            });
            Trace.AutoFlush = true;

            do
            {
                try
                {
                    FetchRealTime();
                }
                catch (Exception ex)
                {
                    LogException(ex);
                }
            } while (true);
        }

        static readonly List<string> lines = new List<string>();
        private static void FetchRealTime()
        {
            var dt = DateTime.Now;
            var failed = 0;
            var success = 0;
            var counter = 0;

            ServicePointManager.DefaultConnectionLimit = ThreadCount;

            var threads = new List<Thread>();
            for (var i = 0; i < ThreadCount; i++)
            {
                var thread = new Thread(() =>
                {
                    var wc = new WebClient();
                    var run = true;
                    do
                    {
                        string stop;
                        lock (StopsToCheck)
                        {
                            if (counter >= StopsToCheck.Length)
                            {
                                run = false;
                                break;
                            }
                            stop = StopsToCheck[counter++];
                        }
                        try
                        {
                            var queryTime = DateTime.Now;
                            var items = RealTimeItem.Realtime(stop, wc).Where(p => p.Type == RealTimeItem.RealTimeTypes.Prediction).GroupBy(p => p.Line);
                            lock (lines)
                            {
                                lines.Add(stop + "|" + queryTime.Ticks + "|" + string.Join("|", items.Select(p => p.Key + ";" + string.Join(";", p.Select(c => c.Arrival.Ticks)))));
                            }
                            success++;
                        }
                        catch (Exception ex)
                        {
                            failed++;
                            WriteLine("Stop " + stop + " failed: " + ex.Message);
                        }
                    } while (run);
                });
                threads.Add(thread);
                thread.Start();
            }

            while (threads.Any(p => p.IsAlive))
            {
                Thread.Sleep(1000);
                if (DateTime.Now.Subtract(dt).TotalSeconds > 55)
                {
                    WriteLine("  > WARNING: TIMEOUT ABORT");
                    threads.Where(p=>p.IsAlive).ToList().ForEach(p=>p.Abort());
                    break;
                }
            }

            WriteLine("{3}: Fetched {0} stops (failed: {1}) in {2:n2} ms.", success, failed, DateTime.Now.Subtract(dt).TotalMilliseconds, DateTime.Now.ToString("HH:mm:ss"));
            if (success != 204)
            {
                WriteLine("  > Weird? {0}, {1}", counter, lines.Count);
            }

            WriteToFile();

            var exportFile = System.IO.Path.Combine(Data.GetDataFolder(Data.DataFolders.RealTime), "export.txt");
            if (System.IO.File.Exists(exportFile))
            {
                System.IO.File.Delete(exportFile);
                ExportData();
            }

            while (DateTime.Now.Subtract(dt).TotalSeconds < 60)
                Thread.Sleep(1000);
        }

        private static void WriteLine(string format, params object[] args)
        {
            Trace.WriteLine(string.Format(format, args));
        }

        private static void LogException(Exception ex)
        {
            System.IO.File.AppendAllText(System.IO.Path.Combine(Data.GetDataFolder(Data.DataFolders.RealTime), "errors_" + Environment.MachineName + ".txt"),
                    DateTime.Now.ToString() + Environment.NewLine +
                ex.Message + Environment.NewLine +
                ex.StackTrace + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine, Encoding.Default);
        }

        private static void ExportData()
        {
            try
            {
                var name = GetDataFile().Replace(".txt", "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".txt");
                System.IO.File.Move(GetDataFile(), name);
            }
            catch (Exception ex)
            {
                WriteLine(ex.Message);
            }
        }

        private static void WriteToFile()
        {
            lock (lines)
            {
                System.IO.File.AppendAllText(GetDataFile(), string.Join(Environment.NewLine, lines) + Environment.NewLine, Encoding.Default);
                lines.Clear();
            }
        }

        private static string GetDataFile()
        {
            return System.IO.Path.Combine(Data.GetDataFolder(Data.DataFolders.RealTime), "realtime_" + Environment.MachineName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");
        }
    }
}
