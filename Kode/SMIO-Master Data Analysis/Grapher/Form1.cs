﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using Utilities.AtB.LinksTravelTime;
using Utilities.AtB.Utilities;
using Utilities.AtB.General;
using Utilities.Yr;
using Yr_Daily_Weather_Data_Fetcher;
using RBK_Data_Fetcher;
using Utilities.Soccer;
using Utilities.Database;

namespace FormApp
{

    public partial class Form1 : Form
    {

        public IEnumerable<WeatherObject> Weather { get; set; }
        public SimpleWeatherObject[] Weather2013 { get; set; }
        public IEnumerable<SoccerGame> Games { get; set; }
        public SoccerGame[] Games2013 { get; set; }
        public SMIODataContext DataBase { get; set; }
        ToolTip tooltip = new ToolTip();
        Point? clickPosition = null;
        public IQueryable<IGrouping<DateTime?, LTTItem>> LTTDictionary { get; set; }
        public string FromStopID;
        public string ToStopID;

        public Form1()
        {
            InitializeComponent();
        }

        public double Correlation(double[] array1, double[] array2)
        {
            double[] array_xy = new double[array1.Length];
            double[] array_xp2 = new double[array1.Length];
            double[] array_yp2 = new double[array1.Length];
            for (int i = 0; i < array1.Length; i++)
                array_xy[i] = array1[i] * array2[i];
            for (int i = 0; i < array1.Length; i++)
                array_xp2[i] = Math.Pow(array1[i], 2.0);
            for (int i = 0; i < array1.Length; i++)
                array_yp2[i] = Math.Pow(array2[i], 2.0);
            double sum_x = 0;
            double sum_y = 0;
            foreach (double n in array1)
                sum_x += n;
            foreach (double n in array2)
                sum_y += n;
            double sum_xy = 0;
            foreach (double n in array_xy)
                sum_xy += n;
            double sum_xpow2 = 0;
            foreach (double n in array_xp2)
                sum_xpow2 += n;
            double sum_ypow2 = 0;
            foreach (double n in array_yp2)
                sum_ypow2 += n;
            double Ex2 = Math.Pow(sum_x, 2.00);
            double Ey2 = Math.Pow(sum_y, 2.00);

            return (array1.Length * sum_xy - sum_x * sum_y) /
            Math.Sqrt((array1.Length * sum_xpow2 - Ex2) * (array1.Length * sum_ypow2 - Ey2));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chart1.ChartAreas.Add("ChartArea");
            chart1.ChartAreas["ChartArea"].AxisX.Minimum = 0;
            chart1.ChartAreas["ChartArea"].AxisX.Interval = 1;
            chart1.ChartAreas["ChartArea"].AxisY.Interval = 2;
            chart1.ChartAreas["ChartArea"].AxisY.Minimum = -20;
            chart1.ChartAreas["ChartArea"].AxisY.Maximum = 30;
            chart1.ChartAreas["ChartArea"].AxisX.MajorGrid.LineColor = System.Drawing.ColorTranslator.FromHtml("#F0F0F0");
            chart1.ChartAreas["ChartArea"].AxisY.MajorGrid.LineColor = System.Drawing.ColorTranslator.FromHtml("#F0F0F0");
            chart1.ChartAreas["ChartArea"].BackColor = Color.White;
            chart1.Series.Add("Temperature");
            chart1.Series.Add("Precipitation");
            chart1.Series.Add("Wind Extreme");
            chart1.Series.Add("Games");
            chart1.Legends.Add("Legend1");
            chart1.Series["Temperature"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series["Precipitation"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series["Wind Extreme"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series["Games"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            chart1.Series["Precipitation"].Color = Color.Orange;
            chart1.Series["Wind Extreme"].Color = Color.Red;
            chart1.Series["Games"].Color = Color.Black;
            checkedListBox1.SetItemCheckState(0, CheckState.Checked);
            checkedListBox1.SetItemCheckState(1, CheckState.Checked);
            checkedListBox1.SetItemCheckState(2, CheckState.Checked);

            chart2.ChartAreas.Add("ChartArea2");
            chart2.ChartAreas["ChartArea2"].AxisX.Minimum = 0;
            chart2.ChartAreas["ChartArea2"].AxisX.Interval = 1;
            chart2.ChartAreas["ChartArea2"].AxisY.Minimum = 0;
            chart2.ChartAreas["ChartArea2"].AxisX.MajorGrid.LineColor = System.Drawing.ColorTranslator.FromHtml("#F0F0F0");
            chart2.ChartAreas["ChartArea2"].AxisY.MajorGrid.LineColor = System.Drawing.ColorTranslator.FromHtml("#F0F0F0");
            chart2.ChartAreas["ChartArea2"].BackColor = Color.White;
            chart2.Series.Add("LTTMedian");
            chart2.Series.Add("LTTMin");
            chart2.Series.Add("LTTMax");
            chart2.Series.Add("LTTAvgHour");
            chart2.Series.Add("LTTMaxHour");
            chart2.Series.Add("LTTMinHour");
            chart2.Series.Add("LTTMedianHour");
            chart2.Legends.Add("Legend2");
            chart2.Series["LTTMedian"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart2.Series["LTTMin"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart2.Series["LTTMax"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;         

            //Queries
            DataBase = new SMIODataContext();
            var weatherJanuary = DataBase.WeatherObjects.
                                    Where(w => w.Time.Month == 8 && w.Time.Hour >= 7 && w.Time.Hour <= 17).
                                    GroupBy(w => DbFunctions.TruncateTime(w.Time)).
                                    Select(w => new
                                    {
                                        Time = w.Key,
                                        MinTemp = (double)w.Min(t => t.Temp),
                                        Precipitation = (double)w.Sum(t => t.Precipitation),
                                        Windextreme = (double)w.Average(t => t.WindExtreme)
                                    }).ToArray();                 

            //Plot points in chart
            foreach (var item in weatherJanuary)
            {
                int day = item.Time.Value.Day;
                chart1.Series["Temperature"].Points.AddXY(day, item.MinTemp);
                chart1.Series["Precipitation"].Points.AddXY(day, item.Precipitation);
                chart1.Series["Wind Extreme"].Points.AddXY(day, item.Windextreme);
            }

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            var startDate = monthCalendar1.SelectionStart.Date;
            var endDate = monthCalendar1.SelectionEnd.Date;

            chart1.ChartAreas["ChartArea"].AxisX.Minimum = startDate.Day;
            chart1.ChartAreas["ChartArea"].AxisX.Maximum = endDate.Day;
            chart2.ChartAreas["ChartArea2"].AxisX.Minimum = startDate.Day;
            chart2.ChartAreas["ChartArea2"].AxisX.Maximum = endDate.Day;
            chart1.Series["Temperature"].Points.Clear();
            chart1.Series["Precipitation"].Points.Clear();
            chart1.Series["Wind Extreme"].Points.Clear();
            chart1.Series["Games"].Points.Clear();
            chart2.Series["LTTMedian"].Points.Clear();
            chart2.Series["LTTMin"].Points.Clear();
            chart2.Series["LTTMax"].Points.Clear();
            chart2.Series["LTTAvgHour"].Points.Clear();
            chart2.Series["LTTMaxHour"].Points.Clear();
            chart2.Series["LTTMinHour"].Points.Clear();
            chart2.Series["LTTMedianHour"].Points.Clear();

            var weatherMonth = DataBase.WeatherObjects.
                               Where(w => DbFunctions.DiffDays(startDate, w.Time) >= 0 && DbFunctions.DiffDays(w.Time, endDate) >= 0 && w.Time.Hour >= 7 && w.Time.Hour <= 17).
                               GroupBy(w => DbFunctions.TruncateTime(w.Time)).
                               Select(w => new
                               {
                                   Time = w.Key,
                                   MinTemp = (double)w.Min(t => t.Temp),
                                   Precipitation = (double)w.Sum(t => t.Precipitation),
                                   Windextreme = (double)w.Average(t => t.WindExtreme)
                               }).ToArray();


            foreach (var item in weatherMonth)
            {
                int day = item.Time.Value.Day;
                chart1.Series["Temperature"].Points.AddXY(day, item.MinTemp);
                chart1.Series["Precipitation"].Points.AddXY(day, item.Precipitation);
                chart1.Series["Wind Extreme"].Points.AddXY(day, item.Windextreme);
                chart1.Series["Temperature"].Enabled = checkedListBox1.CheckedIndices.Contains(0) ? true : false;
                chart1.Series["Precipitation"].Enabled = checkedListBox1.CheckedIndices.Contains(1) ? true : false;
                chart1.Series["Wind Extreme"].Enabled = checkedListBox1.CheckedIndices.Contains(2) ? true : false;
            }

            if (startDate.DayOfYear == endDate.DayOfYear) //valg av én dag
            {
                var q = DataBase.LTTItems.
                                    Where(t => DbFunctions.DiffDays(startDate, t.ActualDeparture) == 0
                                        && t.BusStopFrom.ID == FromStopID && t.BusStopTo.ID == ToStopID).
                                    GroupBy(t => t.ActualDeparture.Hour);
                chart2.ChartAreas["ChartArea2"].AxisX.Minimum = 0;
                chart2.ChartAreas["ChartArea2"].AxisX.Maximum = 24;

                int pointIndex = 0;
                foreach (var group in q)
                {
                    int hour = group.Key;
                    int departures = group.Count();
                    int median = group.OrderBy(w => w.TravelTime).ToList().ElementAt(departures / 2).TravelTime;
                    Console.WriteLine("Group: " + hour);
                    chart2.Series["LTTAvgHour"].Points.AddXY(hour, group.Average(t => t.TravelTime));
                    chart2.Series["LTTMaxHour"].Points.AddXY(hour, group.Max(t => t.TravelTime));
                    chart2.Series["LTTMedianHour"].Points.AddXY(hour, median);
                    chart2.Series["LTTAvgHour"].Points[pointIndex].AxisLabel = "kl: " + hour + "-" + (hour + 1) + "\n#: " + departures;                           


                    pointIndex++;
                }
            }
            else  //valg av flere dager
            {
                LTTDictionary = DataBase.LTTItems.
                                    Where(t => DbFunctions.DiffDays(startDate, t.ActualDeparture) >= 0 && DbFunctions.DiffDays(t.ActualDeparture, endDate) >= 0 
                                        && t.ActualDeparture.Hour >= 7 && t.ActualDeparture.Hour <= 17 
                                        && t.BusStopFrom.ID == FromStopID && t.BusStopTo.ID == ToStopID).
                                    GroupBy(t => DbFunctions.TruncateTime(t.ActualDeparture));
               
                foreach (var group in LTTDictionary)
                {
                    int day = group.Key.Value.Day;
                    int departures = group.Count();
                    int min = group.Min(w => w.TravelTime);
                    int max = group.Max(w => w.TravelTime);
                    int median = group.OrderBy(w => w.TravelTime).ToList().ElementAt(departures / 2).TravelTime;
                    chart2.Series["LTTMedian"].Points.AddXY(day, median);
                    chart2.Series["LTTMin"].Points.AddXY(day, min);
                    chart2.Series["LTTMax"].Points.AddXY(day, max);
                }
            }

            //var series1 = weatherMonth.Select(w => (double)w.MinTemp).ToArray();
            //var series2 = weatherMonth.Select(w => (double)w.Precipitation).ToArray();
            //Console.WriteLine("Correlation temp/precipitation: " + Correlation(series1, series2));

            if (Games != null)
            {
                var gamesSelection = Games.Where(w => w.Time.Date >= startDate && w.Time.Date <= endDate).ToArray();
                foreach (var game in gamesSelection)
                {
                    chart1.Series["Games"].Points.AddXY(game.Time.Day, game.Viewers / 1000);
                    chart1.Series["Games"].Enabled = true;
                }
            }

        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            chart1.Series[e.Index].Enabled = (e.NewValue == CheckState.Checked) ? true : false;
        }

        private void chart1_MouseClick(object sender, MouseEventArgs e)
        {
            var pos = e.Location;
            clickPosition = pos;
            var results = chart1.HitTest(pos.X, pos.Y, false, ChartElementType.PlottingArea);
            foreach (var result in results)
            {
                if (result.ChartElementType == ChartElementType.PlottingArea)
                {
                    var xVal = result.ChartArea.AxisX.PixelPositionToValue(pos.X);
                    var yVal = result.ChartArea.AxisY.PixelPositionToValue(pos.Y);

                    tooltip.Show("X=" + xVal + ", Y=" + yVal, this.chart1, e.Location.X, e.Location.Y - 15);
                }
            }
        }

        private void chart2_MouseClick(object sender, MouseEventArgs e)
        {
            var pos = e.Location;
            clickPosition = pos;
            var hit = chart2.HitTest(pos.X, pos.Y);
            if (hit != null && monthCalendar1.SelectionStart.Date != monthCalendar1.SelectionEnd.Date)
            {               
                var selectedDay = LTTDictionary.SingleOrDefault(w => w.Key.Value.Day == hit.PointIndex + 1);
                if (selectedDay != null)
                {
                    int maxTravelTime = selectedDay.Max(w => w.TravelTime);
                    int minTravelTime = selectedDay.Min(w => w.TravelTime);
                    var maxItem = selectedDay.First(w => w.TravelTime == maxTravelTime);
                    var minItem = selectedDay.First(w => w.TravelTime == minTravelTime);
                    var arrivalTime = maxItem.ActualArrival.TimeOfDay;
                    var maxDepartureTime = maxItem.ActualDeparture.TimeOfDay;
                    var minDepartureTime = minItem.ActualDeparture.TimeOfDay;
                    var stopTime = maxItem.StopTime;
                    var tripID = maxItem.Trip;
                    tooltip.Show("Departure of max travel time: " + maxDepartureTime + "\nDeparture of min travel time: " + minDepartureTime, this.chart2, e.Location.X, e.Location.Y);
                }
            }
        }

        private void textBoxFrom_TextChanged(object sender, EventArgs e)
        {
            fromListBox.Items.Clear();
            toListBox.Items.Clear();
            string requestString = textBoxFrom.Text.ToLower();
            List<BusStop> stops = DataBase.BusStops.Where(s => s.Name.ToLower().StartsWith(requestString)).ToList();
            foreach (var stop in stops)
            {
                fromListBox.Items.Add(stop);
            }
        }

        private void fromListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            toListBox.Items.Clear();
            var startDate = monthCalendar1.SelectionStart.Date;
            int startMonth = startDate.Month;
            var endDate = monthCalendar1.SelectionEnd.Date;
            var selectedBusStopID = ((BusStop)fromListBox.SelectedItem).ID;
            var query = DataBase.LTTItems.
                        Where(t => t.BusStopFrom.ID == selectedBusStopID
                            && DbFunctions.DiffDays(t.ActualArrival, startDate) == 0).
                        Select(t => t.BusStopTo).
                        Distinct().ToList();
            foreach (var stop in query)
            {
                toListBox.Items.Add(stop);
            }
        }

        private void toListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var startDate = monthCalendar1.SelectionStart.Date;
            var endDate = monthCalendar1.SelectionEnd.Date;
            chart2.Series["LTTMedian"].Points.Clear();
            chart2.Series["LTTMin"].Points.Clear();
            chart2.Series["LTTMax"].Points.Clear();
            chart2.Series["LTTAvgHour"].Points.Clear();
            chart2.Series["LTTMaxHour"].Points.Clear();
            chart2.Series["LTTMinHour"].Points.Clear();
            chart2.Series["LTTMedianHour"].Points.Clear();
            FromStopID = ((BusStop)fromListBox.SelectedItem).ID;
            ToStopID = ((BusStop)toListBox.SelectedItem).ID;

            LTTDictionary = DataBase.LTTItems.
                                Where(t => DbFunctions.DiffDays(startDate, t.ActualDeparture) >= 0 && DbFunctions.DiffDays(t.ActualDeparture, endDate) >= 0
                                    && t.ActualDeparture.Hour >= 7 && t.ActualDeparture.Hour <= 17
                                    && t.BusStopFrom.ID == FromStopID && t.BusStopTo.ID == ToStopID).
                                GroupBy(t => DbFunctions.TruncateTime(t.ActualDeparture));


            foreach (var group in LTTDictionary)
            {
                int day = group.Key.Value.Day;
                int departures = group.Count();
                int min = group.Min(w => w.TravelTime);
                int max = group.Max(w => w.TravelTime);
                int median = group.OrderBy(w => w.TravelTime).ToList().ElementAt(departures / 2).TravelTime;
                chart2.Series["LTTMedian"].Points.AddXY(day, median);
                chart2.Series["LTTMin"].Points.AddXY(day, min);
                chart2.Series["LTTMax"].Points.AddXY(day, max);
            }
        }

    }
}
