﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormApp
{
    public class SimpleWeatherObject
    {

        public DateTime Time { get; set; }
        public double MinTemp { get; set; }
        public double Precipitation { get; set; }
        public double Windextreme { get; set; }
    }
}
