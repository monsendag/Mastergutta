﻿namespace GPSLogger
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.webBrowser3 = new System.Windows.Forms.WebBrowser();
            this.webBrowser4 = new System.Windows.Forms.WebBrowser();
            this.webBrowser5 = new System.Windows.Forms.WebBrowser();
            this.webBrowser6 = new System.Windows.Forms.WebBrowser();
            this.webBrowser7 = new System.Windows.Forms.WebBrowser();
            this.webBrowser8 = new System.Windows.Forms.WebBrowser();
            this.webBrowser9 = new System.Windows.Forms.WebBrowser();
            this.webBrowser10 = new System.Windows.Forms.WebBrowser();
            this.webBrowser11 = new System.Windows.Forms.WebBrowser();
            this.webBrowser12 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(12, 12);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(250, 147);
            this.webBrowser1.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // webBrowser2
            // 
            this.webBrowser2.Location = new System.Drawing.Point(268, 12);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.Size = new System.Drawing.Size(250, 147);
            this.webBrowser2.TabIndex = 1;
            // 
            // webBrowser3
            // 
            this.webBrowser3.Location = new System.Drawing.Point(12, 165);
            this.webBrowser3.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser3.Name = "webBrowser3";
            this.webBrowser3.Size = new System.Drawing.Size(250, 161);
            this.webBrowser3.TabIndex = 2;
            // 
            // webBrowser4
            // 
            this.webBrowser4.Location = new System.Drawing.Point(268, 165);
            this.webBrowser4.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser4.Name = "webBrowser4";
            this.webBrowser4.Size = new System.Drawing.Size(250, 161);
            this.webBrowser4.TabIndex = 3;
            // 
            // webBrowser5
            // 
            this.webBrowser5.Location = new System.Drawing.Point(524, 165);
            this.webBrowser5.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser5.Name = "webBrowser5";
            this.webBrowser5.Size = new System.Drawing.Size(250, 161);
            this.webBrowser5.TabIndex = 5;
            // 
            // webBrowser6
            // 
            this.webBrowser6.Location = new System.Drawing.Point(524, 12);
            this.webBrowser6.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser6.Name = "webBrowser6";
            this.webBrowser6.Size = new System.Drawing.Size(250, 147);
            this.webBrowser6.TabIndex = 4;
            // 
            // webBrowser7
            // 
            this.webBrowser7.Location = new System.Drawing.Point(780, 165);
            this.webBrowser7.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser7.Name = "webBrowser7";
            this.webBrowser7.Size = new System.Drawing.Size(250, 161);
            this.webBrowser7.TabIndex = 7;
            // 
            // webBrowser8
            // 
            this.webBrowser8.Location = new System.Drawing.Point(780, 12);
            this.webBrowser8.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser8.Name = "webBrowser8";
            this.webBrowser8.Size = new System.Drawing.Size(250, 147);
            this.webBrowser8.TabIndex = 6;
            // 
            // webBrowser9
            // 
            this.webBrowser9.Location = new System.Drawing.Point(524, 332);
            this.webBrowser9.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser9.Name = "webBrowser9";
            this.webBrowser9.Size = new System.Drawing.Size(250, 157);
            this.webBrowser9.TabIndex = 10;
            // 
            // webBrowser10
            // 
            this.webBrowser10.Location = new System.Drawing.Point(268, 332);
            this.webBrowser10.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser10.Name = "webBrowser10";
            this.webBrowser10.Size = new System.Drawing.Size(250, 157);
            this.webBrowser10.TabIndex = 9;
            // 
            // webBrowser11
            // 
            this.webBrowser11.Location = new System.Drawing.Point(12, 332);
            this.webBrowser11.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser11.Name = "webBrowser11";
            this.webBrowser11.Size = new System.Drawing.Size(250, 157);
            this.webBrowser11.TabIndex = 8;
            // 
            // webBrowser12
            // 
            this.webBrowser12.Location = new System.Drawing.Point(780, 332);
            this.webBrowser12.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser12.Name = "webBrowser12";
            this.webBrowser12.Size = new System.Drawing.Size(250, 157);
            this.webBrowser12.TabIndex = 11;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 534);
            this.Controls.Add(this.webBrowser12);
            this.Controls.Add(this.webBrowser9);
            this.Controls.Add(this.webBrowser10);
            this.Controls.Add(this.webBrowser11);
            this.Controls.Add(this.webBrowser7);
            this.Controls.Add(this.webBrowser8);
            this.Controls.Add(this.webBrowser5);
            this.Controls.Add(this.webBrowser6);
            this.Controls.Add(this.webBrowser4);
            this.Controls.Add(this.webBrowser3);
            this.Controls.Add(this.webBrowser2);
            this.Controls.Add(this.webBrowser1);
            this.Name = "frmMain";
            this.Text = "AtB Data Miner";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private System.Windows.Forms.WebBrowser webBrowser3;
        private System.Windows.Forms.WebBrowser webBrowser4;
        private System.Windows.Forms.WebBrowser webBrowser5;
        private System.Windows.Forms.WebBrowser webBrowser6;
        private System.Windows.Forms.WebBrowser webBrowser7;
        private System.Windows.Forms.WebBrowser webBrowser8;
        private System.Windows.Forms.WebBrowser webBrowser9;
        private System.Windows.Forms.WebBrowser webBrowser10;
        private System.Windows.Forms.WebBrowser webBrowser11;
        private System.Windows.Forms.WebBrowser webBrowser12;
    }
}

