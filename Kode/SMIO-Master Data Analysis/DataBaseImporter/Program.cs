﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Linq.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;
using System.Xml.Linq;
using Utilities.AtB;
using Utilities.AtB.FullTrips;
using Utilities.AtB.GPSLog;
using Utilities.AtB.General;
using Utilities.AtB.Generated;
using Utilities.AtB.Lines;
using Utilities.AtB.LinksTravelTime;
using Utilities.AtB.Passage;
using Utilities.AtB.PassageAtVirtualLoops;
using Utilities.AtB.PassengerCount;
using Utilities.AtB.RealTime;
using Utilities.AtB.Tickets;
using Utilities.AtB.Trips;
using Utilities.AtB.Utilities;
using Utilities.Classifiers;
using Utilities.Database;
using Utilities.Extensions;
using Utilities.Yr;
using Weka_Evaluation_Analyzer;

namespace DataBaseImporter
{
    public static class MercatorProjection
    {
        private static readonly double R_MAJOR = 6378137.0;
        private static readonly double R_MINOR = 6356752.3142;
        private static readonly double RATIO = R_MINOR / R_MAJOR;
        private static readonly double ECCENT = Math.Sqrt(1.0 - (RATIO * RATIO));
        private static readonly double COM = 0.5 * ECCENT;

        private static readonly double DEG2RAD = Math.PI / 180.0;
        private static readonly double RAD2Deg = 180.0 / Math.PI;
        private static readonly double PI_2 = Math.PI / 2.0;

        public static double[] toPixel(double lon, double lat)
        {
            return new double[] { lonToX(lon), latToY(lat) };
        }

        public static double[] toGeoCoord(double x, double y)
        {
            return new double[] { xToLon(x), yToLat(y) };
        }

        public static double lonToX(double lon)
        {
            return R_MAJOR * DegToRad(lon);
        }

        public static double latToY(double lat)
        {
            lat = Math.Min(89.5, Math.Max(lat, -89.5));
            double phi = DegToRad(lat);
            double sinphi = Math.Sin(phi);
            double con = ECCENT * sinphi;
            con = Math.Pow(((1.0 - con) / (1.0 + con)), COM);
            double ts = Math.Tan(0.5 * ((Math.PI * 0.5) - phi)) / con;
            return 0 - R_MAJOR * Math.Log(ts);
        }

        public static double xToLon(double x)
        {
            return RadToDeg(x) / R_MAJOR;
        }

        public static double yToLat(double y)
        {
            double ts = Math.Exp(-y / R_MAJOR);
            double phi = PI_2 - 2 * Math.Atan(ts);
            double dphi = 1.0;
            int i = 0;
            while ((Math.Abs(dphi) > 0.00000000001) && (i < 150))
            {
                double con = ECCENT * Math.Sin(phi);
                dphi = PI_2 - 2 * Math.Atan(ts * Math.Pow((1.0 - con) / (1.0 + con), COM)) - phi;
                phi += dphi;
                i++;
            }
            return RadToDeg(phi);
        }

        private static double RadToDeg(double rad)
        {
            return rad * RAD2Deg;
        }

        private static double DegToRad(double deg)
        {
            return deg * DEG2RAD;
        }
    }

    class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            /*Console.WriteLine(MercatorProjection.xToLon(1154262));
            Console.WriteLine(MercatorProjection.xToLon(9182277));
            var x = MercatorProjection.yToLat(1154262);
            var y = MercatorProjection.yToLat(9182277);

            System.Diagnostics.Process.Start("http://econym.org.uk/gmap/example_plotpoints.htm?q=" + y + "," + x);*/

            if (args.Length > 0 && args[0] == "disc")
            {
                Console.WriteLine("Running exported version.");
            }
            else
            {
                Console.WriteLine("Run disconnected from the Visual Studio debugger? [y/n]");
                var disc = Console.ReadKey();
                if (disc.Key == ConsoleKey.Y)
                {
                    ExportAndRunApplication(new[] {"disc"});
                    return;
                }
            }

            if (args.Length > 0 && args[0] == "export")
            {
                Console.WriteLine("Running exported version.");
                if (Data.User == Data.Users.Erlend)
                    GenerateSql();
                UpdateDatabase();
            }
            else
            {
                while (true)
                {
                    Console.WriteLine("****************************************************************************");
                    Console.WriteLine("Type 'run' to run a separate automated data import (not connected to VS).");
                    Console.WriteLine("Type 'import' to run automated data import (MUST NOT BE INTERRUPTED).");
                    Console.WriteLine("Type 'generate' to generate SQL files from CSV files.");
                    Console.WriteLine("Type 'fetch [yr d1 d2]/[rtgps]/[stops line]' (yyyy-MM-dd) to fetch stuff.");
                    Console.WriteLine("Type 'weka trips/bsp d1 d2' to export a csv file for Weka.");
                    Console.WriteLine("Type 'analyze bsp [s]' to analyze stuff.");
                    Console.WriteLine("Type 'svmcsv/knncsv' to copy results to CSV.");
                    Console.WriteLine("Type 'bus out/in/rid/check' to export/import update bus stops, or update/check RIDs.");
                    Console.WriteLine("Type 'urls d1 d2' (yyyy-MM-dd) to generate Flashnet download urls.");
                    Console.WriteLine("Type 'reset t1, t2, ...' to reset table(s).");
                    Console.WriteLine("Type 'validate [t1, t2, ...]' to validate all or given database tables.");
                    Console.WriteLine("Type 'trips line' to list the trips of a line.");
                    Console.WriteLine("Type 'calculate trips/passages' to calculate stuff.");
                    Console.WriteLine("Type 'heal t1, t2, ...' to heal table(s).");
                    Console.WriteLine("Type 'lt' to list tables.");
                    Console.WriteLine("Type 'dbcount [t1, t2, ...]' to list all or given tables with item counts.");
                    Console.WriteLine("Type 'exit' or close the window to exit.");
                    Console.WriteLine("****************************************************************************");
                    var x = Console.ReadLine().Split(' ');
                    const string f = "yyyy-MM-dd";
                    var cmd = x[0];
                    var parameters = x.Skip(1).ToArray();
                    switch (cmd)
                    {
                        case "exit":
                            return;
                        case "dbcount":
                            RunCommand(() => DbHelper.CountDB(parameters.Select(p => DbHelper.GetDataTypeFromTableName(p.Trim())).ToArray()));
                            break;
                        case "reset":
                            RunCommand(() => DbHelper.Reset(parameters.Select(p => DbHelper.GetDataTypeFromTableName(p.Trim())).ToArray()));
                            break;
                        case "createjobs":
                            RunCommand(() =>
                                {
                                    CreateJobs.Create();
                                    Console.WriteLine("CSV copied to clipboard.");
                                });
                            break;
                        case "svmcsv":
                            RunCommand(() =>
                            {
                                Clipboard.SetText(EvaluationAnalyzer.SvmResultsToCsvCGammaGrid6(EvaluationAnalyzer.SvmCKernelFile));
                                Console.WriteLine("CSV copied to clipboard.");
                            });
                            break;
                        case "knncsv":
                            RunCommand(() =>
                            {
                                Clipboard.SetText(EvaluationAnalyzer.KnnResultsToCsv(EvaluationAnalyzer.KnnFile));
                                Console.WriteLine("CSV copied to clipboard.");
                            });
                            break;
                        case "heal":
                            RunCommand(() => parameters.Select(p => DbHelper.GetDataTypeFromTableName(p.Trim())).ToList().ForEach(p =>
                            {
                                if (p == Data.DataFolders.Buses)
                                    BusStop.HealNames();
                                else if (p == Data.DataFolders.Yr)
                                    WeatherObject.InterpolateWeather();
                                else if (p == Data.DataFolders.RealTime)
                                    RealTimeItem.HealBusStopIDChanges();
                                else
                                    Console.WriteLine("No heal methods for " + p.ToString());
                            }));
                            break;
                        case "analyze":
                            RunCommand(() => {
                                if (parameters[0] == "bsp")
                                    if (parameters.Length > 1)
                                        BusStopPassage.Analyze(parameters[1]);
                                    else
                                    {
                                        var db = new SMIODataContext();
                                        var stopCodes = db.BusStopPassages.Select(p => p.BusStopCode).Distinct();
                                        foreach (var stopCode in stopCodes)
                                        {
                                            BusStopPassage.Analyze(stopCode);
                                            Console.Write("{0}{0}{0}{0}", Environment.NewLine);
                                        }
                                    }
                            });
                            break;
                        case "lt":
                            RunCommand(() => DbHelper.GetTableNames().ToList().ForEach(Console.WriteLine));
                            break;
                        case "import":
                            UpdateDatabase();
                            break;
                        case "generate":
                            GenerateSql();
                            break;
                        case "trips":
                            RunCommand(() =>
                                {
                                    var trips = Line.GetTrips(parameters[0]);
                                    foreach (var trip in trips)
                                    {
                                        Console.WriteLine("Trip {0} ({3}), from '{1}' to '{2}'.", trip.TripID, trip.StartStop.Name, trip.EndStop.Name, trip.IsValid);
                                        foreach (var s in trip.Stops)
                                        {
                                            Console.WriteLine("    > " + s.Name);
                                        }
                                        Console.WriteLine("");
                                    }
                                });
                            break;
                        case "bus":
                            RunCommand(() =>
                            {
                                if (parameters[0] == "out")
                                    BusStop.GenerateBusFile();
                                else if (parameters[0] == "in")
                                    BusStop.ImportBusFile();
                                else if (parameters[0] == "rid")
                                    BusStop.FetchIDsAndLocations(false, true);
                                else if (parameters[0] == "check")
                                    BusStop.CheckRIDs(true);
                                else
                                    throw new Exception("Invalid parameter.");
                            });
                            break;
                        case "calculate":
                            RunCommand(() =>
                            {
                                if (parameters[0] == "trips")
                                    TripItem.ResetData();
                                else if (parameters[0] == "passages")
                                    BusStopPassage.Generate();
                                else
                                    throw new Exception("Invalid parameter.");
                            });
                            break;
                        case "updatedataset":
                            RunCommand(() =>
                                {
                                    var updates = new List<Tuple<string, string>>()
                                        {
                                            new Tuple<string, string>("004", "004"),
                                            new Tuple<string, string>("064", "034"),
                                            new Tuple<string, string>("124", "064"),
                                            new Tuple<string, string>("244", "094"),
                                            new Tuple<string, string>("364", "154"),
                                            new Tuple<string, string>("406", "186"),
                                            new Tuple<string, string>("456", "236"),
                                            new Tuple<string, string>("469", "259"),
                                            new Tuple<string, string>("487", "267"),
                                            new Tuple<string, string>("507", "287"),
                                            new Tuple<string, string>("530", "310"),
                                            new Tuple<string, string>("550", "330"),
                                            new Tuple<string, string>("569", "349"),
                                            new Tuple<string, string>("593", "373"),
                                            new Tuple<string, string>("608", "388"),
                                            new Tuple<string, string>("624", "404")
                                        };

                                    var folder = @"G:\MasterData\Z_Resultat\SVM - finn beste parametre";
                                    var files = new[]
                                        {
                                            @"C 2_-15-2_15, RBF på representative datasett.txt",
                                            @"C 11 21, RBF på representative datasett.txt",
                                            @"C 12 16, RBF på representative datasett.txt",
                                            @"C, Kernel på representative datasett.txt",
                                            @"Grid_1_unfinished.txt",
                                            @"Grid_2.txt",
                                            @"Grid_3.txt",
                                            @"Grid_4.txt",
                                            @"Grid_5.txt",
                                            @"Grid_6.txt",
                                            @"svm_puk_testingErlend.txt"
                                        };

                                    //PolyKernel, C=10.09999999999998, file 'G:\MasterData\weka\finalDataSet\595'.
                                    //PolyKernel, C=0.0, file '/home/shomed/d/dageinm/erlend/weka/finalDataset/608'.

                                    foreach (var file in files)
                                    {
                                        var path = System.IO.Path.Combine(folder, file);
                                        System.IO.File.Copy(path, path + ".bak", false);
                                        var contents = System.IO.File.ReadAllLines(path, Encoding.Default);
                                        for (int i = 0; i < contents.Length; i++)
                                        {
                                            if (contents[i].Contains(", file"))
                                            {
                                                foreach (var u in updates)
                                                    if (contents[i].Contains("finalDataSet\\" + u.Item1) || contents[i].Contains("finalDataSet/" + u.Item1))
                                                    {
                                                        contents[i] = contents[i].Replace("finalDataSet\\" + u.Item1, "finalDataSet\\" + u.Item2);
                                                        contents[i] = contents[i].Replace("finalDataSet/" + u.Item1, "finalDataSet/" + u.Item2);
                                                        break;
                                                    }
                                            }
                                        }
                                        System.IO.File.WriteAllLines(path, contents, Encoding.Default);
                                        Console.WriteLine("Fixed " + path);
                                    }
                                });
                            break;
                        case "validate":
                            RunCommand(() =>
                                {
                                    var tables = parameters.Length == 0 ?
                                                     ((Data.DataFolders[]) Enum.GetValues(typeof (Data.DataFolders))) :
                                                     parameters.Select(p => DbHelper.GetDataTypeFromTableName(p.Trim()));
                                    foreach (var t in tables)
                                        DbHelper.ValidateSql(t);
                                });
                            break;
                        case "export":
                        case "run":
                            ExportAndRunApplication(parameters);
                            return;
                        case "weka":
                            RunCommand(() =>
                            {
                                if (parameters[0] == "trips")
                                    TripItem.ToWeka(DbLoader.ParseDateTime(parameters[1], "yyyy-MM-dd"), DbLoader.ParseDateTime(parameters[2], "yyyy-MM-dd"), parameters.Length >= 4 ? parameters[3] == "1" : false);
                                else if (parameters[0] == "bsp")
                                    BusStopPassage.ToWeka(DbLoader.ParseDateTime(parameters[1], "yyyy-MM-dd"), DbLoader.ParseDateTime(parameters[2], "yyyy-MM-dd"), parameters.Length >= 4 ? parameters[3] == "1" : false);
                                else if (parameters[0] == "bsps")
                                    DataSetConstants.GenerateWeka();
                                else if (parameters[0] == "simi")
                                    BusStopPassage.GenerateSimi();
                                else
                                    throw new Exception("Invalid parameter.");
                            });
                            break;
                        case "urls":
                            RunCommand(() => GenerateDownloadLinks(DateTime.ParseExact(parameters[0], f, CultureInfo.InvariantCulture), DateTime.ParseExact(parameters[1], f, CultureInfo.InvariantCulture), true));
                            break;
                        case "fetch":
                            if (parameters[0] == "yr")
                                RunCommand(() => WeatherObject.FetchAndWriteToFile(DateTime.ParseExact(parameters[1], f, CultureInfo.InvariantCulture), DateTime.ParseExact(parameters[2], f, CultureInfo.InvariantCulture)));
                            else if(parameters[0]=="stops")
                                RunCommand(() =>
                                    {
                                        var stops = BusStop.FetchBusStopsFromAtB(int.Parse(parameters[1]));
                                        var file = Data.GetDataFile(Data.DataFolders.Buses, "templines.txt");
                                        using (var wc = new WebClient())
                                        {
                                            System.IO.File.WriteAllLines(file, stops.Select(p => p.Name + ";" + BusStop.GetRealTimeID(wc, p.ID)), Encoding.Default);
                                        }
                                        System.Diagnostics.Process.Start(file);
                                    });
                            else if(parameters[0] == "rtgps")
                                RunCommand(() =>
                                    {
                                        var pathGPS = @"G:\MasterData2\flashnet\gpslog";
                                        var pathRT = @"G:\MasterData2\flashnet\realtime";

                                        System.IO.Directory.GetFiles(pathRT, "*.txt").
                                               Where(p => p.EndsWith(".txt")).
                                               Where(p => DateTime.ParseExact(System.IO.Path.GetFileNameWithoutExtension(p).Split('_')[2], "yyyy-MM-dd", CultureInfo.InvariantCulture) != DateTime.Now.Date).
                                               ToList().
                                               ForEach(p =>
                                               {
                                                   var dest = p.Replace(pathRT, Data.GetDataFolder(Data.DataFolders.RealTime));
                                                   System.IO.File.Move(p, dest);
                                                   Console.WriteLine("  > Moved '{0}'.", dest);
                                               });

                                        System.IO.Directory.GetFiles(pathGPS, "*.csv").
                                               Where(p => p.EndsWith(".csv")).
                                               Where(p => DateTime.ParseExact(System.IO.Path.GetFileNameWithoutExtension(p).Split('_')[3], "yyyy-MM-dd", CultureInfo.InvariantCulture) != DateTime.Now.Date).
                                               ToList().
                                               ForEach(p =>
                                               {
                                                   var dest = p.Replace(pathGPS, Data.GetDataFolder(Data.DataFolders.GPSLog));
                                                   System.IO.File.Move(p, dest);
                                                   Console.WriteLine("  > Moved '{0}'.", dest);
                                               });
                                        
                                    });
                            else
                                throw new Exception("Invalid parameter.");
                            break;
                        default:
                            Console.WriteLine("Invalid command: '" + cmd + "'");
                            break;
                    }
                }
            }

            Console.ReadLine();
        }

        private static void RunCommand(Action action)
        {
            Console.WriteLine("");
#if DEBUG == false
            try
            {
#endif
                action();
#if DEBUG == false
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.Write(ex.StackTrace);
                Console.Write(Environment.NewLine);
            }
#endif
            Console.WriteLine("");
            Console.WriteLine("");
        }

        private static void ExportAndRunApplication(string[] parameters)
        {
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var target = System.IO.Path.Combine(path, "..", "Run");
            var i = 0;
            while (System.IO.Directory.Exists(target))
            {
                try
                {
                    System.IO.Directory.Delete(target, true);
                }
                catch (Exception)
                {
                    target = System.IO.Path.Combine(path, "..", "Run") + "_" + i++;
                }
            }
            System.IO.Directory.CreateDirectory(target);
            System.IO.Directory.GetFiles(path, "*.*").ToList().ForEach(p => System.IO.File.Copy(p, System.IO.Path.Combine(target, System.IO.Path.GetFileName(p))));
            System.Diagnostics.Process.Start(System.IO.Path.Combine(target, System.IO.Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().Location)), parameters.Length > 0 ? string.Join(" ", parameters) : "export");
        }

        private static void UpdateDatabase()
        {
            var db = new SMIODataContext();

            if (!db.ImportHistory.Any())
            {
                Console.WriteLine("Checking contents.");
                Console.WriteLine("  > Checking existing data.");
                var folders = new Dictionary<Data.DataFolders, string>()
                    {
                        {Data.DataFolders.Yr, "Yr"},
                        {Data.DataFolders.Buses, "Buses"},
                        {Data.DataFolders.Fulltrips, "FullTrips"},
                        {Data.DataFolders.LinkTravelTimes, "LTTItems"},
                        {Data.DataFolders.PassageAtBusStops, "PassageAtBusStops"},
                    };

                foreach (var folder in folders)
                {
                    if (!ImportHistoryItem.FirstTime(db, folder.Key))
                    {
                        Console.WriteLine("      + " + folder.Value + " has a history of imports.");
                        continue;
                    }
                    var fileCount = CountInserts(folder.Key);
                    var dbCount = DbHelper.GetDBCount(folder.Key, db);
                    if (dbCount == fileCount)
                    {
                        ImportHistoryItem.AddSqlFolder(db, folder.Key);
                        Console.WriteLine("      + " + folder.Value + " already imported.");
                    }
                    else
                    {
                        Console.WriteLine("      - " + folder.Value + " not imported (" + dbCount + "/" + fileCount + ").");
                    }
                }

                db.SaveChanges();
            }

            Func<Data.DataFolders, IEnumerable<string>> import = (folder) => System.IO.Directory.GetFiles(Data.GetSqlFolder(folder), "*.sql").Where(p => !ImportHistoryItem.IsImported(db, p));
            var todo = new List<string>();
            BusStop.CheckStops(db);
            Line.CheckLines(db);
            todo.AddRange(import(Data.DataFolders.Yr));
            todo.AddRange(import(Data.DataFolders.Fulltrips));
            todo.AddRange(import(Data.DataFolders.LinkTravelTimes));
            todo.AddRange(import(Data.DataFolders.PassageAtBusStops));
            todo.AddRange(import(Data.DataFolders.PassageAtVirtualLoops));
            todo.AddRange(import(Data.DataFolders.PassengerCount));
            todo.AddRange(import(Data.DataFolders.RealTime));
            todo.AddRange(import(Data.DataFolders.Ticket));
            todo.AddRange(import(Data.DataFolders.GPSLog));
            todo.AddRange(import(Data.DataFolders.CellPhoneTicket));
            todo.AddRange(import(Data.DataFolders.SingleTicket));
            todo.AddRange(import(Data.DataFolders.Trips));

            if (todo.Count > 0)
            {
                Console.WriteLine("");
                Console.WriteLine("------------------------------");
                Console.WriteLine("{0} files to process.", todo.Count);
                Console.WriteLine("------------------------------");
                Console.WriteLine("");
            }

            foreach (var file in todo)
            {
                Console.WriteLine("Processing file: '" + file.Replace(Data.SqlPath, "") + "'.");
                RunSql(file);
            }

            Console.WriteLine("");
            Console.WriteLine("------------------------------");
            Console.WriteLine("All done.");
            Console.WriteLine("------------------------------");
            Console.WriteLine("");
        }

        private static void RunSql(string sqlFile)
        {
            var path = System.IO.Path.Combine(Data.TempFolder, DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            DbHelper.SplitFiles(path, new[] {sqlFile});

            var files = System.IO.Directory.GetFiles(path, "*.sql").OrderBy(p => int.Parse(System.IO.Path.GetFileNameWithoutExtension(p)));

            var times = new List<double>();
            var count = 0;
            var lineCount = 0;

            var error = false;
            foreach (var file in files)
            {
                if (times.Count == 0)
                    Console.Write("\r  > Running '{0}'.", System.IO.Path.GetFileName(file));
                else
                    Console.Write("\r  > Running '{0}'. Average time: {1:n1} ms, files left: {2}, time remaining: {3:n1} minutes.                    ", System.IO.Path.GetFileName(file), times.Average(), files.Count() - count - 2, (times.Average() * (files.Count() - count++ - 1)) / 60000f);
                var start = DateTime.Now;

                lineCount += System.IO.File.ReadAllLines(file).Count(p => p.StartsWith("INSERT"));

                var cmd = new System.Diagnostics.Process();
                cmd.StartInfo.FileName = Data.SqlCmd;
                cmd.StartInfo.Arguments = "-S.\\SQLExpress -i\"" + file + "\"";
                cmd.StartInfo.UseShellExecute = false;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.Start();

                while (!cmd.StandardOutput.EndOfStream)
                {
                    var line = cmd.StandardOutput.ReadLine();
                    if (line == null || string.IsNullOrEmpty(line) || line.ToLower().Contains("1 rows affected")) continue;
                    Console.WriteLine(line);
                    error = true;
                }

                var ms = DateTime.Now.Subtract(start).TotalMilliseconds;
                times.Add(ms);

                if (times.Count > 10) times = times.Skip(times.Count - 10).ToList();
            }

            if (!error)
            {
                var db = new SMIODataContext();
                ImportHistoryItem.Import(db, sqlFile);
                db.SaveChanges();
            }
            try
            {
                System.IO.Directory.Delete(path, true);
            }
            catch (Exception)
            {               
                
            }

            Console.Write(Environment.NewLine + "  > Done." + Environment.NewLine);
        }

        private static void GenerateDownloadLinks(DateTime start, DateTime end, bool openFile = false)
        {
            //Links travel time:
            const string lttUrl = "http://st.atb.no/FlashNet/Proxy/ReportsInfoProxy.ashx?Report=LinksTravelTime&RequestType=LinksTravelTime&RequestFormat=CSV&dateFrom={0}%2000:00:00&dateTo={1}%2023:59:59&curAzId=1&sort=trip&sortDir=ASC&hiddenFields=&timestamp=1391432378708&maxRecords=30000";
            //Passages at bus stops:
            const string passageUrl = "http://st.atb.no/FlashNet/Proxy/ReportsInfoProxy.ashx?Report=Passages&RequestType=Passages&RequestFormat=CSV&dateFrom={0}%2000:00:00&dateTo={1}%2023:59:59&curAzId=1&lineID=&tripID=&vehicleID=&shiftID=&type=&stopCode=&otherInfo=&reportType=Nodes&sort=dateTime&sortDir=ASC&hiddenFields=&timestamp=1391432638790&maxRecords=30000";
            //Passages at virtual loops:
            const string loopsUrl = "http://st.atb.no/FlashNet/Proxy/ReportsInfoProxy.ashx?Report=Passages&RequestType=Passages&RequestFormat=CSV&dateFrom={0}%2000:00:00&dateTo={1}%2023:59:59&curAzId=1&lineID=&tripID=&vehicleID=&shiftID=&type=&stopCode=&otherInfo=&reportType=VirtualLoops&sort=dateTime&sortDir=ASC&hiddenFields=&timestamp=1391432704697&maxRecords=30000";
            //Full trips:
            const string ftUrl = "http://st.atb.no/FlashNet/Proxy/ReportsInfoProxy.ashx?Report=Trips&RequestType=Trips&RequestFormat=CSV&dateFrom={0}&dateTo={1}&AreaId=1&curAzId=1&anomaly=&arrivalAdvance=0&departureDelay=0&lineID=&pathID=&order=&shiftID=&timeSlot=&dayOfWeek=&worked=&tripID=&departureNode=&arrivalNode=&vehicleID=&driverID=&justification=&certified=&reportType=Full&sort=schedDepartureTime&sortDir=ASC&hiddenFields=&timestamp=1391432834619&maxRecords=30000";
            //Counting passengers:
            const string countUrl = "http://st.atb.no/FlashNet/Proxy/ReportsInfoProxy.ashx?Report=CountingPassengers&RequestType=CountingPassengers&RequestFormat=CSV&dateFrom={0}&dateTo={1}&curAzId=1&dayOfWeek=&lineID=&tripID=&driverID=&vehicleID=&reportType=Raw&sort=undefined&sortDir=undefined&hiddenFields=&timestamp=1391432952782&maxRecords=30000";

            var f = "yyyy-MM-dd";

            var urls = "";

            urls += "#Virtual loops" + Environment.NewLine;
            for (var dt = start; dt <= end; dt = dt.AddDays(1))
            {
                urls += string.Format(loopsUrl, dt.ToString(f), dt.ToString(f)) + Environment.NewLine;
                urls += dt.ToString(f) + ".csv" + Environment.NewLine;
            }
            urls += Environment.NewLine;

            Action<string, string> add = (title, url) =>
            {
                urls += "#" + title + ":" + Environment.NewLine;
                urls += string.Format(url, start.ToString(f), end.ToString(f)) + Environment.NewLine;
                urls += string.Format("{0} - {1}.csv{2}{2}", start.ToString(f), end.ToString(f), Environment.NewLine);
            };

            add("Links travel time", lttUrl);
            add("Passages at bus stops", passageUrl);
            add("Full trips", ftUrl);
            add("Passenger count", countUrl);

            var file = System.IO.Path.Combine(Data.DataPath, "flashnet", "urls_" + start.ToString("yyyy-MM-dd") + "_" + end.ToString("yyyy-MM-dd") + ".txt");
            System.IO.File.WriteAllText(file, urls, Encoding.Default);

            System.Diagnostics.Process.Start(file);

            Console.WriteLine("Urls generated and saved to " + file);
        }

        private static void GenerateSql()
        {
            Console.WriteLine("Generating SQL from new CSV files.");
            var stopCount = new SMIODataContext().BusStops.Count();
            PassageAtVirtualLoopsItem.GenerateSqlFiles();
            PassengerCountItem.GenerateSqlFiles();
            LTTItem.GenerateSqlFiles();
            PassageItem.GenerateSqlFiles();
            FullTripItem.GenerateSqlFiles();
            RealTimeItem.GenerateSqlFiles();
            Ticket.GenerateSqlFiles();
            GPSLogItem.GenerateSqlFiles();
            CellPhoneTicket.GenerateSqlFiles();
            SingleTicket.GenerateSqlFiles();
            WeatherObject.GenerateSqlFiles();

            if (stopCount != new SMIODataContext().BusStops.Count())
            {
                Console.WriteLine("Bus stops changed.");
                Console.ReadLine();
            }
        }

        private static decimal CountInserts(Data.DataFolders folder)
        {
            if (folder == Data.DataFolders.GPSLog) return System.IO.Directory.GetFiles(Data.GetSqlFolder(folder), "*.csv").Sum(p => System.IO.File.ReadAllLines(p, Encoding.Default).Count());
            return System.IO.Directory.GetFiles(Data.GetSqlFolder(folder), "*.sql").Sum(p => System.IO.File.ReadAllLines(p, Encoding.Default).Count(c => c.StartsWith("INSERT")));
        }
    }
}