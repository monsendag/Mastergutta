﻿namespace RealTimeGrapher
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPlot = new System.Windows.Forms.Button();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.txtLines = new System.Windows.Forms.TextBox();
            this.lblLines = new System.Windows.Forms.Label();
            this.comType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblStops = new System.Windows.Forms.Label();
            this.txtStop = new System.Windows.Forms.TextBox();
            this.btnPickLines = new System.Windows.Forms.Button();
            this.btnPickStops = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPlot
            // 
            this.btnPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlot.Location = new System.Drawing.Point(161, 157);
            this.btnPlot.Name = "btnPlot";
            this.btnPlot.Size = new System.Drawing.Size(75, 23);
            this.btnPlot.TabIndex = 0;
            this.btnPlot.Text = "Plot";
            this.btnPlot.UseVisualStyleBackColor = true;
            this.btnPlot.Click += new System.EventHandler(this.btnPlot_Click);
            // 
            // dtpStart
            // 
            this.dtpStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(62, 47);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(174, 20);
            this.dtpStart.TabIndex = 1;
            this.dtpStart.Value = new System.DateTime(2014, 2, 3, 14, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Start:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "End:";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(62, 75);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(174, 20);
            this.dtpEnd.TabIndex = 4;
            this.dtpEnd.Value = new System.DateTime(2014, 2, 3, 16, 0, 0, 0);
            // 
            // txtLines
            // 
            this.txtLines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLines.Location = new System.Drawing.Point(62, 102);
            this.txtLines.Name = "txtLines";
            this.txtLines.Size = new System.Drawing.Size(139, 20);
            this.txtLines.TabIndex = 5;
            this.txtLines.Text = "22";
            // 
            // lblLines
            // 
            this.lblLines.AutoSize = true;
            this.lblLines.Location = new System.Drawing.Point(13, 106);
            this.lblLines.Name = "lblLines";
            this.lblLines.Size = new System.Drawing.Size(41, 13);
            this.lblLines.TabIndex = 6;
            this.lblLines.Text = "Line(s):";
            // 
            // comType
            // 
            this.comType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comType.FormattingEnabled = true;
            this.comType.Items.AddRange(new object[] {
            "Next arrival / Time"});
            this.comType.Location = new System.Drawing.Point(62, 18);
            this.comType.Name = "comType";
            this.comType.Size = new System.Drawing.Size(174, 21);
            this.comType.TabIndex = 7;
            this.comType.SelectedIndexChanged += new System.EventHandler(this.comType_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Type:";
            // 
            // lblStops
            // 
            this.lblStops.AutoSize = true;
            this.lblStops.Location = new System.Drawing.Point(13, 134);
            this.lblStops.Name = "lblStops";
            this.lblStops.Size = new System.Drawing.Size(43, 13);
            this.lblStops.TabIndex = 9;
            this.lblStops.Text = "Stop(s):";
            // 
            // txtStop
            // 
            this.txtStop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStop.Location = new System.Drawing.Point(62, 130);
            this.txtStop.Name = "txtStop";
            this.txtStop.Size = new System.Drawing.Size(139, 20);
            this.txtStop.TabIndex = 10;
            this.txtStop.Text = "16010265";
            // 
            // btnPickLines
            // 
            this.btnPickLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickLines.Location = new System.Drawing.Point(207, 101);
            this.btnPickLines.Name = "btnPickLines";
            this.btnPickLines.Size = new System.Drawing.Size(29, 21);
            this.btnPickLines.TabIndex = 11;
            this.btnPickLines.Text = "...";
            this.btnPickLines.UseVisualStyleBackColor = true;
            this.btnPickLines.Click += new System.EventHandler(this.btnPickLines_Click);
            // 
            // btnPickStops
            // 
            this.btnPickStops.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickStops.Location = new System.Drawing.Point(207, 129);
            this.btnPickStops.Name = "btnPickStops";
            this.btnPickStops.Size = new System.Drawing.Size(29, 21);
            this.btnPickStops.TabIndex = 12;
            this.btnPickStops.Text = "...";
            this.btnPickStops.UseVisualStyleBackColor = true;
            this.btnPickStops.Click += new System.EventHandler(this.btnPickStops_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 192);
            this.Controls.Add(this.btnPickStops);
            this.Controls.Add(this.btnPickLines);
            this.Controls.Add(this.txtStop);
            this.Controls.Add(this.lblStops);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comType);
            this.Controls.Add(this.lblLines);
            this.Controls.Add(this.txtLines);
            this.Controls.Add(this.dtpEnd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpStart);
            this.Controls.Add(this.btnPlot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grapher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPlot;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.TextBox txtLines;
        private System.Windows.Forms.Label lblLines;
        private System.Windows.Forms.ComboBox comType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblStops;
        private System.Windows.Forms.TextBox txtStop;
        private System.Windows.Forms.Button btnPickLines;
        private System.Windows.Forms.Button btnPickStops;

    }
}

