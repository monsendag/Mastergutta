﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace RealTimeGrapher.Helpers
{
    public class PlotHelper
    {
        private readonly Form _owner;
        private frmPlot _plotForm;

        public PlotHelper(Form owner)
        {
            _owner = owner;
        }

        public frmPlot Form
        {
            get { return _plotForm; }
        }

        public void InitializeForm()
        {
            if (_owner.InvokeRequired)
            {
                _owner.Invoke(new MethodInvoker(InitializeForm));
                return;
            }

            _plotForm = new frmPlot();
            _plotForm.Show();
        }

        public void Done()
        {
            _plotForm.SetLoading(false);
        }

        public static PlotHelper Plot(Form owner, IEnumerable<double> yValues, string legend = "")
        {
            var vals = yValues.ToArray();
            return Plot(owner, Enumerable.Range(0, vals.Length).Select(p => (double) p).ToArray(), vals, legend);
        }

        public static PlotHelper Plot(Form owner, PointF[] xAndyValues, string legend = "")
        {
            var x = xAndyValues.Select(p => (double) p.X).ToArray();
            var y = xAndyValues.Select(p => (double) p.Y).ToArray();
            return Plot(owner, x, y, legend);
        }

        public static PlotHelper Plot(Form owner, Tuple<double, double>[] xAndyValues, string legend = "")
        {
            var x = xAndyValues.Select(p => p.Item1).ToArray();
            var y = xAndyValues.Select(p => p.Item2).ToArray();
            return Plot(owner, x, y, legend);
        }

        public static PlotHelper Plot(Form owner, double[][] xAndyValues, string legend = "")
        {
            var x = xAndyValues.Select(p => p[0]).ToArray();
            var y = xAndyValues.Select(p => p[1]).ToArray();
            return Plot(owner, x, y, legend);
        }

        public static PlotHelper Plot(Form owner, double[] xValues, double[] yValues, string legend = "")
        {
            var ph = new PlotHelper(owner);
            ph.InitializeForm();

            ph.Plot(xValues, yValues, legend);
            return ph;
        }

        public void Plot(IEnumerable<double> yValues, string legend = "")
        {
            var vals = yValues.ToArray();
            Plot(Enumerable.Range(0, vals.Length).Select(p => (double)p).ToArray(), vals, legend);
        }

        public void Plot(double[] xValues, double[] yValues, string legend)
        {
            if(Form==null || Form.IsDisposed) InitializeForm();

            var chart = Form.Chart;
            var s = new Series(legend) { ChartType = SeriesChartType.Line };
            for (var i = 0; i < yValues.Length; i++)
                s.Points.AddXY(xValues[i], yValues[i]);
            chart.Series.Add(s);
            Done();
        }
    }
}
