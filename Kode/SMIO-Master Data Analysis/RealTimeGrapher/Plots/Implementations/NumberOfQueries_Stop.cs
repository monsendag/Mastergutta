﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using RealTimeGrapher.Helpers;
using Utilities.AtB.General;
using Utilities.Database;
using Utilities.Extensions;

namespace RealTimeGrapher.Plots.Implementations
{
    public class NumberOfQueries_Stop : PlotBase
    {
        public override bool ShowLines
        {
            get { return true; }
        }

        public override bool ShowStops
        {
            get { return false; }
        }

        public override async void Plot(DataKeeper keeper)
        {
            keeper.Helper.InitializeForm();
            var line = keeper.GetData("lines");

            var stops = await Task.Run(() => GetDataPoints(keeper, line));

            var chart = keeper.Helper.Form.Chart;
            var s = new Series("Number of Queries / Stop");
            foreach (var p in stops)
            {
                s.Points.AddXY(p.Key.ID, p.Value);
                s.Points[s.Points.Count - 1].ToolTip = p.Key.Name + ": " + p.Value;
            }
            chart.Series.Add(s);
            keeper.Helper.Done();
        }

        private Dictionary<BusStop, int> GetDataPoints(DataKeeper keeper, string line)
        {
            var db = new SMIODataContext();
            return db.RealTimeItems.
                      Between(keeper.Start, keeper.End).
                      Where(p => p.Line == line).
                      GroupBy(p => p.StopID).
                      ToDictionary(
                          k => db.BusStops.Single(c => c.RealTimeID == k.Key),
                          v => v.Count());
        }
    }
}
