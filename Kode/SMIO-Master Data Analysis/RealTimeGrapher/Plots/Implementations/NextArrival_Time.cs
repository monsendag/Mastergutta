﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using RealTimeGrapher.Helpers;
using Utilities.Database;
using Utilities.Extensions;

namespace RealTimeGrapher.Plots.Implementations
{
    public class NextArrival_Time : PlotBase
    {
        public override bool ShowLines
        {
            get { return true; }
        }

        public override bool ShowStops
        {
            get { return true; }
        }

        public override async void Plot(DataKeeper keeper)
        {
            keeper.Helper.InitializeForm();

            var line = keeper.GetData("lines");
            var busstop = keeper.GetData("stops");
            
            var predictions = await Task.Run(() => GetDataPoints(keeper, line, busstop));

            var chart = keeper.Helper.Form.Chart;
            var s = new Series("Next Arrival / Time") {ChartType = SeriesChartType.Line};
            predictions.ForEach(p => s.Points.Add(p));
            chart.Series.Add(s);
            keeper.Helper.Done();
        }

        public List<DataPoint> GetDataPoints(DataKeeper keeper, string line, string busstop)
        {
            var db = new SMIODataContext();
            var stop = db.BusStops.Single(p => p.ID == busstop);
            var rti = db.RealTimeItems.
                         Between(keeper.Start, keeper.End). //Find items for the selected time span
                         Where(p => p.Line == line && p.StopID == stop.RealTimeID). //Find items at the selected line and bus stop
                         GroupBy(p => p.QueryTime). //Group by query time in order to only extract the next arrival
                         OrderBy(p => p.Key). //Order by query time
                         Select(p => new //Extract only the next arrival
                             {
                                 p.Key,
                                 p.OrderBy(c => c.Arrival).FirstOrDefault().Arrival
                             }).
                         ToList().
                         Select(p => new //Create a human friendly object
                             {
                                 QueryTime = p.Key,
                                 ArrivalInMinutes = p.Arrival.Subtract(p.Key).TotalMinutes,
                                 Minute = (int) p.Key.Subtract(keeper.Start).TotalMinutes
                             }).
                         Select(p => new DataPoint(p.Minute, p.ArrivalInMinutes) //Convert it into a data point
                             {
                                 ToolTip = p.QueryTime.ToString("HH:mm") + ": " + p.ArrivalInMinutes
                             }).
                         ToList();

            return rti;
        }
    }
}
