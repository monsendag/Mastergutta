﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Utilities.AtB.Utilities;
using Utilities.Extensions;
using Utilities.Yr;

namespace Yr_Daily_Weather_Data_Fetcher
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            WeatherObject.FetchAndWriteToFile(new DateTime(2014, 02, 01), new DateTime(2014, 2, 08));
        }
    }
}
