﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Utilities.AtB;
//using Utilities.AtB.Generated;

//namespace UnitTestProject1
//{
//    [TestClass]
//    public class BusStopPassageTests : BusStopPassage
//    {
//        [TestMethod]
//        public void FindDelayTests()
//        {
//            var delays = new List<DelayHolder>()
//                {
//                    new DelayHolder(new DateTime(2014, 02, 26, 14, 0, 0), 15, "A"),
//                    new DelayHolder(new DateTime(2014, 02, 27, 14, 0, 0), 16, "A"),
//                    new DelayHolder(new DateTime(2014, 02, 28, 14, 0, 0), 17, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 1, 14, 0, 0), 18, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 2, 14, 0, 0), 19, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 3, 14, 0, 0), 20, "B"),
//                    new DelayHolder(new DateTime(2014, 03, 4, 15, 0, 0), 21, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 5, 15, 0, 0), 22, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 6, 14, 0, 0), 23, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 7, 14, 0, 0), 24, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 8, 14, 0, 0), 25, "A")
//                };

//            var res = FindDelay(delays, "A", new DateTime(2014, 03, 6, 14, 0, 0), 1, 1);
//            Assert.AreEqual(19, res);

//            res = FindDelay(delays, "A", new DateTime(2014, 03, 6, 14, 0, 0), 1, 2);
//            Assert.AreEqual(19, res);

//            res = FindDelay(delays, "A", new DateTime(2014, 03, 2, 14, 0, 0), 1, 2);
//            Assert.AreEqual(17, res);

//            res = FindDelay(delays, "A", new DateTime(2014, 03, 6, 14, 0, 0), 7, 7);
//            Assert.AreEqual(16, res);
//        }
//        [TestMethod]
//        public void AverageTests()
//        {
//            var delays = new List<DelayHolder>()
//                {
//                    new DelayHolder(new DateTime(2014, 02, 26, 14, 0, 0), 15, "A"),
//                    new DelayHolder(new DateTime(2014, 02, 27, 14, 0, 0), 16, "A"),
//                    new DelayHolder(new DateTime(2014, 02, 28, 14, 0, 0), 17, "B"),
//                    new DelayHolder(new DateTime(2014, 03, 1, 14, 0, 0), 18, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 8, 14, 0, 0), 19, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 8, 15, 0, 0), 29, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 4, 15, 0, 0), 21, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 5, 15, 0, 0), 22, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 6, 14, 0, 0), 23, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 7, 14, 0, 0), 24, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 9, 14, 0, 0), 25, "B"),
//                    new DelayHolder(new DateTime(2014, 03, 15, 14, 0, 0), 20, "A"),
//                    new DelayHolder(new DateTime(2014, 03, 22, 14, 0, 0), 30, "B"),
//                    new DelayHolder(new DateTime(2014, 03, 29, 14, 0, 0), 31, "A")
//                };

//            var res = AverageLast3(delays, "A");
//            Assert.AreEqual(25, res);

//            res = AverageLast3(delays.Take(5).ToList(), "A");
//            Assert.AreEqual(17.66666666666666667, res, 0.0000001);

//            res = AverageLast3OfSameDay(delays, "A", new DateTime(2014, 03, 22, 14, 0, 0));
//            Assert.AreEqual(19, res);
//        }
//    }
//}
