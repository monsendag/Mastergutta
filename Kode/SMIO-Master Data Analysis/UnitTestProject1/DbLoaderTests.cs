﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Database;

namespace UnitTestProject1
{
    [TestClass]
    public class DbLoaderTests : DbLoader
    {
        [TestMethod]
        public void ParseSecondsTests()
        {
            //Actual values
            Assert.AreEqual(80, ParseSeconds("01:20"));
            Assert.AreEqual(0, ParseSeconds(""));
            Assert.AreEqual(20, ParseSeconds("00:20"));
            Assert.AreEqual(80, ParseSeconds("01:20"));
            Assert.AreEqual(-20, ParseSeconds("-00:20"));
            Assert.AreEqual(-80, ParseSeconds("-01:20"));
            Assert.AreEqual(80, ParseSeconds("1:20"));
            Assert.AreEqual(20, ParseSeconds("0:20"));
            Assert.AreEqual(-80, ParseSeconds("-1:20"));
            Assert.AreEqual(-20, ParseSeconds("-0:20"));
            Assert.AreEqual(3680, ParseSeconds("01:01:20"));
            Assert.AreEqual(3680, ParseSeconds("1:1:20"));
            Assert.AreEqual(-3680, ParseSeconds("-1:1:20"));

            //Weird stuff
            Assert.AreEqual(0, ParseSeconds("   "));
            Assert.AreEqual(800, ParseSeconds("01:740"));
            Assert.AreEqual(800, ParseSeconds("10:200"));
            Assert.AreEqual(1+60+60*60+60*60*60, ParseSeconds("1:1:1:1"));
        }
    }
}
