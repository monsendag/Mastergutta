﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Utilities.AtB.Generated;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.EnumUtil;
using Utilities.Extensions;
using Utilities.PowerSet;

namespace Weka_Evaluation_Analyzer
{
    public class DataSetConstants
    {

        public static IEnumerable<DataSetInfo> DataSet()
        {
            var data = DataSetString.
                Split(Environment.NewLine).
                Where(p => !String.IsNullOrEmpty(p.Trim())).
                Select(p => p.Split('\t')).ToList();
            return data.
                Select(p => new
                    {
                        Filename = p[0],
                        BusStop = p[1],
                        TrainStart = p[5],
                        TrainEnd = p[6],
                        TestStart = p[7],
                        TestEnd = p[8],
                        Description = p[9],
                        Comment = p[10],
                        Keywords = new []{"passenger", "ticket"},//p[11].Split(','),
                        Group = p[12],
                        Subgroup = p[13],
                        OriginalLine = p
                    }).
                Select(p => new DataSetInfo()
                    {
                        Filename = p.Filename,
                        BusStop = p.BusStop,
                        TrainIntervals = ParseInterval(p.TrainStart, p.TrainEnd),
                        TestIntervals = ParseInterval(p.TestStart, p.TestEnd),
                        Keywords = p.Keywords,
                        Description = p.Description,
                        Comment = p.Comment,
                        OriginalLine = p.OriginalLine,
                        Group = p.Group,
                        Subgroup = p.Subgroup,
                    });
        }

        private static IEnumerable<Tuple<DateTime, DateTime>> ParseInterval(string start, string end)
        {
            if (!start.Contains(":"))
                return start.Split(',').
                             Select(p => DbLoader.ParseDateTime(p, "yyyy-MM-dd")).
                             Select(p => Tuple.Create(p, p.AddHours(23).AddMinutes(59).AddSeconds(59)));
            start = start.Replace("24:00", "23:59");
            end = end.Replace("24:00", "23:59");
            const string format = "yyyy-MM-dd HH:mm";
            return new[] { Tuple.Create(DbLoader.ParseDateTime(start, format), DbLoader.ParseDateTime(end, format)) };
        }


        public const string DataSetString = @"001	16010476	Samfundet fra byen	1t	1t	2013-11-11 10:00	2013-11-11 11:00	2013-11-11 11:00	2013-11-11 12:00	Mandag; tren på én time, test på neste.		ticket	1	1
002	16010476	Samfundet fra byen	1t	1t	2013-11-11 11:00	2013-11-11 12:00	2013-11-11 12:00	2013-11-11 13:00	En time etter forrige.		ticket	1	1
003	16010476	Samfundet fra byen	1t	1t	2013-11-12 10:00	2013-11-12 11:00	2013-11-12 11:00	2013-11-12 12:00	Tirsdag; tren på én time, test på neste.		ticket	1	1
004	16010476	Samfundet fra byen	1t	1t	2013-11-13 10:00	2013-11-13 11:00	2013-11-13 11:00	2013-11-13 12:00	Onsdag; tren på én time, test på neste.		ticket	1	1
005	16010476	Samfundet fra byen	1t	1t	2013-11-13 12:00	2013-11-13 13:00	2013-11-13 13:00	2013-11-13 14:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	1
006	16010476	Samfundet fra byen	1t	1t	2013-11-13 18:00	2013-11-13 19:00	2013-11-13 19:00	2013-11-13 20:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	1
007	16010476	Samfundet fra byen	1t	1t	2013-11-14 10:00	2013-11-14 11:00	2013-11-14 11:00	2013-11-14 12:00	Torsdag; tren på én time, test på neste.		ticket	1	1
008	16010476	Samfundet fra byen	1t	1t	2013-11-15 10:00	2013-11-15 11:00	2013-11-15 11:00	2013-11-15 12:00	Fredag; tren på én time, test på neste.		ticket	1	1
009	16010476	Samfundet fra byen	1t	1t	2013-11-16 10:00	2013-11-16 11:00	2013-11-16 11:00	2013-11-16 12:00	Lørdag; tren på én time, test på neste.		ticket	1	1
010	16010476	Samfundet fra byen	1t	1t	2013-11-17 10:00	2013-11-17 11:00	2013-11-17 11:00	2013-11-17 12:00	Søndag; tren på én time, test på neste.		ticket	1	1
011	16010264	Lerkendal fra byen	1t	1t	2013-11-11 10:00	2013-11-11 11:00	2013-11-11 11:00	2013-11-11 12:00	Mandag; tren på én time, test på neste.	Samme for Lerkendal	ticket	1	1
012	16010264	Lerkendal fra byen	1t	1t	2013-11-11 11:00	2013-11-11 12:00	2013-11-11 12:00	2013-11-11 13:00	En time etter forrige.		ticket	1	1
013	16010264	Lerkendal fra byen	1t	1t	2013-11-12 10:00	2013-11-12 11:00	2013-11-12 11:00	2013-11-12 12:00	Tirsdag; tren på én time, test på neste.		ticket	1	1
014	16010264	Lerkendal fra byen	1t	1t	2013-11-13 10:00	2013-11-13 11:00	2013-11-13 11:00	2013-11-13 12:00	Onsdag; tren på én time, test på neste.		ticket	1	1
015	16010264	Lerkendal fra byen	1t	1t	2013-11-13 12:00	2013-11-13 13:00	2013-11-13 13:00	2013-11-13 14:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	1
016	16010264	Lerkendal fra byen	1t	1t	2013-11-13 18:00	2013-11-13 19:00	2013-11-13 19:00	2013-11-13 20:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	1
017	16010264	Lerkendal fra byen	1t	1t	2013-11-14 10:00	2013-11-14 11:00	2013-11-14 11:00	2013-11-14 12:00	Torsdag; tren på én time, test på neste.		ticket	1	1
018	16010264	Lerkendal fra byen	1t	1t	2013-11-15 10:00	2013-11-15 11:00	2013-11-15 11:00	2013-11-15 12:00	Fredag; tren på én time, test på neste.		ticket	1	1
019	16010264	Lerkendal fra byen	1t	1t	2013-11-16 10:00	2013-11-16 11:00	2013-11-16 11:00	2013-11-16 12:00	Lørdag; tren på én time, test på neste.		ticket	1	1
020	16010264	Lerkendal fra byen	1t	1t	2013-11-17 10:00	2013-11-17 11:00	2013-11-17 11:00	2013-11-17 12:00	Søndag; tren på én time, test på neste.		ticket	1	1
021	16011465	Steindalsvegen til byen	1t	1t	2013-11-11 10:00	2013-11-11 11:00	2013-11-11 11:00	2013-11-11 12:00	Mandag; tren på én time, test på neste.	Samme for Steindalsvegen	ticket	1	1
022	16011465	Steindalsvegen til byen	1t	1t	2013-11-11 11:00	2013-11-11 12:00	2013-11-11 12:00	2013-11-11 13:00	En time etter forrige.		ticket	1	1
023	16011465	Steindalsvegen til byen	1t	1t	2013-11-12 10:00	2013-11-12 11:00	2013-11-12 11:00	2013-11-12 12:00	Tirsdag; tren på én time, test på neste.		ticket	1	1
024	16011465	Steindalsvegen til byen	1t	1t	2013-11-13 10:00	2013-11-13 11:00	2013-11-13 11:00	2013-11-13 12:00	Onsdag; tren på én time, test på neste.		ticket	1	1
025	16011465	Steindalsvegen til byen	1t	1t	2013-11-13 12:00	2013-11-13 13:00	2013-11-13 13:00	2013-11-13 14:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	1
026	16011465	Steindalsvegen til byen	1t	1t	2013-11-13 18:00	2013-11-13 19:00	2013-11-13 19:00	2013-11-13 20:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	1
027	16011465	Steindalsvegen til byen	1t	1t	2013-11-14 10:00	2013-11-14 11:00	2013-11-14 11:00	2013-11-14 12:00	Torsdag; tren på én time, test på neste.		ticket	1	1
028	16011465	Steindalsvegen til byen	1t	1t	2013-11-15 10:00	2013-11-15 11:00	2013-11-15 11:00	2013-11-15 12:00	Fredag; tren på én time, test på neste.		ticket	1	1
029	16011465	Steindalsvegen til byen	1t	1t	2013-11-16 10:00	2013-11-16 11:00	2013-11-16 11:00	2013-11-16 12:00	Lørdag; tren på én time, test på neste.		ticket	1	1
030	16011465	Steindalsvegen til byen	1t	1t	2013-11-17 10:00	2013-11-17 11:00	2013-11-17 11:00	2013-11-17 12:00	Søndag; tren på én time, test på neste.		ticket	1	1
													
031	16010476	Samfundet fra byen	2t	1t	2013-11-11 10:00	2013-11-11 12:00	2013-11-11 12:00	2013-11-11 13:00	Mandag; tren på to timer, test på neste.		ticket	1	2
032	16010476	Samfundet fra byen	2t	1t	2013-11-11 12:00	2013-11-11 14:00	2013-11-11 14:00	2013-11-11 15:00	To timer etter forrige.		ticket	1	2
033	16010476	Samfundet fra byen	2t	1t	2013-11-12 10:00	2013-11-12 12:00	2013-11-12 12:00	2013-11-12 13:00	Tirsdag; tren på to timer, test på neste.		ticket	1	2
034	16010476	Samfundet fra byen	2t	1t	2013-11-13 10:00	2013-11-13 12:00	2013-11-13 12:00	2013-11-13 13:00	Onsdag; tren på to timer, test på neste.		ticket	1	2
035	16010476	Samfundet fra byen	2t	1t	2013-11-13 12:00	2013-11-13 14:00	2013-11-13 14:00	2013-11-13 15:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	2
036	16010476	Samfundet fra byen	2t	1t	2013-11-13 18:00	2013-11-13 20:00	2013-11-13 20:00	2013-11-13 21:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	2
037	16010476	Samfundet fra byen	2t	1t	2013-11-14 10:00	2013-11-14 12:00	2013-11-14 12:00	2013-11-14 13:00	Torsdag; tren på to timer, test på neste.		ticket	1	2
038	16010476	Samfundet fra byen	2t	1t	2013-11-15 10:00	2013-11-15 12:00	2013-11-15 12:00	2013-11-15 13:00	Fredag; tren på to timer, test på neste.		ticket	1	2
039	16010476	Samfundet fra byen	2t	1t	2013-11-16 10:00	2013-11-16 12:00	2013-11-16 12:00	2013-11-16 13:00	Lørdag; tren på to timer, test på neste.		ticket	1	2
040	16010476	Samfundet fra byen	2t	1t	2013-11-17 10:00	2013-11-17 12:00	2013-11-17 12:00	2013-11-17 13:00	Søndag; tren på to timer, test på neste.		ticket	1	2
041	16010264	Lerkendal fra byen	2t	1t	2013-11-11 10:00	2013-11-11 12:00	2013-11-11 12:00	2013-11-11 13:00	Mandag; tren på to timer, test på neste.	Samme for Lerkendal	ticket	1	2
042	16010264	Lerkendal fra byen	2t	1t	2013-11-11 12:00	2013-11-11 14:00	2013-11-11 14:00	2013-11-11 15:00	To timer etter forrige.		ticket	1	2
043	16010264	Lerkendal fra byen	2t	1t	2013-11-12 10:00	2013-11-12 12:00	2013-11-12 12:00	2013-11-12 13:00	Tirsdag; tren på to timer, test på neste.		ticket	1	2
044	16010264	Lerkendal fra byen	2t	1t	2013-11-13 10:00	2013-11-13 12:00	2013-11-13 12:00	2013-11-13 13:00	Onsdag; tren på to timer, test på neste.		ticket	1	2
045	16010264	Lerkendal fra byen	2t	1t	2013-11-13 12:00	2013-11-13 14:00	2013-11-13 14:00	2013-11-13 15:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	2
046	16010264	Lerkendal fra byen	2t	1t	2013-11-13 18:00	2013-11-13 20:00	2013-11-13 20:00	2013-11-13 21:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	2
047	16010264	Lerkendal fra byen	2t	1t	2013-11-14 10:00	2013-11-14 12:00	2013-11-14 12:00	2013-11-14 13:00	Torsdag; tren på to timer, test på neste.		ticket	1	2
048	16010264	Lerkendal fra byen	2t	1t	2013-11-15 10:00	2013-11-15 12:00	2013-11-15 12:00	2013-11-15 13:00	Fredag; tren på to timer, test på neste.		ticket	1	2
049	16010264	Lerkendal fra byen	2t	1t	2013-11-16 10:00	2013-11-16 12:00	2013-11-16 12:00	2013-11-16 13:00	Lørdag; tren på to timer, test på neste.		ticket	1	2
050	16010264	Lerkendal fra byen	2t	1t	2013-11-17 10:00	2013-11-17 12:00	2013-11-17 12:00	2013-11-17 13:00	Søndag; tren på to timer, test på neste.		ticket	1	2
051	16011465	Steindalsvegen til byen	2t	1t	2013-11-11 10:00	2013-11-11 12:00	2013-11-11 12:00	2013-11-11 13:00	Mandag; tren på to timer, test på neste.	Samme for Steindalsvegen	ticket	1	2
052	16011465	Steindalsvegen til byen	2t	1t	2013-11-11 12:00	2013-11-11 14:00	2013-11-11 14:00	2013-11-11 15:00	To timer etter forrige.		ticket	1	2
053	16011465	Steindalsvegen til byen	2t	1t	2013-11-12 10:00	2013-11-12 12:00	2013-11-12 12:00	2013-11-12 13:00	Tirsdag; tren på to timer, test på neste.		ticket	1	2
054	16011465	Steindalsvegen til byen	2t	1t	2013-11-13 10:00	2013-11-13 12:00	2013-11-13 12:00	2013-11-13 13:00	Onsdag; tren på to timer, test på neste.		ticket	1	2
055	16011465	Steindalsvegen til byen	2t	1t	2013-11-13 12:00	2013-11-13 14:00	2013-11-13 14:00	2013-11-13 15:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	2
056	16011465	Steindalsvegen til byen	2t	1t	2013-11-13 18:00	2013-11-13 20:00	2013-11-13 20:00	2013-11-13 21:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	2
057	16011465	Steindalsvegen til byen	2t	1t	2013-11-14 10:00	2013-11-14 12:00	2013-11-14 12:00	2013-11-14 13:00	Torsdag; tren på to timer, test på neste.		ticket	1	2
058	16011465	Steindalsvegen til byen	2t	1t	2013-11-15 10:00	2013-11-15 12:00	2013-11-15 12:00	2013-11-15 13:00	Fredag; tren på to timer, test på neste.		ticket	1	2
059	16011465	Steindalsvegen til byen	2t	1t	2013-11-16 10:00	2013-11-16 12:00	2013-11-16 12:00	2013-11-16 13:00	Lørdag; tren på to timer, test på neste.		ticket	1	2
060	16011465	Steindalsvegen til byen	2t	1t	2013-11-17 10:00	2013-11-17 12:00	2013-11-17 12:00	2013-11-17 13:00	Søndag; tren på to timer, test på neste.		ticket	1	2
													
061	16010476	Samfundet fra byen	3t	1t	2013-11-11 12:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på tre timer, test på neste.		ticket	1	3
062	16010476	Samfundet fra byen	3t	1t	2013-11-11 14:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Tre timer etter forrige.		ticket	1	3
063	16010476	Samfundet fra byen	3t	1t	2013-11-12 12:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på tre timer, test på neste.		ticket	1	3
064	16010476	Samfundet fra byen	3t	1t	2013-11-13 12:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på tre timer, test på neste.		ticket	1	3
065	16010476	Samfundet fra byen	3t	1t	2013-11-13 14:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	3
066	16010476	Samfundet fra byen	3t	1t	2013-11-13 20:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	3
067	16010476	Samfundet fra byen	3t	1t	2013-11-14 12:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på tre timer, test på neste.		ticket	1	3
068	16010476	Samfundet fra byen	3t	1t	2013-11-15 12:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på tre timer, test på neste.		ticket	1	3
069	16010476	Samfundet fra byen	3t	1t	2013-11-16 12:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på tre timer, test på neste.		ticket	1	3
070	16010476	Samfundet fra byen	3t	1t	2013-11-17 12:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på tre timer, test på neste.		ticket	1	3
071	16010264	Lerkendal fra byen	3t	1t	2013-11-11 12:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på tre timer, test på neste.	Samme for Lerkendal	ticket	1	3
072	16010264	Lerkendal fra byen	3t	1t	2013-11-11 14:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Tre timer etter forrige.		ticket	1	3
073	16010264	Lerkendal fra byen	3t	1t	2013-11-12 12:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på tre timer, test på neste.		ticket	1	3
074	16010264	Lerkendal fra byen	3t	1t	2013-11-13 12:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på tre timer, test på neste.		ticket	1	3
075	16010264	Lerkendal fra byen	3t	1t	2013-11-13 14:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	3
076	16010264	Lerkendal fra byen	3t	1t	2013-11-13 20:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	3
077	16010264	Lerkendal fra byen	3t	1t	2013-11-14 12:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på tre timer, test på neste.		ticket	1	3
078	16010264	Lerkendal fra byen	3t	1t	2013-11-15 12:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på tre timer, test på neste.		ticket	1	3
079	16010264	Lerkendal fra byen	3t	1t	2013-11-16 12:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på tre timer, test på neste.		ticket	1	3
080	16010264	Lerkendal fra byen	3t	1t	2013-11-17 12:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på tre timer, test på neste.		ticket	1	3
081	16011465	Steindalsvegen til byen	3t	1t	2013-11-11 12:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på tre timer, test på neste.	Samme med passasjer	ticket, passenger	1	3
082	16011465	Steindalsvegen til byen	3t	1t	2013-11-11 14:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Tre timer etter forrige.		ticket, passenger	1	3
083	16011465	Steindalsvegen til byen	3t	1t	2013-11-12 12:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på tre timer, test på neste.		ticket, passenger	1	3
084	16011465	Steindalsvegen til byen	3t	1t	2013-11-13 12:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på tre timer, test på neste.		ticket, passenger	1	3
085	16011465	Steindalsvegen til byen	3t	1t	2013-11-13 14:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket, passenger	1	3
086	16011465	Steindalsvegen til byen	3t	1t	2013-11-13 20:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket, passenger	1	3
087	16011465	Steindalsvegen til byen	3t	1t	2013-11-14 12:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på tre timer, test på neste.		ticket, passenger	1	3
088	16011465	Steindalsvegen til byen	3t	1t	2013-11-15 12:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på tre timer, test på neste.		ticket, passenger	1	3
089	16011465	Steindalsvegen til byen	3t	1t	2013-11-16 12:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på tre timer, test på neste.		ticket, passenger	1	3
090	16011465	Steindalsvegen til byen	3t	1t	2013-11-17 12:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på tre timer, test på neste.		ticket, passenger	1	3
													
091	16010476	Samfundet fra byen	4t	1t	2013-11-11 11:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på fire timer, test på neste.		ticket	1	4
092	16010476	Samfundet fra byen	4t	1t	2013-11-11 13:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Fire timer etter forrige.		ticket	1	4
093	16010476	Samfundet fra byen	4t	1t	2013-11-12 11:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på fire timer, test på neste.		ticket	1	4
094	16010476	Samfundet fra byen	4t	1t	2013-11-13 11:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på fire timer, test på neste.		ticket	1	4
095	16010476	Samfundet fra byen	4t	1t	2013-11-13 13:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	4
096	16010476	Samfundet fra byen	4t	1t	2013-11-13 19:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	4
097	16010476	Samfundet fra byen	4t	1t	2013-11-14 11:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på fire timer, test på neste.		ticket	1	4
098	16010476	Samfundet fra byen	4t	1t	2013-11-15 11:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på fire timer, test på neste.		ticket	1	4
099	16010476	Samfundet fra byen	4t	1t	2013-11-16 11:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på fire timer, test på neste.		ticket	1	4
100	16010476	Samfundet fra byen	4t	1t	2013-11-17 11:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på fire timer, test på neste.		ticket	1	4
101	16010264	Lerkendal fra byen	4t	1t	2013-11-11 11:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på fire timer, test på neste.	Samme for Lerkendal	ticket	1	4
102	16010264	Lerkendal fra byen	4t	1t	2013-11-11 13:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Fire timer etter forrige.		ticket	1	4
103	16010264	Lerkendal fra byen	4t	1t	2013-11-12 11:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på fire timer, test på neste.		ticket	1	4
104	16010264	Lerkendal fra byen	4t	1t	2013-11-13 11:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på fire timer, test på neste.		ticket	1	4
105	16010264	Lerkendal fra byen	4t	1t	2013-11-13 13:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	4
106	16010264	Lerkendal fra byen	4t	1t	2013-11-13 19:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	4
107	16010264	Lerkendal fra byen	4t	1t	2013-11-14 11:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på fire timer, test på neste.		ticket	1	4
108	16010264	Lerkendal fra byen	4t	1t	2013-11-15 11:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på fire timer, test på neste.		ticket	1	4
109	16010264	Lerkendal fra byen	4t	1t	2013-11-16 11:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på fire timer, test på neste.		ticket	1	4
110	16010264	Lerkendal fra byen	4t	1t	2013-11-17 11:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på fire timer, test på neste.		ticket	1	4
111	16011465	Steindalsvegen til byen	4t	1t	2013-11-11 11:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på fire timer, test på neste.	Samme med passasjer	ticket, passenger	1	4
112	16011465	Steindalsvegen til byen	4t	1t	2013-11-11 13:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Fire timer etter forrige.		ticket, passenger	1	4
113	16011465	Steindalsvegen til byen	4t	1t	2013-11-12 11:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på fire timer, test på neste.		ticket, passenger	1	4
114	16011465	Steindalsvegen til byen	4t	1t	2013-11-13 11:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på fire timer, test på neste.		ticket, passenger	1	4
115	16011465	Steindalsvegen til byen	4t	1t	2013-11-13 13:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket, passenger	1	4
116	16011465	Steindalsvegen til byen	4t	1t	2013-11-13 19:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket, passenger	1	4
117	16011465	Steindalsvegen til byen	4t	1t	2013-11-14 11:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på fire timer, test på neste.		ticket, passenger	1	4
118	16011465	Steindalsvegen til byen	4t	1t	2013-11-15 11:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på fire timer, test på neste.		ticket, passenger	1	4
119	16011465	Steindalsvegen til byen	4t	1t	2013-11-16 11:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på fire timer, test på neste.		ticket, passenger	1	4
120	16011465	Steindalsvegen til byen	4t	1t	2013-11-17 11:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på fire timer, test på neste.		ticket, passenger	1	4
													
													
121	16010476	Samfundet fra byen	5t	1t	2013-11-11 10:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på fem timer, test på neste.		ticket	1	5
122	16010476	Samfundet fra byen	5t	1t	2013-11-11 12:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Fem timer etter forrige.		ticket	1	5
123	16010476	Samfundet fra byen	5t	1t	2013-11-12 10:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på fem timer, test på neste.		ticket	1	5
124	16010476	Samfundet fra byen	5t	1t	2013-11-13 10:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på fem timer, test på neste.		ticket	1	5
125	16010476	Samfundet fra byen	5t	1t	2013-11-13 12:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	5
126	16010476	Samfundet fra byen	5t	1t	2013-11-13 18:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	5
127	16010476	Samfundet fra byen	5t	1t	2013-11-14 10:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på fem timer, test på neste.		ticket	1	5
128	16010476	Samfundet fra byen	5t	1t	2013-11-15 10:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på fem timer, test på neste.		ticket	1	5
129	16010476	Samfundet fra byen	5t	1t	2013-11-16 10:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på fem timer, test på neste.		ticket	1	5
130	16010476	Samfundet fra byen	5t	1t	2013-11-17 10:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på fem timer, test på neste.		ticket	1	5
131	16010264	Lerkendal fra byen	5t	1t	2013-11-11 10:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på fem timer, test på neste.	Samme for Lerkendal	ticket	1	5
132	16010264	Lerkendal fra byen	5t	1t	2013-11-11 12:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Fem timer etter forrige.		ticket	1	5
133	16010264	Lerkendal fra byen	5t	1t	2013-11-12 10:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på fem timer, test på neste.		ticket	1	5
134	16010264	Lerkendal fra byen	5t	1t	2013-11-13 10:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på fem timer, test på neste.		ticket	1	5
135	16010264	Lerkendal fra byen	5t	1t	2013-11-13 12:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	5
136	16010264	Lerkendal fra byen	5t	1t	2013-11-13 18:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket	1	5
137	16010264	Lerkendal fra byen	5t	1t	2013-11-14 10:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på fem timer, test på neste.		ticket	1	5
138	16010264	Lerkendal fra byen	5t	1t	2013-11-15 10:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på fem timer, test på neste.		ticket	1	5
139	16010264	Lerkendal fra byen	5t	1t	2013-11-16 10:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på fem timer, test på neste.		ticket	1	5
140	16010264	Lerkendal fra byen	5t	1t	2013-11-17 10:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på fem timer, test på neste.		ticket	1	5
141	16011465	Steindalsvegen til byen	5t	1t	2013-11-11 10:00	2013-11-11 15:00	2013-11-11 15:00	2013-11-11 16:00	Mandag; tren på fem timer, test på neste.	Samme med passasjer	ticket, passenger	1	5
142	16011465	Steindalsvegen til byen	5t	1t	2013-11-11 12:00	2013-11-11 17:00	2013-11-11 17:00	2013-11-11 18:00	Fem timer etter forrige.		ticket, passenger	1	5
143	16011465	Steindalsvegen til byen	5t	1t	2013-11-12 10:00	2013-11-12 15:00	2013-11-12 15:00	2013-11-12 16:00	Tirsdag; tren på fem timer, test på neste.		ticket, passenger	1	5
144	16011465	Steindalsvegen til byen	5t	1t	2013-11-13 10:00	2013-11-13 15:00	2013-11-13 15:00	2013-11-13 16:00	Onsdag; tren på fem timer, test på neste.		ticket, passenger	1	5
145	16011465	Steindalsvegen til byen	5t	1t	2013-11-13 12:00	2013-11-13 17:00	2013-11-13 17:00	2013-11-13 18:00	Samme lengde, andre tidspunkter på onsdag.		ticket, passenger	1	5
146	16011465	Steindalsvegen til byen	5t	1t	2013-11-13 18:00	2013-11-13 23:00	2013-11-13 23:00	2013-11-13 24:00	Samme lengde, andre tidspunkter på onsdag.		ticket, passenger	1	5
147	16011465	Steindalsvegen til byen	5t	1t	2013-11-14 10:00	2013-11-14 15:00	2013-11-14 15:00	2013-11-14 16:00	Torsdag; tren på fem timer, test på neste.		ticket, passenger	1	5
148	16011465	Steindalsvegen til byen	5t	1t	2013-11-15 10:00	2013-11-15 15:00	2013-11-15 15:00	2013-11-15 16:00	Fredag; tren på fem timer, test på neste.		ticket, passenger	1	5
149	16011465	Steindalsvegen til byen	5t	1t	2013-11-16 10:00	2013-11-16 15:00	2013-11-16 15:00	2013-11-16 16:00	Lørdag; tren på fem timer, test på neste.		ticket, passenger	1	5
150	16011465	Steindalsvegen til byen	5t	1t	2013-11-17 10:00	2013-11-17 15:00	2013-11-17 15:00	2013-11-17 16:00	Søndag; tren på fem timer, test på neste.		ticket, passenger	1	5
													
151	16010476	Samfundet fra byen	4d	1d	2013-09-09 00:00	2013-09-12 24:00	2013-09-13 00:00	2013-09-13 24:00	Tren på man-tors i uke 37, test på fredag.			2	1
152	16010476	Samfundet fra byen	4d	1d	2013-10-07 00:00	2013-10-10 24:00	2013-10-11 00:00	2013-10-11 24:00	Tren på man-tors i uke 41, test på fredag.			2	1
153	16010476	Samfundet fra byen	4d	1d	2013-12-02 00:00	2013-12-05 24:00	2013-12-06 00:00	2013-12-06 24:00	Tren på man-tors i uke 49, test på fredag.			2	1
154	16010476	Samfundet fra byen	4d	1d	2013-11-11 00:00	2013-11-15 00:00	2013-11-16 00:00	2013-11-16 24:00	Tren på man-tors i uke 46, test på fredag.		ticket	2	1
155	16010264	Lerkendal fra byen	4d	1d	2013-09-09 00:00	2013-09-12 24:00	2013-09-13 00:00	2013-09-13 24:00	Tren på man-tors i uke 37, test på fredag.	Samme for Lerkendal		2	1
156	16010264	Lerkendal fra byen	4d	1d	2013-10-07 00:00	2013-10-10 24:00	2013-10-11 00:00	2013-10-11 24:00	Tren på man-tors i uke 41, test på fredag.			2	1
157	16010264	Lerkendal fra byen	4d	1d	2013-12-02 00:00	2013-12-05 24:00	2013-12-06 00:00	2013-12-06 24:00	Tren på man-tors i uke 49, test på fredag.			2	1
158	16010264	Lerkendal fra byen	4d	1d	2013-11-11 00:00	2013-11-15 00:00	2013-11-16 00:00	2013-11-16 24:00	Tren på man-tors i uke 46, test på fredag.		ticket	2	1
159	16011465	Steindalsvegen til byen	4d	1d	2013-09-09 00:00	2013-09-12 24:00	2013-09-13 00:00	2013-09-13 24:00	Tren på man-tors i uke 37, test på fredag.	Samme for Steindalsvegen		2	1
160	16011465	Steindalsvegen til byen	4d	1d	2013-10-07 00:00	2013-10-10 24:00	2013-10-11 00:00	2013-10-11 24:00	Tren på man-tors i uke 41, test på fredag.			2	1
161	16011465	Steindalsvegen til byen	4d	1d	2013-12-02 00:00	2013-12-05 24:00	2013-12-06 00:00	2013-12-06 24:00	Tren på man-tors i uke 49, test på fredag.			2	1
162	16011465	Steindalsvegen til byen	4d	1d	2013-11-11 00:00	2013-11-15 00:00	2013-11-16 00:00	2013-11-16 24:00	Tren på man-tors i uke 46, test på fredag.		ticket	2	1
													
163	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-09-30 00:00	2013-09-30 24:00	Tren på hele uke 39, test på mandag uke 40.			3	1
164	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-01 00:00	2013-10-01 24:00	Tren på hele uke 39, test på tirsdag uke 40.			3	2
165	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-02 00:00	2013-10-02 24:00	Tren på hele uke 39, test på onsdag uke 40.			3	3
166	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-03 00:00	2013-10-03 24:00	Tren på hele uke 39, test på torsdag uke 40.			3	4
167	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-04 00:00	2013-10-04 24:00	Tren på hele uke 39, test på fredag uke 40.			3	5
168	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-05 00:00	2013-10-05 24:00	Tren på hele uke 39, test på lørdag uke 40.			3	6
169	16010476	Samfundet fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-06 00:00	2013-10-06 24:00	Tren på hele uke 39, test på søndag uke 40.			3	7
170	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-09-30 00:00	2013-09-30 24:00	Tren på hele uke 39, test på mandag uke 40.	Samme for Lerkendal		3	1
171	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-01 00:00	2013-10-01 24:00	Tren på hele uke 39, test på tirsdag uke 40.			3	2
172	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-02 00:00	2013-10-02 24:00	Tren på hele uke 39, test på onsdag uke 40.			3	3
173	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-03 00:00	2013-10-03 24:00	Tren på hele uke 39, test på torsdag uke 40.			3	4
174	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-04 00:00	2013-10-04 24:00	Tren på hele uke 39, test på fredag uke 40.			3	5
175	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-05 00:00	2013-10-05 24:00	Tren på hele uke 39, test på lørdag uke 40.			3	6
176	16010264	Lerkendal fra byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-06 00:00	2013-10-06 24:00	Tren på hele uke 39, test på søndag uke 40.			3	7
177	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-09-30 00:00	2013-09-30 24:00	Tren på hele uke 39, test på mandag uke 40.	Samme med passasjer	passenger	3	1
178	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-01 00:00	2013-10-01 24:00	Tren på hele uke 39, test på tirsdag uke 40.		passenger	3	2
179	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-02 00:00	2013-10-02 24:00	Tren på hele uke 39, test på onsdag uke 40.		passenger	3	3
180	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-03 00:00	2013-10-03 24:00	Tren på hele uke 39, test på torsdag uke 40.		passenger	3	4
181	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-04 00:00	2013-10-04 24:00	Tren på hele uke 39, test på fredag uke 40.		passenger	3	5
182	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-05 00:00	2013-10-05 24:00	Tren på hele uke 39, test på lørdag uke 40.		passenger	3	6
183	16011465	Steindalsvegen til byen	7d	1d	2013-09-23 00:00	2013-09-29 24:00	2013-10-06 00:00	2013-10-06 24:00	Tren på hele uke 39, test på søndag uke 40.		passenger	3	7
184	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-18 00:00	2013-11-18 24:00	Tren på hele uke 46, test på mandag uke 47.		ticket	3	1
185	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-19 00:00	2013-11-19 24:00	Tren på hele uke 46, test på tirsdag uke 47.		ticket	3	2
186	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-20 00:00	2013-11-20 24:00	Tren på hele uke 46, test på onsdag uke 47.		ticket	3	3
187	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-21 00:00	2013-11-21 24:00	Tren på hele uke 46, test på torsdag uke 47.		ticket	3	4
188	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-22 00:00	2013-11-22 24:00	Tren på hele uke 46, test på fredag uke 47.		ticket	3	5
189	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-23 00:00	2013-11-23 24:00	Tren på hele uke 46, test på lørdag uke 47.		ticket	3	6
190	16010476	Samfundet fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-24 00:00	2013-11-24 24:00	Tren på hele uke 46, test på søndag uke 47.		ticket	3	7
191	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-18 00:00	2013-11-18 24:00	Tren på hele uke 46, test på mandag uke 47.		ticket	3	1
192	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-19 00:00	2013-11-19 24:00	Tren på hele uke 46, test på tirsdag uke 47.		ticket	3	2
193	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-20 00:00	2013-11-20 24:00	Tren på hele uke 46, test på onsdag uke 47.		ticket	3	3
194	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-21 00:00	2013-11-21 24:00	Tren på hele uke 46, test på torsdag uke 47.		ticket	3	4
195	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-22 00:00	2013-11-22 24:00	Tren på hele uke 46, test på fredag uke 47.		ticket	3	5
196	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-23 00:00	2013-11-23 24:00	Tren på hele uke 46, test på lørdag uke 47.		ticket	3	6
197	16010264	Lerkendal fra byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-24 00:00	2013-11-24 24:00	Tren på hele uke 46, test på søndag uke 47.		ticket	3	7
198	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-18 00:00	2013-11-18 24:00	Tren på hele uke 46, test på mandag uke 47.		ticket	3	1
199	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-19 00:00	2013-11-19 24:00	Tren på hele uke 46, test på tirsdag uke 47.		ticket	3	2
200	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-20 00:00	2013-11-20 24:00	Tren på hele uke 46, test på onsdag uke 47.		ticket	3	3
201	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-21 00:00	2013-11-21 24:00	Tren på hele uke 46, test på torsdag uke 47.		ticket	3	4
202	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-22 00:00	2013-11-22 24:00	Tren på hele uke 46, test på fredag uke 47.		ticket	3	5
203	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-23 00:00	2013-11-23 24:00	Tren på hele uke 46, test på lørdag uke 47.		ticket	3	6
204	16011465	Steindalsvegen til byen	7d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-24 00:00	2013-11-24 24:00	Tren på hele uke 46, test på søndag uke 47.		ticket	3	7
													
205	16010476	Samfundet fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-09-30 00:00	2013-09-30 24:00	Tren på hverdagene i uke 39, test på mandag uke 40.			4	1
206	16010476	Samfundet fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-01 00:00	2013-10-01 24:00	Tren på hverdagene i uke 39, test på tirsdag uke 40.			4	2
207	16010476	Samfundet fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-02 00:00	2013-10-02 24:00	Tren på hverdagene i uke 39, test på onsdag uke 40.			4	3
208	16010476	Samfundet fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-03 00:00	2013-10-03 24:00	Tren på hverdagene i uke 39, test på torsdag uke 40.			4	4
209	16010476	Samfundet fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-04 00:00	2013-10-04 24:00	Tren på hverdagene i uke 39, test på fredag uke 40.			4	5
210	16010264	Lerkendal fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-09-30 00:00	2013-09-30 24:00	Tren på hverdagene i uke 39, test på mandag uke 40.	Samme for Lerkendal		4	1
211	16010264	Lerkendal fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-01 00:00	2013-10-01 24:00	Tren på hverdagene i uke 39, test på tirsdag uke 40.			4	2
212	16010264	Lerkendal fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-02 00:00	2013-10-02 24:00	Tren på hverdagene i uke 39, test på onsdag uke 40.			4	3
213	16010264	Lerkendal fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-03 00:00	2013-10-03 24:00	Tren på hverdagene i uke 39, test på torsdag uke 40.			4	4
214	16010264	Lerkendal fra byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-04 00:00	2013-10-04 24:00	Tren på hverdagene i uke 39, test på fredag uke 40.			4	5
215	16011465	Steindalsvegen til byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-09-30 00:00	2013-09-30 24:00	Tren på hverdagene i uke 39, test på mandag uke 40.	Samme for Steindalsvegen		4	1
216	16011465	Steindalsvegen til byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-01 00:00	2013-10-01 24:00	Tren på hverdagene i uke 39, test på tirsdag uke 40.			4	2
217	16011465	Steindalsvegen til byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-02 00:00	2013-10-02 24:00	Tren på hverdagene i uke 39, test på onsdag uke 40.			4	3
218	16011465	Steindalsvegen til byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-03 00:00	2013-10-03 24:00	Tren på hverdagene i uke 39, test på torsdag uke 40.			4	4
219	16011465	Steindalsvegen til byen	5d	1d	2013-09-23 00:00	2013-09-27 24:00	2013-10-04 00:00	2013-10-04 24:00	Tren på hverdagene i uke 39, test på fredag uke 40.			4	5
220	16010476	Samfundet fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-18 00:00	2013-11-18 24:00	Tren på hverdagene i uke 46, test på mandag uke 47.		ticket	4	1
221	16010476	Samfundet fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-19 00:00	2013-11-19 24:00	Tren på hverdagene i uke 46, test på tirsdag uke 47.		ticket	4	2
222	16010476	Samfundet fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-20 00:00	2013-11-20 24:00	Tren på hverdagene i uke 46, test på onsdag uke 47.		ticket	4	3
223	16010476	Samfundet fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-21 00:00	2013-11-21 24:00	Tren på hverdagene i uke 46, test på torsdag uke 47.		ticket	4	4
224	16010476	Samfundet fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-22 00:00	2013-11-22 24:00	Tren på hverdagene i uke 46, test på fredag uke 47.		ticket	4	5
225	16010264	Lerkendal fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-18 00:00	2013-11-18 24:00	Tren på hverdagene i uke 46, test på mandag uke 47.		ticket	4	1
226	16010264	Lerkendal fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-19 00:00	2013-11-19 24:00	Tren på hverdagene i uke 46, test på tirsdag uke 47.		ticket	4	2
227	16010264	Lerkendal fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-20 00:00	2013-11-20 24:00	Tren på hverdagene i uke 46, test på onsdag uke 47.		ticket	4	3
228	16010264	Lerkendal fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-21 00:00	2013-11-21 24:00	Tren på hverdagene i uke 46, test på torsdag uke 47.		ticket	4	4
229	16010264	Lerkendal fra byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-22 00:00	2013-11-22 24:00	Tren på hverdagene i uke 46, test på fredag uke 47.		ticket	4	5
230	16011465	Steindalsvegen til byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-18 00:00	2013-11-18 24:00	Tren på hverdagene i uke 46, test på mandag uke 47.		ticket	4	1
231	16011465	Steindalsvegen til byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-19 00:00	2013-11-19 24:00	Tren på hverdagene i uke 46, test på tirsdag uke 47.		ticket	4	2
232	16011465	Steindalsvegen til byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-20 00:00	2013-11-20 24:00	Tren på hverdagene i uke 46, test på onsdag uke 47.		ticket	4	3
233	16011465	Steindalsvegen til byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-21 00:00	2013-11-21 24:00	Tren på hverdagene i uke 46, test på torsdag uke 47.		ticket	4	4
234	16011465	Steindalsvegen til byen	5d	1d	2013-11-11 00:00	2013-11-15 24:00	2013-11-22 00:00	2013-11-22 24:00	Tren på hverdagene i uke 46, test på fredag uke 47.		ticket	4	5
													
235	16010476	Samfundet fra byen	14d	7d	2013-09-16 00:00	2013-09-29 24:00	2013-09-30 00:00	2013-10-06 24:00	Tren på uke 38-39, test på uke 40.			5	1
236	16010476	Samfundet fra byen	14d	7d	2013-11-11 00:00	2013-11-24 24:00	2013-11-25 00:00	2013-12-01 24:00	Tren på uke 46-47, test på uke 48.			5	1
237	16010264	Lerkendal fra byen	14d	7d	2013-09-16 00:00	2013-09-29 24:00	2013-09-30 00:00	2013-10-06 24:00	Tren på uke 38-39, test på uke 40.	Samme for Lerkendal		5	1
238	16010264	Lerkendal fra byen	14d	7d	2013-11-11 00:00	2013-11-24 24:00	2013-11-25 00:00	2013-12-01 24:00	Tren på uke 46-47, test på uke 48.			5	1
239	16011465	Steindalsvegen til byen	14d	7d	2013-09-16 00:00	2013-09-29 24:00	2013-09-30 00:00	2013-10-06 24:00	Tren på uke 38-39, test på uke 40.	Samme for Steindalsvegen		5	1
240	16011465	Steindalsvegen til byen	14d	7d	2013-11-11 00:00	2013-11-24 24:00	2013-11-25 00:00	2013-12-01 24:00	Tren på uke 46-47, test på uke 48.			5	1
													
241	16010476	Samfundet fra byen	21d	7d	2013-09-16 00:00	2013-10-06 24:00	2013-10-07 00:00	2013-10-13 24:00	Tren på uke 38-40, test på uke 41.			6	1
242	16010476	Samfundet fra byen	21d	7d	2013-10-07 00:00	2013-10-27 24:00	2013-10-28 00:00	2013-11-03 24:00	Tren på uke 41-43, test på uke 44			6	1
243	16010264	Lerkendal fra byen	21d	7d	2013-09-16 00:00	2013-10-06 24:00	2013-10-07 00:00	2013-10-13 24:00	Tren på uke 38-40, test på uke 41.	Samme for Lerkendal		6	1
244	16010264	Lerkendal fra byen	21d	7d	2013-10-07 00:00	2013-10-27 24:00	2013-10-28 00:00	2013-11-03 24:00	Tren på uke 41-43, test på uke 44			6	1
245	16011465	Steindalsvegen til byen	21d	7d	2013-09-16 00:00	2013-10-06 24:00	2013-10-07 00:00	2013-10-13 24:00	Tren på uke 38-40, test på uke 41.	Samme for Steindalsvegen		6	1
246	16011465	Steindalsvegen til byen	21d	7d	2013-10-07 00:00	2013-10-27 24:00	2013-10-28 00:00	2013-11-03 24:00	Tren på uke 41-43, test på uke 44			6	1
247	16010476	Samfundet fra byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-09-23 00:00	2013-10-06 24:00	Tren på uke 36-38, test på uke 39.			6	1
248	16010476	Samfundet fra byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-02 00:00	2013-12-08 24:00	Tren på uke 46-48, test på uke 49.			6	1
249	16010264	Lerkendal fra byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-09-23 00:00	2013-10-06 24:00	Tren på uke 36-38, test på uke 39.	Samme for Lerkendal		6	1
250	16010264	Lerkendal fra byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-02 00:00	2013-12-08 24:00	Tren på uke 46-48, test på uke 49.			6	1
251	16011465	Steindalsvegen til byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-09-23 00:00	2013-10-06 24:00	Tren på uke 36-38, test på uke 39.	Samme for Steindalsvegen		6	1
252	16011465	Steindalsvegen til byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-02 00:00	2013-12-08 24:00	Tren på uke 46-48, test på uke 49.			6	1
													
253	16010476	Samfundet fra byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-09-23 00:00	2013-10-06 24:00	Tren på uke 36-38, test på uke 40.			7	1
254	16010476	Samfundet fra byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-09 00:00	2013-12-15 24:00	Tren på uke 46-48, test på uke 50.			7	1
255	16010264	Lerkendal fra byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-09-23 00:00	2013-10-06 24:00	Tren på uke 36-38, test på uke 40.			7	1
256	16010264	Lerkendal fra byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-09 00:00	2013-12-15 24:00	Tren på uke 46-48, test på uke 50.			7	1
257	16011465	Steindalsvegen til byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-09-23 00:00	2013-10-06 24:00	Tren på uke 36-38, test på uke 40.			7	1
258	16011465	Steindalsvegen til byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-09 00:00	2013-12-15 24:00	Tren på uke 46-48, test på uke 50.			7	1
													
259	16010476	Samfundet fra byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-10-07 00:00	2013-10-13 24:00	Tren på uke 36-38, test på uke 41.			8	1
260	16010476	Samfundet fra byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-16 00:00	2013-12-22 24:00	Tren på uke 46-48, test på uke 51.			8	1
261	16010264	Lerkendal fra byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-10-07 00:00	2013-10-13 24:00	Tren på uke 36-38, test på uke 41.			8	1
262	16010264	Lerkendal fra byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-16 00:00	2013-12-22 24:00	Tren på uke 46-48, test på uke 51.			8	1
263	16011465	Steindalsvegen til byen	21d	7d	2013-09-02 00:00	2013-09-22 24:00	2013-10-07 00:00	2013-10-13 24:00	Tren på uke 36-38, test på uke 41.			8	1
264	16011465	Steindalsvegen til byen	21d	7d	2013-11-11 00:00	2013-12-01 24:00	2013-12-16 00:00	2013-12-22 24:00	Tren på uke 46-48, test på uke 51.			8	1
													
265	16010476	Samfundet fra byen	1d	1d	2013-09-16		2013-09-23		Tren på én mandag, test på neste.			9	1
266	16010476	Samfundet fra byen	1d	1d	2013-09-17		2013-09-24		Tren på én tirsdag, test på neste.			9	2
267	16010476	Samfundet fra byen	1d	1d	2013-09-18		2013-09-25		Tren på én onsdag, test på neste.			9	3
268	16010476	Samfundet fra byen	1d	1d	2013-09-19		2013-09-26		Tren på én torsdag, test på neste.			9	4
269	16010476	Samfundet fra byen	1d	1d	2013-09-20		2013-09-27		Tren på én fredag, test på neste.			9	5
270	16010476	Samfundet fra byen	1d	1d	2013-09-21		2013-09-28		Tren på én lørdag, test på neste.			9	6
271	16010476	Samfundet fra byen	1d	1d	2013-09-22		2013-09-29		Tren på én søndag, test på neste.			9	7
272	16010264	Lerkendal fra byen	1d	1d	2013-09-16		2013-09-23		Tren på én mandag, test på neste.	Samme for Lerkendal		9	1
273	16010264	Lerkendal fra byen	1d	1d	2013-09-17		2013-09-24		Tren på én tirsdag, test på neste.			9	2
274	16010264	Lerkendal fra byen	1d	1d	2013-09-18		2013-09-25		Tren på én onsdag, test på neste.			9	3
275	16010264	Lerkendal fra byen	1d	1d	2013-09-19		2013-09-26		Tren på én torsdag, test på neste.			9	4
276	16010264	Lerkendal fra byen	1d	1d	2013-09-20		2013-09-27		Tren på én fredag, test på neste.			9	5
277	16010264	Lerkendal fra byen	1d	1d	2013-09-21		2013-09-28		Tren på én lørdag, test på neste.			9	6
278	16010264	Lerkendal fra byen	1d	1d	2013-09-22		2013-09-29		Tren på én søndag, test på neste.			9	7
279	16011465	Steindalsvegen til byen	1d	1d	2013-09-16		2013-09-23		Tren på én mandag, test på neste.	Samme for Steindalsvegen		9	1
280	16011465	Steindalsvegen til byen	1d	1d	2013-09-17		2013-09-24		Tren på én tirsdag, test på neste.			9	2
281	16011465	Steindalsvegen til byen	1d	1d	2013-09-18		2013-09-25		Tren på én onsdag, test på neste.			9	3
282	16011465	Steindalsvegen til byen	1d	1d	2013-09-19		2013-09-26		Tren på én torsdag, test på neste.			9	4
283	16011465	Steindalsvegen til byen	1d	1d	2013-09-20		2013-09-27		Tren på én fredag, test på neste.			9	5
284	16011465	Steindalsvegen til byen	1d	1d	2013-09-21		2013-09-28		Tren på én lørdag, test på neste.			9	6
285	16011465	Steindalsvegen til byen	1d	1d	2013-09-22		2013-09-29		Tren på én søndag, test på neste.			9	7
													
286	16010476	Samfundet fra byen	2d	1d	2013-09-16,2013-09-23		2013-09-30		Tren på to mandager, test på neste.			10	1
287	16010476	Samfundet fra byen	2d	1d	2013-09-17,2013-09-24		2013-10-01		Tren på to tirsdager, test på neste.			10	2
288	16010476	Samfundet fra byen	2d	1d	2013-09-18,2013-09-25		2013-10-02		Tren på to onsdager, test på neste.			10	3
289	16010476	Samfundet fra byen	2d	1d	2013-09-19,2013-09-26		2013-10-03		Tren på to torsdager, test på neste.			10	4
290	16010476	Samfundet fra byen	2d	1d	2013-09-20,2013-09-27		2013-10-04		Tren på to fredager, test på neste.			10	5
291	16010476	Samfundet fra byen	2d	1d	2013-09-21,2013-09-28		2013-10-05		Tren på to lørdager, test på neste.			10	6
292	16010476	Samfundet fra byen	2d	1d	2013-09-22,2013-09-29		2013-10-06		Tren på to søndager, test på neste.			10	7
293	16010264	Lerkendal fra byen	2d	1d	2013-09-16,2013-09-23		2013-09-30		Tren på to mandager, test på neste.	Samme for Lerkendal		10	1
294	16010264	Lerkendal fra byen	2d	1d	2013-09-17,2013-09-24		2013-10-01		Tren på to tirsdager, test på neste.			10	2
295	16010264	Lerkendal fra byen	2d	1d	2013-09-18,2013-09-25		2013-10-02		Tren på to onsdager, test på neste.			10	3
296	16010264	Lerkendal fra byen	2d	1d	2013-09-19,2013-09-26		2013-10-03		Tren på to torsdager, test på neste.			10	4
297	16010264	Lerkendal fra byen	2d	1d	2013-09-20,2013-09-27		2013-10-04		Tren på to fredager, test på neste.			10	5
298	16010264	Lerkendal fra byen	2d	1d	2013-09-21,2013-09-28		2013-10-05		Tren på to lørdager, test på neste.			10	6
299	16010264	Lerkendal fra byen	2d	1d	2013-09-22,2013-09-29		2013-10-06		Tren på to søndager, test på neste.			10	7
300	16011465	Steindalsvegen til byen	2d	1d	2013-09-16,2013-09-23		2013-09-30		Tren på to mandager, test på neste.	Samme for Steindalsvegen		10	1
301	16011465	Steindalsvegen til byen	2d	1d	2013-09-17,2013-09-24		2013-10-01		Tren på to tirsdager, test på neste.			10	2
302	16011465	Steindalsvegen til byen	2d	1d	2013-09-18,2013-09-25		2013-10-02		Tren på to onsdager, test på neste.			10	3
303	16011465	Steindalsvegen til byen	2d	1d	2013-09-19,2013-09-26		2013-10-03		Tren på to torsdager, test på neste.			10	4
304	16011465	Steindalsvegen til byen	2d	1d	2013-09-20,2013-09-27		2013-10-04		Tren på to fredager, test på neste.			10	5
305	16011465	Steindalsvegen til byen	2d	1d	2013-09-21,2013-09-28		2013-10-05		Tren på to lørdager, test på neste.			10	6
306	16011465	Steindalsvegen til byen	2d	1d	2013-09-22,2013-09-29		2013-10-06		Tren på to søndager, test på neste.			10	7
													
307	16010476	Samfundet fra byen	3d	1d	2013-09-16,2013-09-23,2013-09-30		2013-10-07		Tren på tre mandager, test på neste.			11	1
308	16010476	Samfundet fra byen	3d	1d	2013-09-17,2013-09-24,2013-10-01		2013-10-08		Tren på tre tirsdager, test på neste.			11	2
309	16010476	Samfundet fra byen	3d	1d	2013-09-18,2013-09-25,2013-10-02		2013-10-09		Tren på tre onsdager, test på neste.			11	3
310	16010476	Samfundet fra byen	3d	1d	2013-09-19,2013-09-26,2013-10-03		2013-10-10		Tren på tre torsdager, test på neste.			11	4
311	16010476	Samfundet fra byen	3d	1d	2013-09-20,2013-09-27,2013-10-04		2013-10-11		Tren på tre fredager, test på neste.			11	5
312	16010476	Samfundet fra byen	3d	1d	2013-09-21,2013-09-28,2013-10-05		2013-10-12		Tren på tre lørdager, test på neste.			11	6
313	16010476	Samfundet fra byen	3d	1d	2013-09-22,2013-09-29,2013-10-06		2013-10-13		Tren på tre søndager, test på neste.			11	7
314	16010264	Lerkendal fra byen	3d	1d	2013-09-16,2013-09-23,2013-09-30		2013-10-07		Tren på tre mandager, test på neste.	Samme for Lerkendal		11	1
315	16010264	Lerkendal fra byen	3d	1d	2013-09-17,2013-09-24,2013-10-01		2013-10-08		Tren på tre tirsdager, test på neste.			11	2
316	16010264	Lerkendal fra byen	3d	1d	2013-09-18,2013-09-25,2013-10-02		2013-10-09		Tren på tre onsdager, test på neste.			11	3
317	16010264	Lerkendal fra byen	3d	1d	2013-09-19,2013-09-26,2013-10-03		2013-10-10		Tren på tre torsdager, test på neste.			11	4
318	16010264	Lerkendal fra byen	3d	1d	2013-09-20,2013-09-27,2013-10-04		2013-10-11		Tren på tre fredager, test på neste.			11	5
319	16010264	Lerkendal fra byen	3d	1d	2013-09-21,2013-09-28,2013-10-05		2013-10-12		Tren på tre lørdager, test på neste.			11	6
320	16010264	Lerkendal fra byen	3d	1d	2013-09-22,2013-09-29,2013-10-06		2013-10-13		Tren på tre søndager, test på neste.			11	7
321	16011465	Steindalsvegen til byen	3d	1d	2013-09-16,2013-09-23,2013-09-30		2013-10-07		Tren på tre mandager, test på neste.	Samme for Steindalsvegen		11	1
322	16011465	Steindalsvegen til byen	3d	1d	2013-09-17,2013-09-24,2013-10-01		2013-10-08		Tren på tre tirsdager, test på neste.			11	2
323	16011465	Steindalsvegen til byen	3d	1d	2013-09-18,2013-09-25,2013-10-02		2013-10-09		Tren på tre onsdager, test på neste.			11	3
324	16011465	Steindalsvegen til byen	3d	1d	2013-09-19,2013-09-26,2013-10-03		2013-10-10		Tren på tre torsdager, test på neste.			11	4
325	16011465	Steindalsvegen til byen	3d	1d	2013-09-20,2013-09-27,2013-10-04		2013-10-11		Tren på tre fredager, test på neste.			11	5
326	16011465	Steindalsvegen til byen	3d	1d	2013-09-21,2013-09-28,2013-10-05		2013-10-12		Tren på tre lørdager, test på neste.			11	6
327	16011465	Steindalsvegen til byen	3d	1d	2013-09-22,2013-09-29,2013-10-06		2013-10-13		Tren på tre søndager, test på neste.			11	7
													
328	16010476	Samfundet fra byen	4d	1d	2013-09-16,2013-09-23,2013-09-30,2013-10-07		2013-10-14		Tren på fire mandager, test på neste.			12	1
329	16010476	Samfundet fra byen	4d	1d	2013-09-17,2013-09-24,2013-10-01,2013-10-08		2013-10-15		Tren på fire tirsdager, test på neste.			12	2
330	16010476	Samfundet fra byen	4d	1d	2013-09-18,2013-09-25,2013-10-02,2013-10-09		2013-10-16		Tren på fire onsdager, test på neste.			12	3
331	16010476	Samfundet fra byen	4d	1d	2013-09-19,2013-09-26,2013-10-03,2013-10-10		2013-10-17		Tren på fire torsdager, test på neste.			12	4
332	16010476	Samfundet fra byen	4d	1d	2013-09-20,2013-09-27,2013-10-04,2013-10-11		2013-10-18		Tren på fire fredager, test på neste.			12	5
333	16010476	Samfundet fra byen	4d	1d	2013-09-21,2013-09-28,2013-10-05,2013-10-12		2013-10-19		Tren på fire lørdager, test på neste.			12	6
334	16010476	Samfundet fra byen	4d	1d	2013-09-22,2013-09-29,2013-10-06,2013-10-13		2013-10-20		Tren på fire søndager, test på neste.			12	7
335	16010264	Lerkendal fra byen	4d	1d	2013-09-16,2013-09-23,2013-09-30,2013-10-07		2013-10-14		Tren på fire mandager, test på neste.	Samme for Lerkendal		12	1
336	16010264	Lerkendal fra byen	4d	1d	2013-09-17,2013-09-24,2013-10-01,2013-10-08		2013-10-15		Tren på fire tirsdager, test på neste.			12	2
337	16010264	Lerkendal fra byen	4d	1d	2013-09-18,2013-09-25,2013-10-02,2013-10-09		2013-10-16		Tren på fire onsdager, test på neste.			12	3
338	16010264	Lerkendal fra byen	4d	1d	2013-09-19,2013-09-26,2013-10-03,2013-10-10		2013-10-17		Tren på fire torsdager, test på neste.			12	4
339	16010264	Lerkendal fra byen	4d	1d	2013-09-20,2013-09-27,2013-10-04,2013-10-11		2013-10-18		Tren på fire fredager, test på neste.			12	5
340	16010264	Lerkendal fra byen	4d	1d	2013-09-21,2013-09-28,2013-10-05,2013-10-12		2013-10-19		Tren på fire lørdager, test på neste.			12	6
341	16010264	Lerkendal fra byen	4d	1d	2013-09-22,2013-09-29,2013-10-06,2013-10-13		2013-10-20		Tren på fire søndager, test på neste.			12	7
342	16011465	Steindalsvegen til byen	4d	1d	2013-09-16,2013-09-23,2013-09-30,2013-10-07		2013-10-14		Tren på fire mandager, test på neste.	Samme for Steindalsvegen		12	1
343	16011465	Steindalsvegen til byen	4d	1d	2013-09-17,2013-09-24,2013-10-01,2013-10-08		2013-10-15		Tren på fire tirsdager, test på neste.			12	2
344	16011465	Steindalsvegen til byen	4d	1d	2013-09-18,2013-09-25,2013-10-02,2013-10-09		2013-10-16		Tren på fire onsdager, test på neste.			12	3
345	16011465	Steindalsvegen til byen	4d	1d	2013-09-19,2013-09-26,2013-10-03,2013-10-10		2013-10-17		Tren på fire torsdager, test på neste.			12	4
346	16011465	Steindalsvegen til byen	4d	1d	2013-09-20,2013-09-27,2013-10-04,2013-10-11		2013-10-18		Tren på fire fredager, test på neste.			12	5
347	16011465	Steindalsvegen til byen	4d	1d	2013-09-21,2013-09-28,2013-10-05,2013-10-12		2013-10-19		Tren på fire lørdager, test på neste.			12	6
348	16011465	Steindalsvegen til byen	4d	1d	2013-09-22,2013-09-29,2013-10-06,2013-10-13		2013-10-20		Tren på fire søndager, test på neste.			12	7
													
349	16010476	Samfundet fra byen	5d	1d	2013-09-09,2013-09-16,2013-09-23,2013-09-30,2013-10-07		2013-10-14		Tren på fem mandager, test på neste.			13	1
350	16010476	Samfundet fra byen	5d	1d	2013-09-10,2013-09-17,2013-09-24,2013-10-01,2013-10-08		2013-10-15		Tren på fem tirsdager, test på neste.			13	2
351	16010476	Samfundet fra byen	5d	1d	2013-09-11,2013-09-18,2013-09-25,2013-10-02,2013-10-09		2013-10-16		Tren på fem onsdager, test på neste.			13	3
352	16010476	Samfundet fra byen	5d	1d	2013-09-12,2013-09-19,2013-09-26,2013-10-03,2013-10-10		2013-10-17		Tren på fem torsdager, test på neste.			13	4
353	16010476	Samfundet fra byen	5d	1d	2013-09-13,2013-09-20,2013-09-27,2013-10-04,2013-10-11		2013-10-18		Tren på fem fredager, test på neste.			13	5
354	16010476	Samfundet fra byen	5d	1d	2013-09-14,2013-09-21,2013-09-28,2013-10-05,2013-10-12		2013-10-19		Tren på fem lørdager, test på neste.			13	6
355	16010476	Samfundet fra byen	5d	1d	2013-09-15,2013-09-22,2013-09-29,2013-10-06,2013-10-13		2013-10-20		Tren på fem søndager, test på neste.			13	7
356	16010264	Lerkendal fra byen	5d	1d	2013-09-09,2013-09-16,2013-09-23,2013-09-30,2013-10-07		2013-10-14		Tren på fem mandager, test på neste.	Samme for Lerkendal		13	1
357	16010264	Lerkendal fra byen	5d	1d	2013-09-10,2013-09-17,2013-09-24,2013-10-01,2013-10-08		2013-10-15		Tren på fem tirsdager, test på neste.			13	2
358	16010264	Lerkendal fra byen	5d	1d	2013-09-11,2013-09-18,2013-09-25,2013-10-02,2013-10-09		2013-10-16		Tren på fem onsdager, test på neste.			13	3
359	16010264	Lerkendal fra byen	5d	1d	2013-09-12,2013-09-19,2013-09-26,2013-10-03,2013-10-10		2013-10-17		Tren på fem torsdager, test på neste.			13	4
360	16010264	Lerkendal fra byen	5d	1d	2013-09-13,2013-09-20,2013-09-27,2013-10-04,2013-10-11		2013-10-18		Tren på fem fredager, test på neste.			13	5
361	16010264	Lerkendal fra byen	5d	1d	2013-09-14,2013-09-21,2013-09-28,2013-10-05,2013-10-12		2013-10-19		Tren på fem lørdager, test på neste.			13	6
362	16010264	Lerkendal fra byen	5d	1d	2013-09-15,2013-09-22,2013-09-29,2013-10-06,2013-10-13		2013-10-20		Tren på fem søndager, test på neste.			13	7
363	16011465	Steindalsvegen til byen	5d	1d	2013-09-09,2013-09-16,2013-09-23,2013-09-30,2013-10-07		2013-10-14		Tren på fem mandager, test på neste.	Samme for Steindalsvegen		13	1
364	16011465	Steindalsvegen til byen	5d	1d	2013-09-10,2013-09-17,2013-09-24,2013-10-01,2013-10-08		2013-10-15		Tren på fem tirsdager, test på neste.			13	2
365	16011465	Steindalsvegen til byen	5d	1d	2013-09-11,2013-09-18,2013-09-25,2013-10-02,2013-10-09		2013-10-16		Tren på fem onsdager, test på neste.			13	3
366	16011465	Steindalsvegen til byen	5d	1d	2013-09-12,2013-09-19,2013-09-26,2013-10-03,2013-10-10		2013-10-17		Tren på fem torsdager, test på neste.			13	4
367	16011465	Steindalsvegen til byen	5d	1d	2013-09-13,2013-09-20,2013-09-27,2013-10-04,2013-10-11		2013-10-18		Tren på fem fredager, test på neste.			13	5
368	16011465	Steindalsvegen til byen	5d	1d	2013-09-14,2013-09-21,2013-09-28,2013-10-05,2013-10-12		2013-10-19		Tren på fem lørdager, test på neste.			13	6
369	16011465	Steindalsvegen til byen	5d	1d	2013-09-15,2013-09-22,2013-09-29,2013-10-06,2013-10-13		2013-10-20		Tren på fem søndager, test på neste.			13	7
													
370	16010476	Samfundet fra byen	1m	1m	2013-10-01 00:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-30 24:00	Tren på oktober, test på november.			14	1
371	16010264	Lerkendal fra byen	1m	1m	2013-10-01 00:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-30 24:00	Tren på oktober, test på november.			14	1
372	16011465	Steindalsvegen til byen	1m	1m	2013-10-01 00:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-30 24:00	Tren på oktober, test på november.			14	1
													
373	16010476	Samfundet fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-07 24:00	Tren på oktober, test på første uke i november.			15	1
381	16011465	Steindalsvegen til byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-07 24:00	Tren på oktober, test på første uke i november.			15	1
377	16010264	Lerkendal fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-07 24:00	Tren på oktober, test på første uke i november.			15	1
													
374	16010476	Samfundet fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-08 00:00	2013-11-14 24:00	Tren på oktober, test på andre uke i november.			15	2
378	16010264	Lerkendal fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-08 00:00	2013-11-14 24:00	Tren på oktober, test på andre uke i november.			15	2
382	16011465	Steindalsvegen til byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-08 00:00	2013-11-14 24:00	Tren på oktober, test på andre uke i november.			15	2
													
375	16010476	Samfundet fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-15 00:00	2013-11-21 24:00	Tren på oktober, test på tredje uke i november.			15	3
379	16010264	Lerkendal fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-15 00:00	2013-11-21 24:00	Tren på oktober, test på tredje uke i november.			15	3
383	16011465	Steindalsvegen til byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-15 00:00	2013-11-21 24:00	Tren på oktober, test på tredje uke i november.			15	3
													
376	16010476	Samfundet fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-22 00:00	2013-11-28 24:00	Tren på oktober, test på fjerde uke i november.			15	4
380	16010264	Lerkendal fra byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-22 00:00	2013-11-28 24:00	Tren på oktober, test på fjerde uke i november.			15	4
384	16011465	Steindalsvegen til byen	1m	7d	2013-10-01 00:00	2013-10-31 24:00	2013-11-22 00:00	2013-11-28 24:00	Tren på oktober, test på fjerde uke i november.			15	4
													
385	16010476	Samfundet fra byen	2m	1m	2013-09-01 11:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-30 24:00	Tren på sept-okt, test på november.			16	1
386	16010264	Lerkendal fra byen	2m	1m	2013-09-01 11:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-30 24:00	Tren på sept-okt, test på november.	Samme for Lerkendal		16	1
387	16011465	Steindalsvegen til byen	2m	1m	2013-09-01 11:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-30 24:00	Tren på sept-okt, test på november.	Samme for Steindalsvegen		16	1
													
388	16010476	Samfundet fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-07 24:00	Tren på sept-okt, test på første uke i november.			17	1
392	16010264	Lerkendal fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-07 24:00	Tren på sept-okt, test på første uke i november.			17	1
396	16011465	Steindalsvegen til byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-01 00:00	2013-11-07 24:00	Tren på sept-okt, test på første uke i november.			17	1
													
389	16010476	Samfundet fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-08 00:00	2013-11-14 24:00	Tren på sept-okt, test på andre uke i november.			17	2
397	16011465	Steindalsvegen til byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-08 00:00	2013-11-14 24:00	Tren på sept-okt, test på andre uke i november.			17	2
393	16010264	Lerkendal fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-08 00:00	2013-11-14 24:00	Tren på sept-okt, test på andre uke i november.			17	2
													
390	16010476	Samfundet fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-15 00:00	2013-11-21 24:00	Tren på sept-okt, test på tredje uke i november.			17	3
394	16010264	Lerkendal fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-15 00:00	2013-11-21 24:00	Tren på sept-okt, test på tredje uke i november.			17	3
398	16011465	Steindalsvegen til byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-15 00:00	2013-11-21 24:00	Tren på sept-okt, test på tredje uke i november.			17	3
													
391	16010476	Samfundet fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-22 00:00	2013-11-28 24:00	Tren på sept-okt, test på fjerde uke i november.			17	4
395	16010264	Lerkendal fra byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-22 00:00	2013-11-28 24:00	Tren på sept-okt, test på fjerde uke i november.			17	4
399	16011465	Steindalsvegen til byen	2m	7d	2013-09-01 11:00	2013-10-31 24:00	2013-11-22 00:00	2013-11-28 24:00	Tren på sept-okt, test på fjerde uke i november.			17	4
													
400	16010476	Samfundet fra byen	100d	1d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-09 24:00	Tren på 100 dager, test på 1 dag.			18	1
401	16010476	Samfundet fra byen	100d	1d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-02-24 24:00	Tren på 100 dager, test på 1 dag i 2014.			18	2
402	16010476	Samfundet fra byen	100d	2d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-10 24:00	Tren på 100 dager, test på 2 dager.			18	3
403	16010476	Samfundet fra byen	100d	2d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-02-25 24:00	Tren på 100 dager, test på 2 dager i 2014.			18	4
404	16010476	Samfundet fra byen	100d	7d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-15 24:00	Tren på 100 dager, test på 7 dager.			18	5
405	16010476	Samfundet fra byen	100d	7d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-03-02 24:00	Tren på 100 dager, test på 7 dager i 2014.			18	6
406	16010476	Samfundet fra byen	100d	14d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-23 24:00	Tren på 100 dager, test på 14 dager.			18	7
407	16010476	Samfundet fra byen	100d	14d	2013-09-01 11:00	2013-12-08 24:00	2014-01-06 00:00	2014-01-26 24:00	Tren på 100 dager, test på 14 dager i 2014.			18	8
408	16010264	Lerkendal fra byen	100d	1d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-09 24:00	Tren på 100 dager, test på 1 dag.	Samme for Lerkendal		18	9
409	16010264	Lerkendal fra byen	100d	1d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-02-24 24:00	Tren på 100 dager, test på 1 dag i 2014.			18	10
410	16010264	Lerkendal fra byen	100d	2d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-10 24:00	Tren på 100 dager, test på 2 dager.			18	11
411	16010264	Lerkendal fra byen	100d	2d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-02-25 24:00	Tren på 100 dager, test på 2 dager i 2014.			18	12
412	16010264	Lerkendal fra byen	100d	7d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-15 24:00	Tren på 100 dager, test på 7 dager.			18	13
413	16010264	Lerkendal fra byen	100d	7d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-03-02 24:00	Tren på 100 dager, test på 7 dager i 2014.			18	14
414	16010264	Lerkendal fra byen	100d	14d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-23 24:00	Tren på 100 dager, test på 14 dager.			18	15
415	16010264	Lerkendal fra byen	100d	14d	2013-09-01 11:00	2013-12-08 24:00	2014-01-06 00:00	2014-01-26 24:00	Tren på 100 dager, test på 14 dager i 2014.			18	16
416	16011465	Steindalsvegen til byen	100d	1d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-09 24:00	Tren på 100 dager, test på 1 dag.	Samme for Steindalsvegen		18	17
417	16011465	Steindalsvegen til byen	100d	1d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-02-24 24:00	Tren på 100 dager, test på 1 dag i 2014.			18	18
418	16011465	Steindalsvegen til byen	100d	2d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-10 24:00	Tren på 100 dager, test på 2 dager.			18	19
419	16011465	Steindalsvegen til byen	100d	2d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-02-25 24:00	Tren på 100 dager, test på 2 dager i 2014.			18	20
420	16011465	Steindalsvegen til byen	100d	7d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-15 24:00	Tren på 100 dager, test på 7 dager.			18	21
421	16011465	Steindalsvegen til byen	100d	7d	2013-09-01 11:00	2013-12-08 24:00	2014-02-24 00:00	2014-03-02 24:00	Tren på 100 dager, test på 7 dager i 2014.			18	22
422	16011465	Steindalsvegen til byen	100d	14d	2013-09-01 11:00	2013-12-08 24:00	2013-12-09 00:00	2013-12-23 24:00	Tren på 100 dager, test på 14 dager.			18	23
423	16011465	Steindalsvegen til byen	100d	14d	2013-09-01 11:00	2013-12-08 24:00	2014-01-06 00:00	2014-01-26 24:00	Tren på 100 dager, test på 14 dager i 2014.			18	24";


        public static void GenerateWeka()
        {
            var data = DataSet().ToList();

            var db = new SMIODataContext();
            var stops = data.Select(p => p.BusStop).Distinct().ToArray();
            var passages = db.BusStopPassages.Where(p => stops.Contains(p.BusStopCode)).ToList();

            for (var i = 0; i < data.Count; i++)
            {
                var file = data[i];
                var rows = passages.Where(p => p.BusStopCode == file.BusStop);
                //if (file.Keywords.Contains("passenger"))
                //    rows = rows.Where(p => p.Vehicle == 5872);
                var trainingSet = BusStopPassage.FilterRows(rows, file.TrainIntervals);
                var testSet = BusStopPassage.FilterRows(rows, file.TestIntervals);

                if (!trainingSet.Any() || !testSet.Any())
                {
                    Console.WriteLine("Empty data set! " + file.Filename);
                    continue;
                }

                var format = file.Filename + "{0}.csv";
                BusStopPassage.SaveRows(trainingSet, String.Format(format, "train"), file.Keywords);
                BusStopPassage.SaveRows(testSet, String.Format(format, "test"), file.Keywords);
            }
        }
    }

    public class DataSetInfo
    {
        private ListViewItem _lvi;
        private string _filename;

        public string Filename
        {
            get { return _filename; }
            set
            {
                _filename = value;
                Read();
            }
        }

        public string BusStop { get; set; }
        public IEnumerable<Tuple<DateTime, DateTime>> TrainIntervals { get; set; }
        public IEnumerable<Tuple<DateTime, DateTime>> TestIntervals { get; set; }
        public string[] Keywords { get; set; }
        public string Comment { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }
        public string Subgroup { get; set; }
        public string[] OriginalLine { get; set; }

        public Dictionary<string, string[]> Eval { get; set; }
        public string[] TrainingSet { get; set; }
        public string[] TestingSet { get; set; }

        public string TrainingFile { get { return Data.GetWekaFile("finalDataSet", Filename + "train.csv"); } }
        public string TestFile { get { return Data.GetWekaFile("finalDataSet", Filename + "test.csv"); } }
        public string[] EvaluationFiles { get { return _classifiers.Select(p => Data.GetWekaFile("finalDataSet", Filename + "eval." + p + ".txt")).ToArray(); } }

        private static string[] _classifiers { get; set; }

        public ListViewItem ListViewItem
        {
            get
            {
                if (_lvi == null)
                {
                    _lvi = new ListViewItem(OriginalLine);
                    _lvi.SubItems.Add((TrainingSet.Length - 1).ToString());
                    _lvi.SubItems.Add((TestingSet.Length - 1).ToString());
                    _lvi.Tag = this;

                    var addFields = new[]
                    {"Mean absolute error: ", "Correlation coefficient: ", "Root mean squared error: "};

                    foreach (var find in addFields)
                        foreach (var cls in _classifiers)
                            _lvi.SubItems.Add(Eval.ContainsKey(cls)
                                ? Eval[cls].Single(p => p.StartsWith(find)).Replace(find, "")
                                : "N/A");

                    var folders = new[]
                    {
                        Data.GetWekaFile("attributes","svm"),
                        //Data.GetWekaFile("attributes",""),
                        //Data.GetWekaFile("attributes","")

                        //Data.GetWekaFile("parameterExperimentation", "Results", "04-07 Ann Combo (1-dagers + utenActualArrival, skikkelig)"),
                        //Data.GetWekaFile("parameterExperimentation", "Results", "03-28 UtenActualArrival, skikkelig",
                            //"Knn skikkelig")
                    };

                    foreach (var folder in folders)
                    {

                        if (PowerSetResultSet.HasFile(folder, Filename))
                        {
                            MiniPowerSet pow = new MiniPowerSet(folder, Filename, "svm");
                            var results = EnumUtil.GetValues<MiniPowType>();
                            foreach (var miniPowType in results)
                            {
                                try
                                {
                                    PowerSetResult mae = pow.Results().GetValueOrDefault(miniPowType.ToString(), null);

                                    _lvi.SubItems.Add(mae != null ? mae.MeanAbsoluteError.ToString("N3") : "N/A AAS");
                                }
                                catch (Exception ex)
                                {
                                    _lvi.SubItems.Add("ERROR AAS");
                                }
                            }
                        }
                        else
                        {
                            _lvi.SubItems.Add("N/A AAS");
                        }
                    }
                   
                }
                return _lvi;
            }
        }

        private void Read()
        {
            var files = System.IO.Directory.GetFiles(Data.GetWekaFile("finalDataSet"));
            if (_classifiers == null)
                _classifiers = files.
                    Where(p => p.EndsWith(".txt")).
                    Select(System.IO.Path.GetFileNameWithoutExtension).
                    Select(p => p.Split('.')[1]).
                    Distinct().
                    ToArray();

            var trainingFile = Data.GetWekaFile("finalDataSet", Filename + "train.csv");
            var testingFile = Data.GetWekaFile("finalDataSet", Filename + "test.csv");
            TrainingSet = System.IO.File.Exists(trainingFile) ? System.IO.File.ReadAllLines(trainingFile, Encoding.Default) : new string[0];
            TestingSet = System.IO.File.Exists(trainingFile) ? System.IO.File.ReadAllLines(testingFile, Encoding.Default) : new string[0];

            foreach (var cls in _classifiers)
            {
                var file = Data.GetWekaFile("finalDataSet", Filename + "eval." + cls + ".txt");
                if (System.IO.File.Exists(file))
                    Eval.Add(cls, System.IO.File.ReadAllLines(file, Encoding.Default));
            }
        }

        public DataSetInfo ()
        {
            Eval = new Dictionary<string, string[]>();
        }
    }
}
