﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.LinksTravelTime;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.Extensions;

namespace FlashNet_LTT_Analyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var blah = new SMIODataContext();

            var path = Data.GetDataFolder(Data.DataFolders.LinkTravelTimes);

            var files = System.IO.Directory.GetFiles(path, "*.csv");
            var c = 0;
            foreach (var file in files)
                Console.WriteLine(("[" + c++ + "]").Expand(files.Count().ToString().Length + 3) + System.IO.Path.GetFileName(file));

            do
            {
                Console.WriteLine("Choose file by entering a 0-indexed integer from 0 to {0}, or 'exit':", (files.Length - 1));

                var selection = -1;
                while (selection == -1)
                {
                    var input = Console.ReadLine();
                    if (input == "exit" || input == "") return;
                    if (!int.TryParse(input, out selection) || selection < 0 || selection > files.Length - 1)
                    {
                        Console.WriteLine("'{0}' is not a valid value.", input);
                        selection = -1;
                    }
                }

                var items = LTTItem.FromFile(files[selection]).ToList();

                var stats = new LTTStatistics(items);

                Console.Write(stats);
                Console.WriteLine("");
            } while (true);
        }
    }
}