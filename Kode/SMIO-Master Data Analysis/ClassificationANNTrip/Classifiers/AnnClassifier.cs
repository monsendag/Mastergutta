﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Neuro;
using AForge.Neuro.Learning;
using ClassificationANNTrip.Controls;
using RealTimeGrapher.Helpers;
using Utilities.AtB.Trips;
using Utilities.Extensions;

namespace ClassificationANNTrip.Classifiers
{
    class AnnClassifier : AClassifier
    {
        private ActivationNetwork _network;

        public AnnClassifier()
        {
            Editor = new SettingsAnn();
        }

        public override double CurrentError { get; protected set; }

        public override bool ReadyForTesting
        {
            get { return _network != null; }
        }

        public override string Summary
        {
            get { return _network == null ? "[Null]" : string.Format("Inputs: {0}, Layers: {1}, Outputlength: {2}", _network.InputsCount, string.Join(",", _network.Layers.Select(p => p.Neurons.Length)), _network.Output.Length); }
        }

        protected override async Task DoTraining()
        {
            Errors = new List<double>();

            var e = Editor as SettingsAnn;
            var neuronCounts = e.NeuronCounts;
            var sigmoidAlphaValue = e.SigmoidAlphaValue;
            var learningErrorLimit = e.LearningErrorLimit;
            var learningRate = e.LearningRate;
            var momentum = e.Momentum;

            await Task.Run(() =>
            {
                Log("Number of input/output pairs: " + Input.Length.ToString("n0"));

                _network = new ActivationNetwork(new SigmoidFunction(sigmoidAlphaValue), Input[0].Length, neuronCounts);
                var teacher = new BackPropagationLearning(_network)
                {
                    LearningRate = learningRate,
                    Momentum = momentum
                };

                cancelOperation = false;
                var sameError = 0;
                while (!cancelOperation)
                {
                    CurrentError = teacher.RunEpoch(Input, Output);

                    if (Errors.Any() && CurrentError == Errors.Last())
                        sameError++;
                    else
                        sameError = 0;

                    Errors.Add(CurrentError);

                    if (CurrentError <= learningErrorLimit)
                    {
                        cancelOperation = true;
                        Log("Error lower than error limit.");
                    }
                    else if (sameError > 500)
                    {
                        cancelOperation = true;
                        Log("No error improvements.");
                    }
                }
                Log("Iterations: " + Errors.Count);
            });

            SetStatus(Statuses.Idle, "");
        }

        protected override async Task<ErrorKeeper> DoTesting()
        {
            var error = await Task.Run(() =>
            {
                var hits = new List<double>();
                for (var i = 0; i < TestInput.Length; i++)
                {
                    var unscaledResult = _network.Compute(TestInput[i])[0];
                    var unscaledTarget = TestOutput[i][0];
                    var result = ScaleFromAnn(unscaledResult);
                    var target = ScaleFromAnn(unscaledTarget);
                    var deviation = result - target;
                    Console.WriteLine("Pred: '{0}', Target: '{1}', ({2} / {3})", result, target, unscaledResult, unscaledTarget);
                    hits.Add(deviation);
                }

                return new ErrorKeeper(hits);
            });

            SetStatus(Statuses.Idle, "");

            return error;
        }

        public override void SetData<T>(IEnumerable<T> trainingSet, IEnumerable<T> testSet)
        {
            if (!(trainingSet is IEnumerable<TripItem> && testSet is IEnumerable<TripItem>))
                throw new Exception("Unsupported data type.");

            if (true)
            {
                var trainingItems = trainingSet as TripItem[] ?? ((IEnumerable<TripItem>) trainingSet).ToArray();
                var testingItems = testSet as TripItem[] ?? ((IEnumerable<TripItem>) testSet).ToArray();

                Input = trainingItems.ToAnnInput();
                Output = trainingItems.ToAnnOutput();
                TestInput = testingItems.ToAnnInput();
                TestOutput = testingItems.ToAnnOutput();
            }
            else //For testing purposes
            {
                var inputData = new double[101][];
                var outputData = new double[101][];
                for (var i = 0; i < inputData.Length; i++)
                {
                    inputData[i] = new double[] {i};
                    outputData[i] = new[] {inputData[i][0]*2};
                }

                Input = inputData.Take(90).ToArray();
                Output = outputData.Take(90).ToArray();
                TestInput = inputData.Skip(90).ToArray();
                TestOutput = outputData.Skip(90).ToArray();
            }

            var x = Output.Select(p => p[0]).Concat(TestOutput.Select(p => p[0])).ToList();
            Max = x.Max();
            Min = x.Min();

            Input = Input.Normalize();
            Output = Output.Normalize();

            TestInput = TestInput.Normalize();
            TestOutput = TestOutput.Normalize();

            //Output = Output.Select(p => new[] {ScaleToAnn(p[0])}).ToArray();
            //TestOutput = TestOutput.Select(p => new[] {ScaleToAnn(p[0])}).ToArray();
        }
    }
}
