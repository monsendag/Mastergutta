﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Utilities.Extensions;
using Utilities.Classifiers;

namespace WekaLearning.PowerSet
{
    public class PowerSetResultSet
    {
        private List<PowerSetResult> _results;
        private IEnumerable<PowerSetResult> _nBestResults;
        private string[] _attrs;
        private PowerSetSettings _settings;

     
        public PowerSetResultSet(string fullResultPath, int nBestMae)
        {
            CalculateResults(fullResultPath,nBestMae);
        }

        public PowerSetResultSet(string sourceFolder, string fileNumber, int nBestMae)
        {
            _settings = InstantiateSettings(sourceFolder);
            if (!HasFile(sourceFolder, fileNumber)) return;

            var filter = "*" + fileNumber + "result" + _settings.Attribute(SettingsAttribute.Classifier) + "*";
            CalculateResults(Directory.GetFiles(sourceFolder,filter)[0],nBestMae);
        }

        public static bool HasFile(string sourceFolder, string fileNumber)
        {
            try
            {

                PowerSetSettings settings = InstantiateSettings(sourceFolder);
                var filter = "*" + fileNumber + "result" + settings.Attribute(SettingsAttribute.Classifier) + "*";
                var files = Directory.GetFiles(sourceFolder, filter);

                return files.Length >= 1;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        private void CalculateResults(string fullResultPath, int nBestMae)
        {
            InstantiateSettings(Path.GetDirectoryName(fullResultPath));

            _attrs = File.ReadLines(fullResultPath).ElementAt(0).Split(',');
            _results = ReadPowerSetResultFromFile(fullResultPath);
            _nBestResults = BestMeanAbsoluteErrorSets(nBestMae);
        }

        private static PowerSetSettings InstantiateSettings(string sourceFolder)
        {
           return new PowerSetSettings(Path.Combine(path1: sourceFolder, path2: "settings.txt"));
        }

        public PowerSetSettings PowerSetSettings()
        {
            return _settings;
        }

        public double BestCorrelationCoefficient()
        {
            return BestCorrelationSets(1).First().CorrelationCoefficient;
        }

        public double BestMeanAbsoluteError()
        {
            return _results.OrderBy(p => p.MeanAbsoluteError).First().MeanAbsoluteError;
        }

        public IEnumerable<PowerSetResult> BestCorrelationSets(int count)
        {
            return OrderByCorrelation().Take(count);
        }

        public IEnumerable<PowerSetResult> BestMeanAbsoluteErrorSets(int count)
        {
            return OrderByMeanAbsoluteError().Take(count);
        }
        
        private IEnumerable<PowerSetResult> OrderByCorrelation()
        {
            return _results.OrderByDescending(p => p.CorrelationCoefficient);
        }
        
        private IEnumerable<PowerSetResult> OrderByMeanAbsoluteError()
        {
            return _results.OrderBy(p => p.MeanAbsoluteError);
        }

        public List<KeyValuePair<string, int>> RemovedCountPerAttribute(IEnumerable<PowerSetResult> set, int nBest )
        {
           Dictionary<String,int> removedCount = new Dictionary<string, int>();
            
           foreach (PowerSetResult result in set)
           {
                foreach (var attribute in result.AttributesRemoved)
                {
                    int currValue;
                    removedCount.TryGetValue(attribute, out currValue);
                    if (currValue == 0)
                        removedCount.Add(attribute, currValue + 1);
                    else
                        removedCount[attribute] += 1;
                }
            }
           
            List<KeyValuePair<string, int>> removedCountList = removedCount.ToList();
            removedCountList.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));

            return removedCountList;
        }

        public IEnumerable<PowerSetResult> BestCorrelationPercentile(int percentile)
        {
            int topCount = (int)(_results.Count * (percentile / (double)100));
            return OrderByCorrelation().Take(topCount);
        }

        private List<PowerSetResult> ReadAllPowerSetFilesInFolder(string[] files, string regex)
        {
            List<PowerSetResult> result = new List<PowerSetResult>();

            foreach (var file in files)
            {
                result.AddRange(ReadPowerSetResultFromFile(file));
            }

            return result;
        }
        
        private List<PowerSetResult> ReadPowerSetResultFromFile(string file)
        {
            return (
                from line in File.ReadAllLines(file)
                select new PowerSetResult(line)).ToList();
        }

        private List<PowerSetResult> ReadPowerSetResultFromDetailedFile(string file)
        {
            //var s = File.ReadAllText(file, Encoding.Default).Split(Environment.NewLine.Repeat(2)).
            //               Where(p => !string.IsNullOrEmpty(p)).
            //               Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray());

            //var runs = s.
            //    Select(c => new
            //    {
            //        Neurons = c[0].Split(", ")[0].Split("Neurons:")[1],
            //        LearningRate = Math.Round(Double.Parse(c[0].Split(", ")[1].Split("LearningRate:")[1].Replace(".", ",")), 1),
            //        Momentum = Math.Round(Double.Parse(c[0].Split(", ")[2].Split("Momentum:")[1].Replace(".", ",")), 1),
            //        Epochs = c[0].Split(", ")[3].Split("Epochs:")[1],
            //        File = String.Join("", c[0].Split("finalDataSet\\")[1].Take(3)),
            //        Training = EvaluationAnalyzer.Find(c, "Training finished in "),
            //        Testing = EvaluationAnalyzer.Find(c, "Testing finished in "),
            //        Correlation = EvaluationAnalyzer.Find(c, "Correlation"),
            //        MAE = (c, "Mean absolute error"),
            //        RMSE = Find(c, "Root mean squared error"),
            //        RAE = Find(c, "Relative absolute error"),
            //        RRSE = Find(c, "Root relative squared error"),
            //        NumInstances = Find(c, "Total Number of Instances")
            //    });
            return null;
        } 


        /// <summary>
        /// Note: Dette var metoden som fungerte veldig dårlig. Gjennomsnittlig feil blir omtrent lik for alle sett.
        /// </summary>
        /// <param name="set"></param>
        /// <returns></returns>
        private IEnumerable<Tuple<string, double>> MeanAbsoluteErrorPerAttribute(IEnumerable<PowerSetResult> set)
        {
            var x = _attrs.Select(attr => new
            {
                Attribute = attr,
                Set = set.Where(c => !c.AttributesRemoved.Contains(attr))
            }).
                Select(p => new
                {
                    p.Attribute,
                    p.Set,
                    Average = p.Set.Select(c => c.MeanAbsoluteError).Average()
                });

            return null;
        }

        /// <summary>
        /// From the top N results, the worst attributes are fetched, sorted by
        /// the number of times they are removed.
        /// </summary>
        /// <param name="numOfWorstAttributes"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, int>> WorstAttributes(int numOfWorstAttributes)
        {
            var arrs = _nBestResults.Select(result => result.AttributesRemoved);
            
            Dictionary<string, int> countPerAttribute = new Dictionary<string, int>();


            foreach (var val in arrs.SelectMany(arr => arr))
            {
                int currValue;
                countPerAttribute.TryGetValue(val, out currValue);
                if (currValue == 0)
                    countPerAttribute.Add(val, currValue + 1);
                else
                    countPerAttribute[val] += 1;
            }

            return countPerAttribute.ToList().OrderByDescending(p => p.Value).Take(numOfWorstAttributes);
        } 


    }
}
