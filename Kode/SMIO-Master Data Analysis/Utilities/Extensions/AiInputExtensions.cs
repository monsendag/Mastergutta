﻿using System;
using System.Globalization;
using System.Linq;
using Utilities.AtB.Interfaces;

namespace Utilities.Extensions
{
    public static class AiInputExtensions
    {
        public static string ToSummary(this IAiInput input)
        {
            string s = String.Join(",",
                input.ToAnnData().Select(p => p.ToString("F", CultureInfo.CreateSpecificCulture("en-US"))));

            return s;
        }


    }
}
