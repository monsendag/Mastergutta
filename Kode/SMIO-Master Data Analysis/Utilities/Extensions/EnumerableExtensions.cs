﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Extensions
{
    public static class EnumerableExtensions
    {
        public static double[] ToDoubles(this IEnumerable<object> objects)
        {
            var arr = objects.ToArray();

            var input = new double[arr.Length];
            for (var i = 0; i < arr.Length; i++)
            {
                double d;
                var o = arr[i];

                if (o is int)
                    d = (double)(int)o;
                else if (o is double)
                    d = (double)o;
                else if (o is decimal)
                    d = (double)(decimal)o;
                else if (o is float)
                    d = (double)(float)o;
                else if (o is bool)
                    d = (bool)o ? 1 : 0;
                else if (o is string)
                    d = o.GetHashCode();
                else if (o is DateTime)
                    d = ((DateTime) o).Ticks;
                else
                    throw new Exception("Unsupported input type: " + i + ", " + o.GetType());

                input[i] = d;
            }

            return input;
        }
        
    }
}
