﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.AtB.LinksTravelTime;
using Utilities.AtB.Passage;
using Utilities.AtB.RealTime;
using Utilities.AtB.Tickets;
using Utilities.AtB.Trips;
using Utilities.Yr;

namespace Utilities.Extensions
{
    public static class DBExtensions
    {
        public static IQueryable<RealTimeItem> Between(this IQueryable<RealTimeItem> items, DateTime start, DateTime end)
        {
            return items.Where(p => p.QueryTime >= start && p.QueryTime <= end);
        }

        public static IQueryable<TripItem> Between(this IQueryable<TripItem> items, DateTime start, DateTime end)
        {
            return items.Where(p => p.ScheduledDeparture >= start && p.ScheduledDeparture <= end);
        }

        public static IQueryable<LTTItem> Between(this IQueryable<LTTItem> items, DateTime start, DateTime end)
        {
            return items.Where(p => p.ActualDeparture >= start && p.ActualDeparture <= end);
        }

        public static IQueryable<PassageItem> Between(this IQueryable<PassageItem> items , DateTime start, DateTime end)
        {
            return items.Where(p => p.ScheduledArrival >= start && p.ScheduledArrival <= end);
        }

        public static WeatherObject FromHour(this IQueryable<WeatherObject> items, DateTime dt)
        {
            dt = dt.AddMinutes(-dt.Minute).AddSeconds(-dt.Second).AddMilliseconds(-dt.Millisecond);
            return items.FirstOrDefault(p => p.Time == dt);
        }

        public static int SoldLastPeriod(this IQueryable<Ticket> items, string line, string stopCode, DateTime dt, int minutes)
        {
            var min = dt.AddMinutes(-minutes);
            var linei = int.Parse(line);
            return items.Count(p => p.UsageType == Ticket.UsageTypes.PeriodUsage && p.EventDateTime >= min && p.EventDateTime <= dt && p.LineNr == linei && p.EntryStop != null && p.EntryStop.ID == stopCode);
        }

        public static IQueryable<CellPhoneTicket> FromDay(this IQueryable<CellPhoneTicket> items, DateTime dt)
        {
            return items.Where(p => p.PurchaseDate == dt);
        }

        public static double[][] ToAnnInput(this IEnumerable<TripItem> tripItems)
        {
            return tripItems.Select(p => p.ToAnnInput()).ToArray();
        }

        public static double[][] ToAnnOutput(this IEnumerable<TripItem> tripItems)
        {
            return tripItems.Select(p => p.ToAnnOutput()).ToArray();
        }
    }
}
