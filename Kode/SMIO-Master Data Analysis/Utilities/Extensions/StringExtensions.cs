﻿using System;
using System.Linq;

namespace Utilities.Extensions
{
    public static class StringExtensions
    {
        public static string[] Split(this string body, string separator)
        {
            return body.Replace(separator, "µ").Split('µ');
        }

        public static string Expand(this string s, int length)
        {
            while (s.Length < length)
                s += " ";
            return s;
        }

        public static string Repeat(this string s, int times)
        {
            return string.Join("", Enumerable.Range(0, times).Select(p => s));
        }
    }
}