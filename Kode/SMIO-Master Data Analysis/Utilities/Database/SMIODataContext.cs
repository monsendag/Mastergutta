﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB;
using Utilities.AtB.FullTrips;
using Utilities.AtB.GPSLog;
using Utilities.AtB.General;
using Utilities.AtB.Generated;
using Utilities.AtB.Lines;
using Utilities.AtB.LinksTravelTime;
using Utilities.AtB.Passage;
using Utilities.AtB.PassageAtVirtualLoops;
using Utilities.AtB.PassengerCount;
using Utilities.AtB.RealTime;
using Utilities.AtB.Tickets;
using Utilities.AtB.Trips;
using Utilities.Yr;
using Utilities.Soccer;

namespace Utilities.Database
{
    public class SMIODataContext : DbContext
    {
        public DbSet<LTTItem> LTTItems { get; set; }
        public DbSet<PassageItem> PassageItems { get; set; }
        public DbSet<WeatherObject> WeatherObjects { get; set; }
        public DbSet<BusStop> BusStops { get; set; }
        public DbSet<SoccerGame> SoccerGames { get; set; }
        public DbSet<GPSLogItem> GPSLogItems { get; set; }
        public DbSet<FullTripItem> FullTripItems { get; set; }
        public DbSet<ImportHistoryItem> ImportHistory { get; set; }
        public DbSet<PassageAtVirtualLoopsItem> PassagesAtVirtualLoops { get; set; }
        public DbSet<PassengerCountItem> PassengerCounts { get; set; }
        public DbSet<RealTimeItem> RealTimeItems { get; set; }
        public DbSet<Ticket> TicketData { get; set; }
        public DbSet<CellPhoneTicket> CellPhoneTicketData { get; set; }
        public DbSet<SingleTicket> SingleTickets { get; set; }
        public DbSet<Line> Lines { get; set; }
        public DbSet<TripItem> Trips { get; set; }
        public DbSet<BusStopPassage> BusStopPassages { get; set; }

        public SMIODataContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 600;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BusStop>().Property(x => x.X).HasPrecision(18, 8);
            modelBuilder.Entity<BusStop>().Property(x => x.Y).HasPrecision(18, 8);
        }
    }
}
