﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Database
{
    public abstract class DbLoader
    {
        protected static decimal ParseDecimal(string p)
        {
            if (string.IsNullOrEmpty(p)) return -1;
            return decimal.Parse(p.Replace(".", ","));
        }

        protected static int ParseInt(string s)
        {
            if (string.IsNullOrEmpty(s)) return 0;
            return int.Parse(s);
        }

        public static DateTime ParseDateTime(string s, string format)
        {
            if (string.IsNullOrEmpty(s)) return DateTime.MinValue;
            return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
        }

        protected static TimeSpan ParseTimeSpan(string s, string format)
        {
            if (string.IsNullOrEmpty(s)) return TimeSpan.Zero;
            if (s.Length >= 8 && format == "mm:ss")
                format = "HH:mm:ss";
            int modifier = 1;
            if (s.StartsWith("-"))
            {
                modifier = -1;
                s = s.Substring(1);
            }
            return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture).TimeOfDay;
        }

        protected static string Sql(DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        protected static string Sql(Decimal d)
        {
            return d.ToString().Replace(",", ".");
        }

        protected static string Sql(double d)
        {
            return d.ToString().Replace(",", ".");
        }

        protected static string Sql(float d)
        {
            return d.ToString().Replace(",", ".");
        }

        protected static string Sql(bool d)
        {
            return d ? "1" : "0";
        }

        protected static int ParseSeconds(string time, string inputFormat)
        {
            try
            {
                var s = time;
                var format = inputFormat;
                if (string.IsNullOrEmpty(s)) return 0;
                if (s.Length >= 8 && format == "mm:ss")
                    format = "HH:mm:ss";
                int modifier = 1;
                if (s.StartsWith("-"))
                {
                    modifier = -1;
                    s = s.Substring(1);
                }
                return modifier * (int)DateTime.ParseExact(s, format, CultureInfo.InvariantCulture).TimeOfDay.TotalMinutes;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to parse seconds from timespan stuff: '{0}', '{1}'", time, inputFormat), ex);
            }
        }

        protected static int ParseSeconds(string time)
        {
            try
            {
                time = time.Trim();
                if (string.IsNullOrEmpty(time)) return 0;
                int modifier = 1;
                if (time.StartsWith("-"))
                {
                    modifier = -1;
                    time = time.Substring(1);
                }
                var parts = time.Split(':');
                var i = parts.Length - 1;
                var secMod = 1;
                var secs = 0;
                while (i >= 0)
                {
                    secs += int.Parse(parts[i--])*(secMod);
                    secMod *= 60;
                }
                return modifier * secs;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to parse seconds from timespan stuff: '{0}'", time), ex);
            }
        }

        private static readonly Dictionary<string, Dictionary<string, int>> _buckets = new Dictionary<string, Dictionary<string, int>>();

        public static int Bucketize(string id, string text)
        {
            if (!_buckets.ContainsKey(id))
                _buckets.Add(id, new Dictionary<string, int>());
            if (!_buckets[id].ContainsKey(text))
                _buckets[id].Add(text, _buckets[id].Keys.Count);
            return _buckets[id][text];
        }
    }
}
