﻿using System;
using System.Linq;
using Utilities.Classifiers;
using Utilities.Extensions;

namespace Utilities.PowerSet
{
    public class PowerSetResult
    {
        enum ContentType
        {
            AttributesRemoved,
            MeanAbsoluteError,
            CorrelationCoefficient,
            RelativeAbsoluteError,
            RootMeanSquaredError,
            RootRelativeSquaredError
        }
        public String[] AttributesRemoved  { get; set; }
        public double MeanAbsoluteError { get; set; }
        public double Correlation { get; set; }
        public double RelativeAbsoluteError { get; set; }
        public double RootMeanSquaredError { get; set; }
        public double RootRelativeSquaredError { get; set; }
        public string LearnerType { get; set; }
        public string DataSet { get; set; }
        

        public override string ToString()
        {
            return
                String.Format(
                    "Removed: {0}, MAE: {1}, Corr: {2}, RAE: {3}, RMSE: {4} RRSE: {5}",
                    String.Join(",", AttributesRemoved), MeanAbsoluteError, Correlation, 
                        RelativeAbsoluteError, RootMeanSquaredError, RootRelativeSquaredError);
        }

        public enum ResultType
        {
            Aleksander,
            Erlend
        };

        public PowerSetResult(string dataString, ResultType type, string learnerType = "", string dataSet = "-1")
        {
            DataSet = dataSet;
            LearnerType = learnerType;
            
            if(type == ResultType.Aleksander){
                var dataArr = dataString.Replace(" ", "").Split(';');
         
                var attrsRemoved = dataArr[(int) ContentType.AttributesRemoved];
                AttributesRemoved = attrsRemoved.Substring(1, attrsRemoved.Length - 2).Split(',');

                Correlation = Convert.ToDouble(dataArr[(int) ContentType.CorrelationCoefficient].Replace(".", ","));
                MeanAbsoluteError = Convert.ToDouble(dataArr[(int) ContentType.MeanAbsoluteError].Replace(".", ","));
                RelativeAbsoluteError = Convert.ToDouble(dataArr[(int) ContentType.RelativeAbsoluteError].Replace(".", ","));
                RootMeanSquaredError = Convert.ToDouble(dataArr[(int) ContentType.RootMeanSquaredError].Replace(".", ","));
                RootRelativeSquaredError = Convert.ToDouble(dataArr[(int) ContentType.RootRelativeSquaredError].Replace(".", ","));
            
            }

            if (type == ResultType.Erlend)
            {
                var data = dataString.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToList();

                AttributesRemoved = data[2].Split(';');
                Correlation = EvaluationAnalyzer.Find(data, "Correlation");
                MeanAbsoluteError = EvaluationAnalyzer.Find(data, "Mean absolute error");
                RootMeanSquaredError = EvaluationAnalyzer.Find(data, "Root mean squared error");
                RelativeAbsoluteError = EvaluationAnalyzer.Find(data, "Relative absolute error");
                RootRelativeSquaredError = EvaluationAnalyzer.Find(data, "Root relative squared error"); 
            }

        }
    }
}
