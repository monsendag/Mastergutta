﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Utilities.AtB.Utilities;

namespace Utilities.PowerSet
{
    public enum MiniPowType
    {
        AllAttributes,
        BaseAttributes,
        Temperature,
        Wind,
        Humidity,
        Precipitation,
        AllWeather,
        SoccerGame,
        PassengersPresent,
        TicketsSoldLastHour,
        TicketsSoldLastThreeHours
    }

    public class MiniPowerSet
    {
        private readonly string _sourceFolder;
        private readonly string _filenumber;

        private readonly string _descriptionFile;
        private readonly string _powersetResultFile;


        public MiniPowerSet(string sourceFolder, string filenumber, string classifier)
        {
            _sourceFolder = sourceFolder;
            _filenumber = filenumber;

            _descriptionFile = Data.GetWekaFile("attributes","descriptions","description.txt");
            _powersetResultFile = Data.GetWekaFile(_sourceFolder, _filenumber + "result" + classifier + ".txt");
        }

        public Dictionary<String, PowerSetResult> Results()
        {
            Dictionary<String, PowerSetResult> results = new Dictionary<string, PowerSetResult>();

            var descrLines = File.ReadAllLines(_descriptionFile);
            var pwrLines = File.ReadAllLines(_powersetResultFile);

            if (descrLines.Count() != pwrLines.Count())
            {
                throw new ApplicationException("Description lines are not matching up with power set lines.");
            }

            for(int i = 0; i < descrLines.Count(); i++)
            {
                results.Add(descrLines[i],
                    new PowerSetResult(pwrLines[i], PowerSetResult.ResultType.Aleksander, "", _filenumber));
            }

            return results;
        }
    }
}
