﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Database;

namespace Utilities.AtB.Interfaces
{
    public interface IAiInput
    {
        double[] ToAnnData();
        string Header();

    }
}
