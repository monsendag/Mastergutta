﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.AtB.LinksTravelTime
{
    public class LTTStatistics
    {
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public int ItemCount { get; set; }
        public int DifferentTrips { get; set; }
        public int DifferentBusStops { get; set; }
        
        public LTTStatistics(List<LTTItem> items)
        {
            MinDate = items.Min(p => p.ActualDeparture);
            MaxDate = items.Max(p => p.ActualDeparture);
            ItemCount = items.Count;
            DifferentTrips = items.Select(p => p.Trip).Distinct().Count();
            DifferentBusStops = items.Select(p => p.BusStopFrom.ID).Distinct().Count();
        }

        public static LTTStatistics FromFile(string file)
        {
            var items = LTTItem.FromFile(file);
            return new LTTStatistics(items.ToList());
        }

        public static LTTStatistics LoadPeriod(DateTime start, DateTime end)
        {
            var items = LTTItem.LoadPeriod(start, end);
            return new LTTStatistics(items.ToList());
        }

        public override string ToString()
        {
            return string.Format("Minimum date: {1}{0}Maximum date: {2}{0}Items: {3}{0}Different trips: {4}{0}Different bus stops: {5}", Environment.NewLine,
                                 MinDate, MaxDate, ItemCount, DifferentTrips, DifferentBusStops);
        }
    }
}