﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using Utilities.AtB.Lines;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.Extensions;

namespace Utilities.AtB.General
{
    public class BusStop
    {
        [Key]
        public string ID { get; set; }
        public string Name { get; set; }
        public string RealTimeID { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }

        public virtual ICollection<Line> Lines { get; set; }

        public BusStop()
        {
            
        }
        
        public BusStop(string id, string name)
        {
            ID = id;
            Name = name;
        }

        /// <summary>
        /// Reads a bus stop from the format "X Name (X)" or "Name (X)", where X represents the 8-digit ID, and returns a BusStop object with Name and ID.
        /// </summary>
        /// <param name="csvValue"></param>
        /// <returns></returns>
        public static BusStop FromCSVFormat(string csvValue)
        {
            if (string.IsNullOrEmpty(csvValue)) return null;
            if (csvValue.Substring(0, 8).All(Char.IsDigit))
                return new BusStop(csvValue.Substring(0, 8), csvValue.Substring(8).Split('(')[0].Trim());
            else
            {
                var parts = csvValue.Split('(');
                return new BusStop(parts[1].Substring(0, 8), parts[0].Trim());
            }
        }

        public override string ToString()
        {
            return Name + " (" + ID + ")";
        }

        /// <summary>
        /// Inserts the allbuses.sql file into the BusStop table.
        /// </summary>
        public static void ResetBusStops()
        {
            Console.WriteLine("Inserting bus stops ...");
            var file = Data.GetSqlFile(Data.DataFolders.Buses, "allbuses.sql");
            if (!System.IO.File.Exists(file))
            {
                Console.WriteLine("  > No buses.");
                return;
            }

            DbHelper.RunSQLFile(file, false);

            Console.WriteLine("  > Done.");
        }

        /// <summary>
        /// Inserts the lines.sql file into the Line tables.
        /// </summary>
        public static void ResetLines()
        {
            Console.WriteLine("Inserting lines ...");
            var file = Data.GetSqlFile(Data.DataFolders.Buses, "lines.sql");
            if (!System.IO.File.Exists(file))
            {
                Console.WriteLine("  > No buses.");
                return;
            }

            DbHelper.RunSQLFile(file, false);

            Console.WriteLine("  > Done.");
        }

        public static int GetNumberOfBusStopsInFile()
        {
            return System.IO.File.ReadAllLines(Data.GetSqlFile(Data.DataFolders.Buses, "allbuses.sql"), Encoding.Default).Count(p=>p.StartsWith("INSERT"));
        }

        private static Dictionary<string, string> _scandinavianLetters = new Dictionary<string, string>()
                {
                    {"Ã¸", "ø"},
                    {"Ã¥", "å"},
                    {"Ã˜", "Ø"},
                    {"Ã…", "Å"},
                    {"Ã¦", "æ"}
                };

        /// <summary>
        /// Scans the BusStop table and fixes corrupted scandinavian letters and combinations.
        /// </summary>
        public static void HealNames()
        {
            Console.WriteLine("Healing bus stops ...");

            Console.WriteLine("  > Healing Scandinavian letters ...");

            var db = new SMIODataContext();
            foreach (var letter in _scandinavianLetters)
                db.BusStops.Where(p => p.Name.Contains(letter.Key)).ToList().ForEach(p => p.Name = p.Name.Replace(letter.Key, letter.Value));
            db.SaveChanges();

            Console.WriteLine("  > Healing combinations ...");

            foreach (var stop in db.BusStops.Where(p => p.Name.Contains("|")))
            {
                var parts = stop.Name.Split('|').Select(p => p.Trim()).ToArray();
                for (var i = 0; i < parts.Length; i++)
                {
                    if (parts[i].StartsWith("-"))
                        parts[i] = parts[i].Substring(1).Trim();
                }
                var fullName = parts.LastOrDefault(p => p.Contains("(") && p.Contains(")"));
                if (fullName != null)
                {
                    var without = fullName.Split('(')[0].Trim();
                    parts = parts.Where(p => p == fullName || !(p.ToLower().Replace(".", "").StartsWith(without.ToLower().Replace(".", "")) || without.ToLower().Replace(".", "").StartsWith(p.ToLower().Replace(".", "")) || fullName.ToLower().Replace(".", "").Contains(p.ToLower().Replace(".", "")))).ToArray();
                }
                var newName = string.Join(" | ", parts.Where(p => !string.IsNullOrEmpty(p)).Distinct());
                if (newName == stop.Name) continue;
                Console.WriteLine("      > Renaming: '{0}' to '{1}'.", stop.Name, newName);
                stop.Name = newName;
            }
            db.SaveChanges();

            Console.WriteLine("  > Done.");
        }

        public static string HealScandinavianLetters(string text)
        {
            foreach (var letter in _scandinavianLetters)
                text = text.Replace(letter.Key, letter.Value);
            return text;
        }

        /// <summary>
        /// Scans a BusStop INSERT file and changes it to append to the current database table without making duplicates.
        /// </summary>
        /// <param name="folder"></param>
        public static void StripExistingBusStops(Data.DataFolders folder)
        {
            Console.WriteLine("Analyzing bus stops ...");
            var path = Data.GetDataFolder(folder);
            var file = System.IO.Path.Combine(path, "buses.sql");

            var result = new List<string>();
            var stops = System.IO.File.ReadAllLines(file).Select(p => p.Split('\'')).Select(p => new { ID = p[1], Name = p[3] }).ToList();

            var db = new SMIODataContext();

            int total = stops.Count;
            int current = 0;

            foreach (var stop in stops)
            {
                current++;
                Console.Write("\r  > {0:n2}%", (decimal)current / total * 100m);
                var exStop = db.BusStops.SingleOrDefault(p => p.ID == stop.ID);
                if (exStop != null)
                {
                    var name = exStop.Name.Replace("Ã¸", "ø").Replace("Ã¥", "å").Replace("Ã˜", "Ø").Replace("Ã…", "Å").Replace("Ã¦", "æ");
                    if (name == stop.Name)
                        continue;
                    var names = name.Split(" | ").ToList();
                    names.AddRange(stop.Name.Split(" | "));
                    var newName = string.Join(" | ", names.Distinct());
                    result.Add(string.Format("UPDATE [Utilities.Database.SMIODataContext].[dbo].[BusStops] SET Name='{1}' WHERE ID='{0}';", stop.ID, newName));
                }
                else
                {
                    result.Add(string.Format("INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[BusStops]([ID],[Name]) VALUES('{0}','{1}');", stop.ID, stop.Name));
                }
            }
            Console.Write(Environment.NewLine + "  > Done.");

            System.IO.File.WriteAllLines(System.IO.Path.Combine(path, "buses_fixed.sql"), result, Encoding.Default);
        }

        /// <summary>
        /// Runs through a collection of bus stops and inserts missing stops into the database. (Also appends new names to existing stops.)
        /// </summary>
        /// <param name="allStops"></param>
        public static void Append(IEnumerable<BusStop> allStops)
        {
            Console.WriteLine("      > Analyzing bus stops ...");

            var db = new SMIODataContext();

            allStops.Where(p => p.Name == null).ToList().ForEach(p => p.Name = "");
            var stops = allStops.GroupBy(p => p.ID).Select(p => new BusStop(p.Key, string.Join(" | ", p.SelectMany(c => c.Name.Split('|').Select(d => d.Trim())).ToArray().Distinct()))).ToList();
            int total = stops.Count;
            int current = 0;

            Func<string, string> heal = s => s.Replace("Ã¸", "ø").Replace("Ã¥", "å").Replace("Ã˜", "Ø").Replace("Ã…", "Å").Replace("Ã¦", "æ");

            foreach (var stop in stops)
            {
                current++;
                Console.Write("\r          > {0:n2}%", (decimal)current / total * 100m);
                var exStop = db.BusStops.SingleOrDefault(p => p.ID == stop.ID);
                if (exStop != null)
                {
                    var name = heal(exStop.Name);
                    stop.Name = heal(stop.Name);
                    if (name == stop.Name)
                        continue;
                    var names = name.Split(" | ").ToList();
                    names.AddRange(stop.Name.Split(" | "));
                    var newName = string.Join(" | ", names.Distinct());
                    if(newName!=exStop.Name)
                        exStop.Name = newName;
                }
                else
                {
                    stop.Name = heal(stop.Name);
                    db.BusStops.Add(stop);
                    db.SaveChanges();
                }
            }
            db.SaveChanges();
            Console.Write(Environment.NewLine + "          > Done." + Environment.NewLine);
        }

        public static void CheckStops(SMIODataContext db)
        {
            var dbCount = DbHelper.GetDBCount(Data.DataFolders.Buses, db);
            var fileCount = GetNumberOfBusStopsInFile();
            if (dbCount != fileCount || !db.BusStops.Any(p => !string.IsNullOrEmpty(p.RealTimeID)))
                ResetBusStops();
            if (!db.Lines.Any())
                ResetLines();
        }
        
        /// <summary>
        /// Return a list of bus stops at the current line. Fetches data directly from AtB.
        /// </summary>
        /// <returns></returns>
        public static List<BusStop> FetchBusStopsFromAtB(int line)
        {
            var html = new WebClient().DownloadString("https://www.atb.no/holdeplassoversikt-2014/category1254.html");
            html = Regex.Split(html, "<table")[1];
            html = "<table" + Regex.Split(html, "</table>")[0].Replace("&", "") + "</table>";

            var xml = XElement.Parse(html);
            return xml.Element("tbody").Elements("tr").Skip(1).Where(p => p.Elements("td").ElementAt(3).Value != "\n\t\t\t\tnbsp;").Select(p => p.Elements("td").ToArray()).Select(p => new
                {
                    Name = HealScandinavianLetters(p[1].Value.Replace("nbsp;", " ").Trim()),
                    Lines = p[2].Value.Trim().Split(',').Select(i => int.Parse(i.Trim())).ToList(),
                    Code = p[3].Value.Trim()
                }).
                       Where(p => p.Lines.Contains(line)).
                       Select(p => new BusStop(p.Code, p.Name)).ToList();
        }

        public static void FetchIDsAndLocations(bool forceNames = false, bool forceIDs = false)
        {
            Console.WriteLine("Checking stops ...");
            var db = new SMIODataContext();
            using (var client = new WebClient())
            {
                var count = 0;
                var countID = 0;
                var countLocation = 0;
                var stops = db.BusStops.ToList();
                var dbCount = stops.Count;
                foreach (var stop in stops)
                {
                    if (string.IsNullOrEmpty(stop.RealTimeID) || forceIDs)
                    {
                        var res = GetRealTimeID(client, stop.ID);
                        stop.RealTimeID = res;
                        if (res != "-1")
                            countID++;
                    }

                    if ((stop.X == 0 && stop.Y == 0) || forceNames || string.IsNullOrEmpty(stop.Name))
                    {
                        var res = client.DownloadString("https://rp.atb.no/scripts/TravelMagic/TravelMagicWE.dll/maplocationxml?id=" + Uri.EscapeDataString(stop.ID));
                        if (res != "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><address/>")
                        {
                            var parts = res.Split('\"');
                            stop.X = decimal.Parse(parts[15]);
                            stop.Y = decimal.Parse(parts[17]);
                            var name = HealScandinavianLetters(parts[11]);
                            if(!stop.Name.Contains(name))
                                stop.Name += " | " + name;
                            countLocation++;
                        }
                    }

                    if (count%20 == 0)
                        db.SaveChanges();

                    Console.Write("\r  > {0} / {1}, ID={2}, Loc={3}", count++, dbCount, countID, countLocation);
                }
                db.SaveChanges();
                Console.Write(Environment.NewLine + "  > Done." + Environment.NewLine);
            }
        }

        public static string GetRealTimeID(WebClient client, string id)
        {
            var res = client.DownloadString("https://www.atb.no/xmlhttprequest.php?service=api.search.realtime&q=" + Uri.EscapeDataString(id));
            return res == "[]" ? "-1" : res.Split(':')[1].Split(',')[0];
        }

        public static void GenerateBusFile()
        {
            System.IO.File.WriteAllLines(
                Data.GetDataFile(Data.DataFolders.Buses, "busfile.csv"),
                new SMIODataContext().BusStops.ToList().Select(p =>
                                                      string.Join(";", new[]
                                                          {
                                                              p.ID,
                                                              p.Name,
                                                              p.RealTimeID,
                                                              p.X.ToString().Replace(",", "."),
                                                              p.Y.ToString().Replace(",", ".")
                                                          })), Encoding.Default);
        }

        public static void ImportBusFile()
        {
            var db = new SMIODataContext();
            var updated = 0;
            var inserted = 0;
            foreach (var p in System.IO.File.ReadAllLines(Data.GetDataFile(Data.DataFolders.Buses, "busfile.csv"), Encoding.Default).Select(p => p.Split(';')))
            {
                var id = p[0];
                decimal x;
                decimal y;
                if (!decimal.TryParse(p[3], out x))
                    x = decimal.Parse(p[3].Replace(".", ","));
                if (!decimal.TryParse(p[4], out y))
                    y = decimal.Parse(p[4].Replace(".", ","));

                var stop = db.BusStops.FirstOrDefault(b => b.ID == id);

                if (stop != null)
                {
                    var changed = false;
                    if (stop.Name != p[1])
                    {
                        stop.Name = p[1];
                        changed = true;
                    }
                    if (stop.RealTimeID != p[2])
                    {
                        stop.RealTimeID = p[2];
                        changed = true;
                    }
                    if (stop.X != x)
                    {
                        stop.X = x;
                        changed = true;
                    }
                    if (stop.Y != y)
                    {
                        stop.Y = y;
                        changed = true;
                    }

                    if (changed)
                        updated++;
                }
                else
                {
                    db.BusStops.Add(new BusStop()
                        {
                            ID = id,
                            Name = p[1],
                            RealTimeID = p[2],
                            X = x,
                            Y = y
                        });
                    inserted++;
                }
            }
            db.SaveChanges();

            Console.WriteLine("Updated {0} items, inserted {1}.", updated, inserted);
        }

        public static void CheckRIDs(bool andUpdateThemInTheDatabaseToo)
        {
            Console.WriteLine("Checking stops ...");
            using (var client = new WebClient())
            {
                var db = new SMIODataContext();
                var stops = db.BusStops.ToList();
                var count = 0;
                var log = new StringBuilder();
                foreach (var stop in stops)
                {
                    var res = GetRealTimeID(client, stop.ID);
                    if (res != stop.RealTimeID && res != "-1")
                    {
                        Console.WriteLine("  > " + stop.Name);
                        Console.WriteLine("      > " + stop.RealTimeID + "  =>  " + res);
                        log.AppendLine(stop.RealTimeID + ";" + res);

                        if (andUpdateThemInTheDatabaseToo)
                        {
                            stop.RealTimeID = res;
                            count++;
                            if (count%20 == 0)
                                db.SaveChanges();
                        }
                    }
                }

                if (andUpdateThemInTheDatabaseToo)
                    db.SaveChanges();

                System.IO.File.WriteAllText(Data.GetDataFile(Data.DataFolders.Buses, "tempstopids"), log.ToString(), Encoding.Default);

                Console.WriteLine("  > Done.");
            }
        }
    }
}
