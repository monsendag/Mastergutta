﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Utilities.AtB.Utilities;

namespace Utilities.AtB.Passage
{
    public class PassageStatistics
    {
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public int ItemCount { get; set; }
        public int DifferentTrips { get; set; }
        public int DifferentBusStops { get; set; }
        public double AverageDelay { get; set; }
        public double MaxDelay { get; set; }
        public double MinDelay { get; set; }
        public double AverageStopTime { get; set; }
        public double MaxStopTime { get; set; }
        public double MinStopTime { get; set; }
        
        public PassageStatistics(List<PassageItem> items)
        {
            MinDate = items.Min(p => p.ScheduledArrival);
            MaxDate = items.Max(p => p.ScheduledArrival);
            ItemCount = items.Count;
            DifferentTrips = items.Select(p => p.Trip).Distinct().Count();
            DifferentBusStops = items.Select(p => p.BusStop.ID).Distinct().Count();
            AverageDelay = items.Average(p => p.Delay);
            MaxDelay = items.Max(p => p.Delay);
            MinDelay = items.Min(p => p.Delay);
            AverageStopTime = items.Average(p => p.StopTime);
            MaxStopTime = items.Max(p => p.StopTime);
            MinStopTime = items.Min(p => p.StopTime);
        }

        /// <summary>
        /// Loads data for the given period.
        /// </summary>
        /// <returns></returns>
        public static PassageStatistics LoadPeriod(DateTime start, DateTime end)
        {
            var allItems = PassageItem.LoadPeriod(start, end);
            return new PassageStatistics(allItems);
        }

        public static PassageStatistics FromFile(string file)
        {
            var items = PassageItem.FromFile(file);
            return new PassageStatistics(items.ToList());
        }

        public override string ToString()
        {
            return string.Format("Minimum date: {1}{0}Maximum date: {2}{0}Items: {3}{0}Different trips: {4}{0}Different bus stops: {5}{0}Average delay: {6} ({8} - {9}){0}Average stop time: {7} ({10} - {11})", Environment.NewLine,
                                 MinDate, MaxDate, ItemCount, DifferentTrips, DifferentBusStops, AverageDelay, AverageStopTime, MinDelay, MaxDelay, MinStopTime, MaxStopTime);
        }
    }
}