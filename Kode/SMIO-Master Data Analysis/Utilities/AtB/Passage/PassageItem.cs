﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;
using System.Linq;
using Utilities.AtB.General;
using Utilities.AtB.Interfaces;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.Extensions;
using Utilities.Yr;

namespace Utilities.AtB.Passage
{
    public class PassageItem : DbLoader, IAiInput
    {
        [Key]
        public int ID { get; set; }
        public string Line { get; set; } 
        public string Shift { get; set; } 
        public string Trip { get; set; }
        public string Vehicle { get; set; } 
        public string BusStopCode { get; set; } 
        public virtual BusStop BusStop { get; set; }
        public string Type { get; set; } 
        public DateTime ActualArrival { get; set; }
        public DateTime ScheduledArrival { get; set; } 
        public int StopTime { get; set; }
        public int Delay { get; set; } 
        public int ActualHeadWay { get; set; } 
        public int ScheduledHeadWay { get; set; } 

        public static IEnumerable<PassageItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).
                          Where(p => Char.IsDigit(p[0])).
                          Select(p => p.Split(';')).
                          Select(p => new PassageItem()
                              {
                                  Line = p[0],
                                  Shift = p[1],
                                  Trip = p[2],
                                  Vehicle = p[3],
                                  BusStopCode = p[4],
                                  BusStop = BusStop.FromCSVFormat(p[5]),
                                  Type = p[6],
                                  ActualArrival = ParseDateTime(p[7], "dd'/'MM'/'yyyy' 'HH':'mm':'ss"),
                                  ScheduledArrival = ParseDateTime(p[8], "dd'/'MM'/'yyyy' 'HH':'mm':'ss"),
                                  StopTime = int.Parse(p[9]),
                                  Delay = int.Parse(p[10]),
                                  ActualHeadWay = int.Parse(p[11]),
                                  ScheduledHeadWay = int.Parse(p[12]),
                              });
        }

        public static List<PassageItem> LoadPeriod(DateTime start, DateTime end)
        {
            var files = Data.GetDataFiles(Data.DataFolders.PassageAtBusStops, start, end);

            if (files.Count == 0) throw new Exception("No data.");

            var allItems = new List<PassageItem>();
            allItems.AddRange(FromFile(files.First()).Where(p => p.ActualArrival >= start));
            if (files.Count == 1)
                allItems = allItems.Where(p => p.ActualArrival <= end).ToList();
            else
                for (var i = 0; i < files.Count; i++)
                {
                    if (i == files.Count - 1)
                        allItems.AddRange(FromFile(files[i]).Where(p => p.ActualArrival <= end));
                    else
                        allItems.AddRange(FromFile(files[i]));
                }

            return allItems;
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.PassageAtBusStops);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.PassageAtBusStops, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();
                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[PassageItems]([Line],[Shift],[Trip],[Vehicle],[BusStopCode],[Type],[ActualArrival],[ScheduledArrival],[StopTime],[Delay],[ActualHeadWay],[ScheduledHeadWay],[BusStop_ID]) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},{9},{10},{11},{12});",
                    p.Line, p.Shift, p.Trip, p.Vehicle, p.BusStopCode, p.Type, p.ActualArrival.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.ScheduledArrival.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.StopTime, p.Delay, p.ActualHeadWay, p.ScheduledHeadWay, p.BusStop.ID)), Encoding.Default);

                BusStop.Append(items.Select(p => p.BusStop));
            }
        }

        public double[] ToAnnData()
        {
            var objects = new List<object>()
            {
               ID,
               int.Parse(Line),
               Shift,
               Bucketize("Trip",Trip),
               int.Parse(Vehicle),
               int.Parse(BusStopCode),
               Bucketize("Type",Type),
               ScheduledArrival.TimeOfDay.TotalMinutes,
               ActualArrival.TimeOfDay.TotalMinutes,
               StopTime,
               ActualHeadWay,
               ScheduledHeadWay,
               Delay
            }.ToDoubles();

            return objects;
        }

        public string Header()
        {
            return "PassageID," +
                "Line," +
                "Shift," +
                "Trip," +
                "Vehicle," +
                "BusStopCode," +
                "Type," +
                "SheduledArrival," +
                "ActualArrival," +
                "StopTime," +
                "ActualHeadWay," +
                "ScheduledHeadWay," +
                "Delay";
        }
    }
}