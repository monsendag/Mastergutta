﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.AtB.Utilities
{
    public static class Data
    {
        public enum DataFolders
        {
            PassageAtBusStops,
            LinkTravelTimes,
            GPSLog,
            Soccer,
            Yr,
            Fulltrips,
            Buses,
            PassageAtVirtualLoops,
            PassengerCount,
            RealTime,
            Ticket,
            CellPhoneTicket,
            SingleTicket,
            Lines,
            Trips,
            BusStopPassages,
            WekaAnn = 1001, //Use 1001 or larger for items not connected to the DB.
        }

        public enum Users
        {
            Erlend,
            Aleksander,
            Simen,
            Server
        }

        public static Users User
        {
            get
            {
                if (Environment.MachineName == "ERLENDX")
                    return Users.Erlend;
                if (Environment.MachineName == "MASTERPCEN")
                    return Users.Aleksander;
                if (Environment.MachineName == "SKOGEN")
                    return Users.Simen;
                if (Environment.MachineName.ToLower() == "stud3051")
                    return Users.Server;
                throw new Exception("User not found!");
            }
        }

        public static string DataPath
        {
            get
            {
                switch (User)
                {
                    case Users.Erlend:
                        return @"G:\MasterData";
                    case Users.Server:
                        return @"C:\Data";
                    case Users.Simen:
                        return @"C:\Users\simen\Sync";
                    case Users.Aleksander:
                        return @"C:\Master\RepoDataSync";
                }
                throw new Exception("Data location not set. Set it for your user in DataBaseImporter\\Paths\\Data");
            }
        }

        public static string SqlPath
        {
            get
            {
                return TouchDir(Path.Combine(DataPath, "__SQL"));
            }
        }

        public static string SqlCmd
        {
            get
            {
                switch (User)
                {
                    case Users.Erlend:
                    case Users.Simen:
                    case Users.Aleksander:
                        return @"C:\Program Files\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe";
                }
                throw new Exception("Data location not set. Set it for your user in DataBaseImporter\\Paths\\SqlCmd");
            }
        }

        public static string TempFolder
        {
            get { return TouchDir(Path.Combine(DataPath, "..", "__temp", User.ToString())); }
        }

        public static List<string> GetDataFiles(DataFolders folder, DateTime start, DateTime end)
        {
            var path = GetDataFolder(folder);

            if (!System.IO.Directory.Exists(path))
                throw new DirectoryNotFoundException("Could not find data folder. Please change the code, or place the project in the correct location.");

            var periods = new List<DateTime>();
            for (var date = start; date <= end; date = date.AddDays(1))
            {
                var week = Weekify(date);
                if (!periods.Contains(week))
                    periods.Add(week);
            }

            return periods.Select(p => Path.Combine(path, p.ToString("yyyy-MM-dd") + " - " + WeekifyEnd(p).ToString("yyyy-MM-dd") + ".csv")).Where(File.Exists).ToList();
        }

        public static string GetDataFolder(DataFolders folder)
        {
            return TouchDir(Path.Combine(DataPath, GetRelativeFolder(folder)));
        }

        public static string GetWekaFolder()
        {
            return TouchDir(Path.Combine(DataPath, "weka"));
        }

        private static string TouchDir(string path)
        {
            if (path.Split('\\').Last().Contains("."))
            {
                TouchDir(Path.GetDirectoryName(path));
                return path;
            }
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        private static string GetRelativeFolder(DataFolders folder)
        {
            switch (folder)
            {
                case DataFolders.PassageAtBusStops:
                    return Path.Combine("flashnet", "passage");
                case DataFolders.PassageAtVirtualLoops:
                    return Path.Combine("flashnet", "passageatvirtualloops");
                case DataFolders.LinkTravelTimes:
                    return Path.Combine("flashnet", "links_travel_time");
                case DataFolders.GPSLog:
                    return Path.Combine("flashnet", "gpslog");
                case DataFolders.Fulltrips:
                    return Path.Combine("flashnet", "fulltrips");
                case DataFolders.PassengerCount:
                    return Path.Combine("flashnet", "passengercount");
                case DataFolders.RealTime:
                    return Path.Combine("flashnet", "realtime");
                case DataFolders.Ticket:
                    return Path.Combine("atb", "tickets");
                case DataFolders.CellPhoneTicket:
                    return Path.Combine("atb", "cellphonetickets");
                case DataFolders.SingleTicket:
                    return Path.Combine("atb", "singletickets");
                case DataFolders.Soccer:
                    return "fotball";
                case DataFolders.Buses:
                    return "busstops";
                case DataFolders.WekaAnn:
                    return Path.Combine("weka", "ann");
                default:
                    return folder.ToString().ToLower();
            }
        }

        public static string GetSqlFolder(DataFolders folder)
        {
            string path;
            switch (folder)
            {
                case DataFolders.LinkTravelTimes:
                    path = "linkstraveltime";
                    break;
                case DataFolders.Yr:
                    path = "yr";
                    break;
                case DataFolders.GPSLog:
                    path = "gps";
                    break;
                case DataFolders.PassengerCount:
                    path = "passengercount";
                    break;
                case DataFolders.RealTime:
                    path = "realtime";
                    break;
                default:
                    var name = folder.ToString().ToLower();
                    path = name + (name.EndsWith("s") ? "" : "s");
                    break;
            }
            return TouchDir(Path.Combine(SqlPath, path));
        }

        public static string GetDataFile(DataFolders folder, params string[] path)
        {
            var l = path.ToList();
            l.Insert(0, GetDataFolder(folder));
            return Path.Combine(l.ToArray());
        }

        public static string GetWekaFile(params string[] path)
        {
            var l = path.ToList();
            l.Insert(0, GetWekaFolder());
            return TouchDir(Path.Combine(l.ToArray()));
        }

        private static DateTime WeekifyEnd(DateTime date)
        {
            var newDate = date.AddDays(date.Day == 1 ? 6 : 7);
            if (newDate.Month > date.Month)
                return newDate.AddDays(-1);
            return newDate;
        }

        /// <summary>
        /// Returns the date changed to the appropriate week value (1st, 8th, 16th or 24th).
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static DateTime Weekify(DateTime date)
        {
            var day = date.Day;
            var newDay = day < 8 ? 1 : day < 16 ? 8 : day < 24 ? 16 : 24;
            return date.AddDays(newDay - day);
        }

        public static string GetSqlFile(DataFolders folder, params string[] path)
        {
            var l = path.ToList();
            l.Insert(0, GetSqlFolder(folder));
            return Path.Combine(l.ToArray());
        }

        public static bool HasDBTable(DataFolders folder)
        {
            return (int) folder <= 1000;
        }
    }
}
