﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.Tickets
{
    public class SingleTicket: DbLoader
    {
        [Key]
        public int ID { get; set; }

        //public int CompanyNo { get; set; }
        public Companies Company { get; set; }
        public virtual BusStop FromStop { get; set; }
        //public string StopShortName {get;set;}
        public int ToZone { get; set; }
        public string ZoneShortName { get; set; }
        //public int ProdTemplateNo {get;set;}
        public SingleTicketProducts Product { get; set; }
        //public string CustProfileId {get;set;}
        public CustomerProfiles CustomerProfile { get; set; }
        public int Amount { get; set; }

        public enum Companies
        {
            AtBDriftAS = 160,
            GauldalBillagAS = 162,
            GråkallbanenAS = 163,
            Kystekspressen = 165,
            FosenNamsosSjøAS = 166,
            NettbussTrøndelagAS = 168,
            SteinkjerbussAS = 172,
            TrønderbileneAS = 175,
            AtBDriftNBTro = 260,
            AtBDriftTB = 261,
            AtBDriftTIDE = 262,
            AtBDriftNBTrø = 263,
        }

        public enum SingleTicketProducts
        {
            AutoreiseStorTrondheim = 42,
            AutoreiseReisekontoStorTrondheim = 52,
            KontantbillettTrh = 350,
            EnkeltbillettRegion = 1,
            Makspris = 27,
            OvergangKontant = 24,
            AutoreiseVerdikortRegion = 41,
            AutoreiseReisekontoRegion = 51,
            EnkeltbillettBasis = 2,
            OvergangBussBåtFerge = 15,
            EnkeltbillettKombiBussBåt = 3,
            AutoreiseReisekontoKombiBussBåt = 53,
            FakturaEnkeltbillett = 173,
            AutoreiseVerdikortKombiBussBåt = 43,
            EnkeltbillettBussBåtVanvikanTrondheim = 19,
            Forsinkelsesbillett = 25,
            NattbillettAnbud = 191,
        }

        public enum CustomerProfiles
        {
            Voksen = 1,
            Honnør = 69,
            Barn = 2,
            Student = 3,
            Militær = 10,
            Ledsager = 113,
            Hund = 68,
            Sykkel = 81,
            BarnYngreEnnFire = 181,
        }

        public static IEnumerable<SingleTicket> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Skip(1).Select(line => line.Split(';')).Select(Load);
        }

        private static SingleTicket Load(string[] p)
        {
            return new SingleTicket()
            {
                Company = ParseCompany(p[1]),
                FromStop = new BusStop(){ID=p[2]},
                ToZone = ParseInt(p[4]),
                ZoneShortName = p[5],
                Product = ParseTicketProduct(p[7]),
                CustomerProfile = ParseCustomerProfile(p[9]),
                Amount = ParseInt(p[10]),
            };
        }

        private static Companies ParseCompany(string s)
        {
            switch (s)
            {
                case "AtB Drift AS":
                    return Companies.AtBDriftAS;
                case "Gauldal Billag AS":
                    return Companies.GauldalBillagAS;
                case "Gråkallbanen AS":
                    return Companies.GråkallbanenAS;
                case "Kystekspressen":
                    return Companies.Kystekspressen;
                case "FosenNamsos Sjø AS":
                    return Companies.FosenNamsosSjøAS;
                case "Nettbuss Trøndelag AS":
                    return Companies.NettbussTrøndelagAS;
                case "Steinkjerbuss AS":
                    return Companies.SteinkjerbussAS;
                case "Trønderbilene AS":
                    return Companies.TrønderbileneAS;
                case "AtB Drift NBTro":
                    return Companies.AtBDriftNBTro;
                case "AtB Drift TB":
                    return Companies.AtBDriftTB;
                case "AtB Drift TIDE":
                    return Companies.AtBDriftTIDE;
                case "AtB Drift NBTrø":
                    return Companies.AtBDriftNBTrø;
            }

            throw new Exception("Invalid product type: " + s);
        }

        private static SingleTicketProducts ParseTicketProduct(string s)
        {
            switch (s)
            {
                case "Autoreise Stor-Trondheim":
                    return SingleTicketProducts.AutoreiseStorTrondheim;
                case "Autoreise Reisekonto Stor-Trondheim":
                    return SingleTicketProducts.AutoreiseReisekontoStorTrondheim;
                case "Kontantbillett Trh":
                    return SingleTicketProducts.KontantbillettTrh;
                case "Enkeltbillett region":
                    return SingleTicketProducts.EnkeltbillettRegion;
                case "Makspris":
                    return SingleTicketProducts.Makspris;
                case "Overgang kontant":
                    return SingleTicketProducts.OvergangKontant;
                case "Autoreise Verdikort Region":
                    return SingleTicketProducts.AutoreiseVerdikortRegion;
                case "Autoreise Reisekonto Region":
                    return SingleTicketProducts.AutoreiseReisekontoRegion;
                case "Enkeltbillett Basis":
                    return SingleTicketProducts.EnkeltbillettBasis;
                case "Overgang Buss Båt og Ferge":
                    return SingleTicketProducts.OvergangBussBåtFerge;
                case "Enkeltbillett Kombi Buss/Båt":
                    return SingleTicketProducts.EnkeltbillettKombiBussBåt;
                case "Autoreise Reisekonto Kombi Buss/Båt":
                    return SingleTicketProducts.AutoreiseReisekontoKombiBussBåt;
                case "Faktura Enkeltbillett":
                    return SingleTicketProducts.FakturaEnkeltbillett;
                case "Autoreise Verdikort Kombi Buss/Båt":
                    return SingleTicketProducts.AutoreiseVerdikortKombiBussBåt;
                case "Enkeltbillett Buss/Båt, Vanvikan - Trondheim":
                    return SingleTicketProducts.EnkeltbillettBussBåtVanvikanTrondheim;
                case "Forsinkelsesbillett":
                    return SingleTicketProducts.Forsinkelsesbillett;
                case "Nattbillett Anbud":
                    return SingleTicketProducts.NattbillettAnbud;
            }

            throw new Exception("Invalid product type: " + s);
        }

        private static CustomerProfiles ParseCustomerProfile(string s)
        {
            switch (s)
            {
                case "Voksen":
                    return CustomerProfiles.Voksen;
                case "Honnør":
                    return CustomerProfiles.Honnør;
                case "Barn":
                    return CustomerProfiles.Barn;
                case "Student":
                    return CustomerProfiles.Student;
                case "Militær":
                    return CustomerProfiles.Militær;
                case "Ledsager":
                    return CustomerProfiles.Ledsager;
                case "Hund":
                    return CustomerProfiles.Hund;
                case "Sykkel":
                    return CustomerProfiles.Sykkel;
                case "BarnLT4":
                    return CustomerProfiles.BarnYngreEnnFire;
            }

            throw new Exception("Invalid product type: " + s);
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.SingleTicket);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.SingleTicket, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[SingleTickets]([Company],[ToZone],[ZoneShortName],[Product],[CustomerProfile],[Amount],[FromStop_ID]) VALUES({0},{1},'{2}',{3},{4},{5},{6});",
                    (int) p.Company, p.ToZone, p.ZoneShortName, (int) p.Product, (int) p.CustomerProfile, p.Amount, p.FromStop.ID)), Encoding.Default);

                BusStop.Append(items.Select(p=>p.FromStop));
            }
        }
    }
}
