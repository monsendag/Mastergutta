﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.Tickets
{
    public class CellPhoneTicket: DbLoader
    {
        [Key]
        public int ID { get; set; }

        public DateTime PurchaseDate { get; set; }
        public Categories Category { get; set; }
        public int NumTickets { get; set; }
        //public string TicketchannelId {get;set;}
        public TicketChannels Ticketchannel { get; set; }
        public Devices Device { get; set; }
        //public string PaymentchannelId {get;set;}
        public PaymentChannels PaymentChannel { get; set; }
        public decimal RevenueIncTax { get; set; }

        public enum Categories
        {
            TwentyFourHours,
            Child,
            Senior,
            Dog,
            Military,
            NightCity,
            Bike,
            Adult,
            NightByneset,
            NightKlaebu
        }

        public enum TicketChannels
        {
            Sms,
            App
        }

        public enum Devices
        {
            None,
            Sms,
            Iphone,
            Android
        }

        public enum PaymentChannels
        {
            Sms,
            TravelAccont,
            CreditCard
        }

        public static IEnumerable<CellPhoneTicket> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Select(line => line.Split(';')).Select(Load);
        }

        private static CellPhoneTicket Load(string[] p)
        {
            return new CellPhoneTicket()
            {
                PurchaseDate = ParseDateTime(p[0], "yyyy-MM-dd"),
                Category = ParseCategory(p[1]),
                NumTickets = ParseInt(p[2]),
                Ticketchannel = ParseTicketChannel(p[4]),
                Device = ParseDevice(p[5]),
                PaymentChannel = ParsePaymentChannel(p[7]),
                RevenueIncTax = ParseDecimal(p[8]),
            };
        }

        private static Categories ParseCategory(string s)
        {
            switch (s)
            {
                case "24t":
                    return Categories.TwentyFourHours;
                case "Barn":
                    return Categories.Child;
                case "Honnør":
                    return Categories.Senior;
                case "Hund":
                    return Categories.Dog;
                case "Militær":
                    return Categories.Military;
                case "Natt by":
                    return Categories.NightCity;
                case "Sykkel":
                    return Categories.NightCity;
                case "Voksen":
                    return Categories.Adult;
                case "Natt Byneset":
                    return Categories.NightByneset;
                case "Natt Klæbu":
                    return Categories.NightKlaebu;
            }

            throw new Exception("Invalid product type: " + s);
        }

        private static TicketChannels ParseTicketChannel(string s)
        {
            switch (s)
            {
                case "SMS":
                    return TicketChannels.Sms;
                case "APP":
                    return TicketChannels.App;
            }

            throw new Exception("Invalid product type: " + s);
        }

        private static Devices ParseDevice(string s)
        {
            switch (s)
            {
                case "sms":
                    return Devices.Sms;
                case "":
                    return Devices.None;
                case "iphone":
                    return Devices.Iphone;
                case "android":
                    return Devices.Android;
            }

            throw new Exception("Invalid product type: " + s);
        }

        private static PaymentChannels ParsePaymentChannel(string s)
        {
            switch (s)
            {
                case "SMS":
                    return PaymentChannels.Sms;
                case "TRAVELACCOUNT":
                    return PaymentChannels.TravelAccont;
                case "CREDITCARD":
                    return PaymentChannels.CreditCard;
            }

            throw new Exception("Invalid product type: " + s);
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.CellPhoneTicket);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.CellPhoneTicket, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[CellPhoneTickets]([PurchaseDate],[Category],[NumTickets],[Ticketchannel],[Device],[PaymentChannel],[RevenueIncTax]) VALUES('{0}',{1},{2},{3},{4},{5},{6});",
                    Sql(p.PurchaseDate), (int) p.Category, p.NumTickets, (int) p.Ticketchannel, (int) p.Device, (int) p.PaymentChannel, Sql(p.RevenueIncTax))), Encoding.Default);
            }
        }
    }
}
