﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.GPSLog
{
    public class GPSLogItem : DbLoader
    {
        [Key]
        public int ID { get; set; }

        public int AreaID { get; set; }

        public string AreaCode { get; set; }
        public string AreaDescription { get; set; }
        public int LineID { get; set; }
        public int LineAlphaCode { get; set; }
        public int PathID { get; set; }
        public string PathDirection { get; set; }
        public int VehicleID { get; set; }
        public int VehicleAlphaCode { get; set; }
        public int VehicleStatus { get; set; }
        public string OrigStopCode { get; set; }
        public string OrigStopDescription { get; set; }
        public string OrigStopType { get; set; }
        public string DestStopCode { get; set; }
        public string DestStopDesc { get; set; }
        public string DestStopType { get; set; }
        public string TermDestStopCode { get; set; }
        public string TermDestStopDesc { get; set; }
        public string ProxChangeStopCode { get; set; }
        public string ProxChangeStopDescription { get; set; }
        public string ProxChangeTimeArrive { get; set; }
        public int GeoLongtitude { get; set; }
        public int GeoLatitude { get; set; }
        public string LocationCity { get; set; }
        public string LocationAddress { get; set; }
        public DateTime TimeRefreshLocation { get; set; }
        public int PassengersCount { get; set; }
        public string PassengersLevel { get; set; }
        public int SecDelay { get; set; }
        public int SecInterval { get; set; }
        public int SecDelayPlanned { get; set; }
        public int SecBonusLine { get; set; }
        public string SecOffset { get; set; }
        public string SecDelayFormatted { get; set; }
        public string SecIntervalFormatted { get; set; }
        public string SecDelayPlannedFormatted { get; set; }
        public string SecBonusLineFormatted { get; set; }
        public int RelativePosition { get; set; }
        public int DriverCode { get; set; }
        public string DriverName { get; set; }
        public string TripCode { get; set; }
        public int ShiftCode { get; set; }

        public static IEnumerable<GPSLogItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Select(line => line.Split(';')).Select(Load);
        }

        private static GPSLogItem Load(string[] p)
        {
            var emptyFields = new int[] {6, 10, 12, 13, 14, 15, 16, 18, 19, 20, 27, 32, 34, 35, 36, 39};
            foreach (var e in emptyFields.Where(e => !string.IsNullOrEmpty(p[e])))
                Console.WriteLine("WHAAAT!? Supposedly empty field isn't empty at all! (" + e + ": '" + p[e] + "')");
            for (var i = 0; i < p.Length; i++)
                if (p[i].Contains("'")) p[i] = p[i].Replace("'", "''");
            return new GPSLogItem()
                {
                    AreaID = ParseInt(p[0]),
                    AreaCode = p[1],
                    AreaDescription = p[2],
                    LineID = ParseInt(p[3]),
                    LineAlphaCode = ParseInt(p[4]),
                    PathID = ParseInt(p[5]),
                    PathDirection = p[6],
                    VehicleID = ParseInt(p[7]),
                    VehicleAlphaCode = ParseInt(p[8]),
                    VehicleStatus = ParseInt(p[9]),
                    OrigStopCode = p[10],
                    OrigStopDescription = p[11],
                    OrigStopType = p[12],
                    DestStopCode = p[13],
                    DestStopDesc = p[14],
                    DestStopType = p[15],
                    TermDestStopCode = p[16],
                    TermDestStopDesc = p[17],
                    ProxChangeStopCode = p[18],
                    ProxChangeStopDescription = p[19],
                    ProxChangeTimeArrive = p[20],
                    GeoLongtitude = ParseInt(p[21]),
                    GeoLatitude = ParseInt(p[22]),
                    LocationCity = p[23],
                    LocationAddress = p[24],
                    TimeRefreshLocation = ParseDateTime(p[25], "dd.MM.yyyy HH:mm:ss"),
                    PassengersCount = ParseInt(p[26]),
                    PassengersLevel = p[27],
                    SecDelay = ParseInt(p[28]),
                    SecInterval = ParseInt(p[29]),
                    SecDelayPlanned = ParseInt(p[30]),
                    SecBonusLine = ParseInt(p[31]),
                    SecOffset = p[32],
                    SecDelayFormatted = p[33],
                    SecIntervalFormatted = p[34],
                    SecDelayPlannedFormatted = p[35],
                    SecBonusLineFormatted = p[36],
                    RelativePosition = ParseInt(p[37]),
                    DriverCode = ParseInt(p[38]),
                    DriverName = p[39],
                    TripCode = p[40],
                    ShiftCode = ParseInt(p[41])
                };
        }

        public static void ImportData(bool ignoreImported = true)
        {
            var path = Data.GetDataFolder(Data.DataFolders.GPSLog);

            for (int i = 0; i < 100; i++)
            {
                var db = new SMIODataContext();

                var files = new List<string>();
                foreach (var bak in System.IO.Directory.GetDirectories(path))
                    files.AddRange(System.IO.Directory.GetFiles(bak, "Line_" + i + "_*.csv"));

                if (ignoreImported && files.Count > 0)
                {
                    var count = files.Count;
                    files = files.Where(p => !ImportHistoryItem.IsImported(db, p)).ToList();
                    if (count - files.Count > 0 && files.Count > 0)
                        Console.WriteLine("Ignored {0} already imported files.", count - files.Count);
                }

                if (files.Count == 0) continue;

                var originalFiles = files.ToArray();

                var items = new List<GPSLogItem>();
                files.ForEach(p => items.AddRange(FromFile(p)));
                if (items.Count == 0) continue;
                Console.WriteLine("Found {0:n0} items for line {1}, from {2} different source(s).", items.Count, i, files.Count);

                if (items.Any(p => p.LineID != i))
                {
                    Console.WriteLine("  > WHAT THE FUCK! Other lines detected.");
                    Console.ReadLine();
                }

                items = items.OrderBy(p => p.TimeRefreshLocation).ThenBy(p => p.GeoLatitude).ThenBy(p => p.GeoLongtitude).ToList();

                var removed = 0;
                var j = 0;
                while (j < items.Count - 2)
                {
                    if (Math.Abs(items[j].TimeRefreshLocation.Subtract(items[j + 1].TimeRefreshLocation).TotalMinutes) < 5 && (items[j].GeoLatitude == items[j + 1].GeoLatitude && items[j].GeoLongtitude == items[j + 1].GeoLongtitude))
                    {
                        items.RemoveAt(j + 1);
                        removed++;
                    }
                    else
                        j++;
                }

                Console.WriteLine("  > Removed {0:n0} duplicate or redundant items, {1:n0} items remaining.", removed, items.Count);

                j = 0;
                removed = 0;
                var lineID = i;
                var minDate = items.Min(p => p.TimeRefreshLocation).AddMinutes(-10);
                var fullLine = db.GPSLogItems.Where(p => p.TimeRefreshLocation > minDate && p.LineID == lineID).ToList().Select(p => new {p.TimeRefreshLocation, p.GeoLatitude, p.GeoLongtitude}).ToList();
                var start = DateTime.Now;

                Console.WriteLine("  > Database already has {0:n0} items.", fullLine.Count);

                if (fullLine.Count > 0)
                {
                    Console.WriteLine("      > Searching for duplicates ...");
                    while (j < items.Count)
                    {
                        var item = items[j];
                        if (fullLine.Any(p => Math.Abs(p.TimeRefreshLocation.Subtract(item.TimeRefreshLocation).TotalMinutes) < 5 && (p.GeoLatitude == item.GeoLatitude && p.GeoLongtitude == item.GeoLongtitude)))
                        {
                            items.RemoveAt(j);
                            removed++;
                        }
                        else
                            j++;
                    }
                    Console.WriteLine("      > Removed {0:n0} duplicates, {1:n0} unique items remaining ({2:n2}). Inserting ...", removed, items.Count, DateTime.Now.Subtract(start).TotalMilliseconds);
                }

                if (items.Count > 0)
                {
                    db.GPSLogItems.AddRange(items);
                    db.SaveChanges();

                    Console.WriteLine("  > Inserted {0:n0} new items.", items.Count);
                }

                if (ignoreImported)
                {
                    originalFiles.ToList().ForEach(p => ImportHistoryItem.Import(db, p));
                    db.SaveChanges();
                }
            }

            Console.WriteLine();
        }

        public static int CountFilesToBeImported(bool ignoreImported = true)
        {
            var path = Data.GetDataFolder(Data.DataFolders.GPSLog);
            var db = new SMIODataContext();
            var fileCount = 0;
            for (var i = 0; i < 100; i++)
            {
                var files = new List<string>();
                foreach (var bak in System.IO.Directory.GetDirectories(path))
                    files.AddRange(System.IO.Directory.GetFiles(bak, "Line_" + i + "_*.csv"));

                if (ignoreImported && files.Count > 0)
                    files = files.Where(p => !ImportHistoryItem.IsImported(db, p)).ToList();

                fileCount += files.Count;
            }

            return fileCount;
        }

        public static void GenerateSqlFiles()
        {
            MergeFiles();

            var path = Data.GetDataFolder(Data.DataFolders.GPSLog);

            for (int i = 0; i < 100; i++)
            {
                var files = new List<string>();
                files.AddRange(System.IO.Directory.GetFiles(path, "Line_" + i + "_*.csv").
                                      Select(p => new {File = p, NameParts = System.IO.Path.GetFileNameWithoutExtension(p).Split('_')}).
                                      Where(p => p.NameParts.Length == 3).
                                      Where(p => ParseDateTime(p.NameParts[2], "yyyy-MM-dd").Date != DateTime.Now.Date).
                                      Select(p => p.File));

                if (files.Count == 0) continue;

                foreach (var file in files)
                {
                    var sql = Data.GetSqlFile(Data.DataFolders.GPSLog, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                    if (System.IO.File.Exists(sql)) continue;
                    Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                    var items = FromFile(file).ToList();
                    if (items.Count == 0) continue;

                    System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                        "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[GPSLogItems]([AreaID],[AreaCode],[AreaDescription],[LineID],[LineAlphaCode],[PathID],[PathDirection],[VehicleID],[VehicleAlphaCode],[VehicleStatus],[OrigStopCode],[OrigStopDescription],[OrigStopType],[DestStopCode],[DestStopDesc],[DestStopType],[TermDestStopCode],[TermDestStopDesc],[ProxChangeStopCode],[ProxChangeStopDescription],[ProxChangeTimeArrive],[GeoLongtitude],[GeoLatitude],[LocationCity],[LocationAddress],[TimeRefreshLocation],[PassengersCount],[PassengersLevel],[SecDelay],[SecInterval],[SecDelayPlanned],[SecBonusLine],[SecOffset],[SecDelayFormatted],[SecIntervalFormatted],[SecDelayPlannedFormatted],[SecBonusLineFormatted],[RelativePosition],[DriverCode],[DriverName],[TripCode],[ShiftCode]) VALUES({0},'{1}','{2}',{3},{4},{5},'{6}',{7},{8},{9},'{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',{21},{22},'{23}','{24}','{25}',{26},'{27}',{28},{29},{30},{31},'{32}','{33}','{34}','{35}','{36}',{37},{38},'{39}','{40}',{41});",
                        p.AreaID, p.AreaCode, p.AreaDescription, p.LineID, p.LineAlphaCode, p.PathID, p.PathDirection, p.VehicleID, p.VehicleAlphaCode, p.VehicleStatus, p.OrigStopCode, p.OrigStopDescription, p.OrigStopType, p.DestStopCode, p.DestStopDesc, p.DestStopType, p.TermDestStopCode, p.TermDestStopDesc, p.ProxChangeStopCode, p.ProxChangeStopDescription, p.ProxChangeTimeArrive, p.GeoLongtitude, p.GeoLatitude, p.LocationCity, p.LocationAddress, Sql(p.TimeRefreshLocation), p.PassengersCount, p.PassengersLevel, p.SecDelay, p.SecInterval, p.SecDelayPlanned, p.SecBonusLine, p.SecOffset, p.SecDelayFormatted, p.SecIntervalFormatted, p.SecDelayPlannedFormatted, p.SecBonusLineFormatted, p.RelativePosition, p.DriverCode, p.DriverName, p.TripCode, p.ShiftCode)), Encoding.Default);
                }
            }

            Console.WriteLine();
        }

        /// <summary>
        /// Will merge files from different computers and remove duplicates.
        /// </summary>
        private static void MergeFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.GPSLog);

            for (int i = 0; i < 100; i++)
            {
                var db = new SMIODataContext();

                var files = System.IO.Directory.GetFiles(path, "Line_" + i + "_*.csv").
                                   Select(p => new {File = p, NameParts = System.IO.Path.GetFileNameWithoutExtension(p).Split('_')}).
                                   Where(p => p.NameParts.Length == 4).
                                   Where(p => !System.IO.File.Exists(System.IO.Path.Combine(path, "Line_" + i + "_" + p.NameParts[3] + ".csv"))).
                                   GroupBy(p => ParseDateTime(p.NameParts[3], "yyyy-MM-dd")).
                                   Where(p => p.Key.Date != DateTime.Now.Date).ToList();

                if (files.Count == 0) continue;

                foreach (var date in files)
                {
                    var items = date.Select(p => p.File).SelectMany(p => System.IO.File.ReadAllLines(p, Encoding.Default).Select(l => new {Line = l, Item = Load(l.Split(';'))})).ToList();
                    if (items.Count == 0) continue;

                    Console.WriteLine("Found {0:n0} items for line {1}, from {2} different computers.", items.Count, i, date.Count());

                    if (items.Any(p => p.Item.LineID != i))
                    {
                        Console.WriteLine("  > WHAT THE FUCK! Other lines detected.");
                        Console.ReadLine();
                    }

                    items = items.OrderBy(p=>p.Item.VehicleID).ThenBy(p => p.Item.TimeRefreshLocation).ThenBy(p => p.Item.GeoLatitude).ThenBy(p => p.Item.GeoLongtitude).ToList();

                    var removed = 0;
                    var j = 0;
                    while (j < items.Count)
                    {
                        if (j + 1 >= items.Count) break;
                        if (items[j].Item.VehicleID==items[j+1].Item.VehicleID && Math.Abs(items[j].Item.TimeRefreshLocation.Subtract(items[j + 1].Item.TimeRefreshLocation).TotalMinutes) < 5 && (items[j].Item.GeoLatitude == items[j + 1].Item.GeoLatitude && items[j].Item.GeoLongtitude == items[j + 1].Item.GeoLongtitude))
                        {
                            items.RemoveAt(j + 1);
                            removed++;
                        }
                        else
                            j++;
                    }

                    Console.WriteLine("  > Removed {0:n0} duplicate or redundant items, {1:n0} items remaining.", removed, items.Count);

                    var file = System.IO.Path.Combine(path, string.Format("Line_{0}_{1}.csv", i, date.Key.Date.ToString("yyyy-MM-dd")));
                    System.IO.File.WriteAllLines(file, items.Select(p => p.Line), Encoding.Default);
                    Console.WriteLine("  > Wrote to file '{0}'.", file);
                }
            }
        }

        /// <summary>
        /// Will merge all data from the old system (Line_[n]_[computer].csv) into files fitting the new system (Line_[n]_[computer]_[date].csv).
        /// </summary>
        private static void MergeOldFiles()
        {
            //return; //Not to be run anymore!
            var path = Data.GetDataFolder(Data.DataFolders.GPSLog);
            var oldPath = System.IO.Path.Combine(path, "Old files");

            Console.WriteLine("Merging old files.");

            for (int i = 0; i < 100; i++)
            {
                var files = new List<string>();
                foreach (var bak in System.IO.Directory.GetDirectories(oldPath))
                    files.AddRange(System.IO.Directory.GetFiles(bak, "Line_" + i + "_*.csv").Where(p => System.IO.Path.GetFileNameWithoutExtension(p).Split('_').Length == 3));

                if (files.Count == 0) continue;

                Console.WriteLine("  > Found {0} files for line {1}.", files.Count, i);

                var items = files.SelectMany(p => System.IO.File.ReadAllLines(p, Encoding.Default).Select(l => new {Line = l, Date = ParseDateTime(l.Split(';')[25], "dd.MM.yyyy HH:mm:ss")})).GroupBy(p => p.Date.Date).ToDictionary(p => p.Key, c => c.Select(d => d).ToList());

                Console.WriteLine("  > Found {0} elements from {1} different days.", items.Values.Sum(p => p.Count), items.Keys.Count);

                foreach (var group in items)
                {
                    var file = System.IO.Path.Combine(path, string.Format("Line_{0}_{2}_{1}.csv", i, group.Key.Date.ToString("yyyy-MM-dd"), Environment.MachineName));
                    System.IO.File.WriteAllLines(file, group.Value.OrderBy(p => p.Date).Select(p => p.Line), Encoding.Default);
                    Console.WriteLine("      > Saved '{0}'.", System.IO.Path.GetFileNameWithoutExtension(file));
                }
            }
            Console.WriteLine("  > Done.");
        }
    }
}
