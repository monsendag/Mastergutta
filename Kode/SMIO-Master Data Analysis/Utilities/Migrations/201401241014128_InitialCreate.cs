namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BusStops",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LTTItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Trip = c.String(),
                        ActualDeparture = c.DateTime(nullable: false),
                        ActualArrival = c.DateTime(nullable: false),
                        StopTime = c.Int(nullable: false),
                        TravelTime = c.Int(nullable: false),
                        TotalTravelTime = c.Int(nullable: false),
                        HeadWay = c.Int(nullable: false),
                        BusStopFrom_ID = c.String(maxLength: 128),
                        BusStopTo_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BusStops", t => t.BusStopFrom_ID)
                .ForeignKey("dbo.BusStops", t => t.BusStopTo_ID)
                .Index(t => t.BusStopFrom_ID)
                .Index(t => t.BusStopTo_ID);
            
            CreateTable(
                "dbo.PassageItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Line = c.String(),
                        Shift = c.String(),
                        Trip = c.String(),
                        Vehicle = c.String(),
                        BusStopCode = c.String(),
                        Type = c.String(),
                        ActualArrival = c.DateTime(nullable: false),
                        ScheduledArrival = c.DateTime(nullable: false),
                        StopTime = c.Int(nullable: false),
                        Delay = c.Int(nullable: false),
                        ActualHeadWay = c.Int(nullable: false),
                        ScheduledHeadWay = c.Int(nullable: false),
                        BusStop_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BusStops", t => t.BusStop_ID)
                .Index(t => t.BusStop_ID);
            
            CreateTable(
                "dbo.WeatherObjects",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        Description = c.String(),
                        Temp = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TempMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TempMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Precipitation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Wind = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WindExtreme = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Humidity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PassageItems", "BusStop_ID", "dbo.BusStops");
            DropForeignKey("dbo.LTTItems", "BusStopTo_ID", "dbo.BusStops");
            DropForeignKey("dbo.LTTItems", "BusStopFrom_ID", "dbo.BusStops");
            DropIndex("dbo.PassageItems", new[] { "BusStop_ID" });
            DropIndex("dbo.LTTItems", new[] { "BusStopTo_ID" });
            DropIndex("dbo.LTTItems", new[] { "BusStopFrom_ID" });
            DropTable("dbo.WeatherObjects");
            DropTable("dbo.PassageItems");
            DropTable("dbo.LTTItems");
            DropTable("dbo.BusStops");
        }
    }
}
