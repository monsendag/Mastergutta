namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixTripItems : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TripItems", "ActualDeparture", c => c.DateTime(nullable: false));
            AddColumn("dbo.TripItems", "ActualArrival", c => c.DateTime(nullable: false));
            AddColumn("dbo.TripItems", "ScheduledDeparture", c => c.DateTime(nullable: false));
            AddColumn("dbo.TripItems", "ScheduledArrival", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TripItems", "ScheduledArrival");
            DropColumn("dbo.TripItems", "ScheduledDeparture");
            DropColumn("dbo.TripItems", "ActualArrival");
            DropColumn("dbo.TripItems", "ActualDeparture");
        }
    }
}
