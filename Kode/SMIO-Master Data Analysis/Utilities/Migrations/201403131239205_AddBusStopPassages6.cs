namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStopPassages", "TicketsSoldLastThreeHours", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusStopPassages", "TicketsSoldLastThreeHours");
        }
    }
}
