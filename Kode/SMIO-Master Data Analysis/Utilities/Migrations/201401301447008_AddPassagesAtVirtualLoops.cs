namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPassagesAtVirtualLoops : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PassageAtVirtualLoopsItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Line = c.String(),
                        Shift = c.String(),
                        Trip = c.String(),
                        Vehicle = c.String(),
                        Loop = c.String(),
                        Link = c.String(),
                        ActualArrival = c.DateTime(nullable: false),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RPTelegram = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PassageAtVirtualLoopsItems");
        }
    }
}
