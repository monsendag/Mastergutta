namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFullTrips : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FullTripItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Shift = c.String(),
                        Line = c.String(),
                        RouteID = c.String(),
                        Trip = c.String(),
                        Vehicle = c.String(),
                        ScheduledDepartureTime = c.Time(nullable: false, precision: 7),
                        DepartureTime = c.Time(nullable: false, precision: 7),
                        ScheduledArrivalTime = c.Time(nullable: false, precision: 7),
                        ArrivalTime = c.Time(nullable: false, precision: 7),
                        Length = c.Int(nullable: false),
                        OperatedMT = c.Int(nullable: false),
                        WithoutService = c.String(),
                        AverageTravelTime = c.Time(nullable: false, precision: 7),
                        TravelTime = c.Time(nullable: false, precision: 7),
                        ScheduledLayover = c.Time(nullable: false, precision: 7),
                        Layover = c.Time(nullable: false, precision: 7),
                        NotPerformedLayover = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DepartureDelay = c.String(),
                        ArrivalAdvanceSeconds = c.Int(nullable: false),
                        AverageHeadway = c.Time(nullable: false, precision: 7),
                        Headway = c.Time(nullable: false, precision: 7),
                        AverageOperationalSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageCommercialSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OperationalSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CommercialSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Order = c.String(),
                        Anomaly = c.String(),
                        LeaveOnTime = c.String(),
                        ArriveOnTime = c.String(),
                        ArrivalTerminal_ID = c.String(maxLength: 128),
                        DepartureTerminal_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BusStops", t => t.ArrivalTerminal_ID)
                .ForeignKey("dbo.BusStops", t => t.DepartureTerminal_ID)
                .Index(t => t.ArrivalTerminal_ID)
                .Index(t => t.DepartureTerminal_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FullTripItems", "DepartureTerminal_ID", "dbo.BusStops");
            DropForeignKey("dbo.FullTripItems", "ArrivalTerminal_ID", "dbo.BusStops");
            DropIndex("dbo.FullTripItems", new[] { "DepartureTerminal_ID" });
            DropIndex("dbo.FullTripItems", new[] { "ArrivalTerminal_ID" });
            DropTable("dbo.FullTripItems");
        }
    }
}
