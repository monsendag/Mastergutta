namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTrips : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TripItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LineID = c.String(maxLength: 8),
                        RouteID = c.String(maxLength: 8),
                        VehicleID = c.String(maxLength: 16),
                        InitialDelay = c.Int(nullable: false),
                        SoccerInTwo = c.Boolean(nullable: false),
                        SoccerTwoAgo = c.Boolean(nullable: false),
                        SoccerGameViewers = c.Int(nullable: false),
                        Temperature = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Precipitation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Wind = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Humidity = c.Int(nullable: false),
                        TemperatureYesterday = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecipitationYesterday = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WindYesterday = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HumidityYesterday = c.Int(nullable: false),
                        TemperatureLastWeek = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecipitationLastWeek = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WindLastWeek = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HumidityLastWeek = c.Int(nullable: false),
                        DelayLastTrip = c.Int(nullable: false),
                        AverageLFive = c.Int(nullable: false),
                        StdLastFive = c.Int(nullable: false),
                        DelayLWeek = c.Int(nullable: false),
                        AvrDelayLWeek = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TripItems");
        }
    }
}
