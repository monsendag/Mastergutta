namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusStopPrecisionFix2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStops", "Y", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            DropColumn("dbo.BusStops", "Y_");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusStops", "Y_", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            DropColumn("dbo.BusStops", "Y");
        }
    }
}
