namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTripItems : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TripItems", "TripID", c => c.String(maxLength: 16));
            AddColumn("dbo.TripItems", "FullTripItemID", c => c.Int(nullable: false));
            AddColumn("dbo.TripItems", "CPTYesterday", c => c.Int(nullable: false));
            AddColumn("dbo.TripItems", "CPTLWeek", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TripItems", "CPTLWeek");
            DropColumn("dbo.TripItems", "CPTYesterday");
            DropColumn("dbo.TripItems", "FullTripItemID");
            DropColumn("dbo.TripItems", "TripID");
        }
    }
}
