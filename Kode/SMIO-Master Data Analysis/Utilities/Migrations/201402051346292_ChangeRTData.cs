namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRTData : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RealTimeItems", "RealTimeStop_ID", "dbo.RealTimeStops");
            DropIndex("dbo.RealTimeItems", new[] { "RealTimeStop_ID" });
            DropColumn("dbo.RealTimeItems", "RealTimeStop_ID");
            DropTable("dbo.RealTimeStops");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RealTimeStops",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StopID = c.String(),
                        QueryTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.RealTimeItems", "RealTimeStop_ID", c => c.Int());
            CreateIndex("dbo.RealTimeItems", "RealTimeStop_ID");
            AddForeignKey("dbo.RealTimeItems", "RealTimeStop_ID", "dbo.RealTimeStops", "ID");
        }
    }
}
