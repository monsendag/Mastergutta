namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStopPassages", "ScheduledArrivalDateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.BusStopPassages", "AverageDelayLast3", c => c.Int(nullable: false));
            AlterColumn("dbo.BusStopPassages", "AverageDelayLast3OfSameDay", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BusStopPassages", "AverageDelayLast3OfSameDay", c => c.Double(nullable: false));
            AlterColumn("dbo.BusStopPassages", "AverageDelayLast3", c => c.Double(nullable: false));
            DropColumn("dbo.BusStopPassages", "ScheduledArrivalDateTime");
        }
    }
}
