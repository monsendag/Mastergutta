namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PassengerCountChangedVariableType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PassengerCountItems", "HighCapacity", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PassengerCountItems", "HighCapacity", c => c.Int(nullable: false));
        }
    }
}
