namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BusStopPassages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DayOfWeek = c.Int(nullable: false),
                        TimeOfDayMinute = c.Int(nullable: false),
                        TimeOfDayQuarter = c.Int(nullable: false),
                        TimeOfDayHalfHour = c.Int(nullable: false),
                        TimeOfDayHour = c.Int(nullable: false),
                        ActualArrival = c.Int(nullable: false),
                        ScheduledArrival = c.Int(nullable: false),
                        StopTime = c.Int(nullable: false),
                        HeadWay = c.Int(nullable: false),
                        Delay = c.Int(nullable: false),
                        Vehicle = c.Int(nullable: false),
                        PassengersPresent = c.Int(nullable: false),
                        Temperature = c.Double(nullable: false),
                        Precipitation = c.Double(nullable: false),
                        Wind = c.Double(nullable: false),
                        Humidity = c.Int(nullable: false),
                        SoccerGame = c.Int(nullable: false),
                        CellPhoneTicketsLast2Hours = c.Int(nullable: false),
                        DelayLast = c.Int(nullable: false),
                        AverageDelayLast3 = c.Double(nullable: false),
                        DelayYesterday = c.Int(nullable: false),
                        DelayDayBeforeYesterday = c.Int(nullable: false),
                        DelayLastWeek = c.Int(nullable: false),
                        AverageDelayLast3OfSameDay = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BusStopPassages");
        }
    }
}
