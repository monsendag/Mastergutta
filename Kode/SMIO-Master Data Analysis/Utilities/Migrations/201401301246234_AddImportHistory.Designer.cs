// <auto-generated />
namespace Utilities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.2-21211")]
    public sealed partial class AddImportHistory : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddImportHistory));
        
        string IMigrationMetadata.Id
        {
            get { return "201401301246234_AddImportHistory"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
