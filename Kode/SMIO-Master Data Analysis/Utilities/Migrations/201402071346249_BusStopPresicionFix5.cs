namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusStopPresicionFix5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BusStops", "X", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            AlterColumn("dbo.BusStops", "Y", c => c.Decimal(nullable: false, precision: 18, scale: 8));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BusStops", "Y", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.BusStops", "X", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
