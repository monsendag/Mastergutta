namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRealTimeItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RealTimeStops",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StopID = c.String(),
                        QueryTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RealTimeItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Line = c.String(),
                        Arrival = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        RealTimeStop_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RealTimeStops", t => t.RealTimeStop_ID)
                .Index(t => t.RealTimeStop_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RealTimeItems", "RealTimeStop_ID", "dbo.RealTimeStops");
            DropIndex("dbo.RealTimeItems", new[] { "RealTimeStop_ID" });
            DropTable("dbo.RealTimeItems");
            DropTable("dbo.RealTimeStops");
        }
    }
}
