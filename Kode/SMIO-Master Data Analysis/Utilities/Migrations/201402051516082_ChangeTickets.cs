namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTickets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "ZoneDistance", c => c.String());
            AlterColumn("dbo.Tickets", "TransportType", c => c.Int(nullable: false));
            DropColumn("dbo.Tickets", "Overgang");
            DropColumn("dbo.Tickets", "SoneDistance");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tickets", "SoneDistance", c => c.String());
            AddColumn("dbo.Tickets", "Overgang", c => c.Int(nullable: false));
            AlterColumn("dbo.Tickets", "TransportType", c => c.Boolean(nullable: false));
            DropColumn("dbo.Tickets", "ZoneDistance");
        }
    }
}
