namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGPSLogItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GPSLogItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AreaID = c.Int(nullable: false),
                        AreaCode = c.String(),
                        AreaDescription = c.String(),
                        LineID = c.Int(nullable: false),
                        LineAlphaCode = c.Int(nullable: false),
                        PathID = c.Int(nullable: false),
                        PathDirection = c.String(),
                        VehicleID = c.Int(nullable: false),
                        VehicleAlphaCode = c.Int(nullable: false),
                        VehicleStatus = c.Int(nullable: false),
                        OrigStopCode = c.String(),
                        OrigStopDescription = c.String(),
                        OrigStopType = c.String(),
                        DestStopCode = c.String(),
                        DestStopDesc = c.String(),
                        DestStopType = c.String(),
                        TermDestStopCode = c.String(),
                        TermDestStopDesc = c.String(),
                        ProxChangeStopCode = c.String(),
                        ProxChangeStopDescription = c.String(),
                        ProxChangeTimeArrive = c.String(),
                        GeoLongtitude = c.Int(nullable: false),
                        GeoLatitude = c.Int(nullable: false),
                        LocationCity = c.String(),
                        LocationAddress = c.String(),
                        TimeRefreshLocation = c.DateTime(nullable: false),
                        PassengersCount = c.Int(nullable: false),
                        PassengersLevel = c.String(),
                        SecDelay = c.Int(nullable: false),
                        SecInterval = c.Int(nullable: false),
                        SecDelayPlanned = c.Int(nullable: false),
                        SecBonusLine = c.Int(nullable: false),
                        SecOffset = c.String(),
                        SecDelayFormatted = c.String(),
                        SecIntervalFormatted = c.String(),
                        SecDelayPlannedFormatted = c.String(),
                        SecBonusLineFormatted = c.String(),
                        RelativePosition = c.Int(nullable: false),
                        DriverCode = c.Int(nullable: false),
                        DriverName = c.String(),
                        TripCode = c.String(),
                        ShiftCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GPSLogItems");
        }
    }
}
