namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusStopPrecisionFix4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStops", "X", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            DropColumn("dbo.BusStops", "X_");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusStops", "X_", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            DropColumn("dbo.BusStops", "X");
        }
    }
}
