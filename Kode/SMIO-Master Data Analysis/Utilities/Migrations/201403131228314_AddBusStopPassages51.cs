namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages51 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStopPassages", "TicketsSoldLastHour", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusStopPassages", "TicketsSoldLastHour");
        }
    }
}
