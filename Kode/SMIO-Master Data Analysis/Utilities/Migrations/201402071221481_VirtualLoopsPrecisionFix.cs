namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualLoopsPrecisionFix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Latitude_", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Longitude_", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Latitude");
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Longitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Longitude_");
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Latitude_");
        }
    }
}
