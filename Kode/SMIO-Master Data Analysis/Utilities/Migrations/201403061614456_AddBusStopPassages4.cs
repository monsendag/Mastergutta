namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BusStopPassages", "CellPhoneTicketsLast2Hours");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusStopPassages", "CellPhoneTicketsLast2Hours", c => c.Int(nullable: false));
        }
    }
}
