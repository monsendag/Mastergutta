﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Yr;

namespace DelayGrapher
{
    class WeatherGroup
    {
        private readonly IEnumerable<WeatherObject> _children;
        private DateTime _time;
        public WeatherGroup(DateTime time)
        {
            _time = time;
        }

        public DateTime Time { get { return _time; } }

        public WeatherGroup(DateTime time, IEnumerable<WeatherObject> children ) : this(time)
        {
            _children = children;
        }

        public string Id
        {
            get { return String.Format("{0} kl {1}", _time.ToShortDateString(), _time.Hour); }
        }

        public decimal MinTemp
        {
            get { return _children.Min(p => p.TempMin); }
        }

        public decimal Precipitation
        {
            get { return _children.Sum(p => p.Precipitation); }
        }

        public decimal WindExtreme
        {
            get { return _children.Max(p => p.Wind); }
        }
    }
}
